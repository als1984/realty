<?php
return [

    /*
    |--------------------------------------------------------------------------
    | Api key
    |--------------------------------------------------------------------------
    | The key obtained in the developer's office. Used only in the paid API version.
    | Ключ, полученный в кабинете разработчика. Используется только в платной версии API.
    |
     */
    'api_key'     => '3d91c420-facb-41da-91fc-6c81a8b61e67',
    /*
    |--------------------------------------------------------------------------
    | Version api
    |--------------------------------------------------------------------------
    | Version used api
    | Версия используемого api
    |
     */
    'api_version' => '1.x',
    /*
    |--------------------------------------------------------------------------
    | Language
    |--------------------------------------------------------------------------
    | Language response
    | Язык ответа
    |
    | uk_UA = ukraine
    | ru_RU = russian
    | en_RU = english
    |
     */
    'language'    => 'ru_RU',
    /*
    |--------------------------------------------------------------------------
    | Version api
    |--------------------------------------------------------------------------
    | Version used api
    | Версия используемого api
    |
     */
    'skip_object' => 0,

];
