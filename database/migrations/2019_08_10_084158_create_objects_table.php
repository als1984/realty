<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateObjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('objects', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('slug')->nullable()->index();
            $table->string('main_image')->nullable();
            $table->text('images')->nullable();
            $table->string('name')->index();
            $table->double('price')->default(0);
            $table->double('price_usd')->default(0)->index();
            $table->double('price_m2')->default(0);
            $table->double('price_m2_usd')->default(0)->index();
            $table->string('type')->nullable()->index();
            $table->string('property_type')->nullable()->comment('тип недвижимости')->index();
            $table->string('house_type')->nullable()->index();
            $table->integer('rooms')->default(1)->index();
            $table->double('square')->default(0)->index();
            $table->double('living_square')->default(0)->index();
            $table->double('kitchen_square')->default(0)->index();
            $table->integer('floor')->default(0)->index();
            $table->integer('year')->default(0)->index();
            $table->boolean('repair')->default(false)->index();
            $table->boolean('furniture')->default(false)->index();
            $table->string('title')->nullable();
            $table->string('description')->nullable();
            $table->text('content')->nullable()->comment('описание');
            $table->text('address')->nullable();
            $table->point('coordinates');
            $table->boolean('published')->default(true)->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('objects');
    }
}
