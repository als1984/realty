<?php

/*
 * Инструкция по установке
 *
 * В .htaccess добавить:
 * RewriteRule "^export/([a-z]+)\.xml$" "/export.php?format=$1" [PT]
 * RewriteRule "^export/([a-z]+)-flats\.xml$" "/export.php?format=$1&type=1" [PT]
 * RewriteRule "^export/([a-z]+)-cottages\.xml$" "/export.php?format=$1&type=3" [PT]
 */

define("LOGIN", 'ulasnydah');
define("PASSWORD", '23d79m23')	;

/*
 * Выгрузка в форматах сайтов партнеров
 */
if (!empty($_GET['format'])) {
	$urlParams = GetUserUrlParams();

	$urlParams[] = 'login=' . urlencode(LOGIN);
	$urlParams[] = 'password=' . urlencode(PASSWORD);

	foreach ($_GET as $key => $value) {
	 	$urlParams[] = $key . '=' . urlencode($value);
 	}

    $url = 'https://realt.by/api/export/?' . implode('&', $urlParams);
	file_put_contents(__DIR__.'/.export.log', $url);

    header('Content-type: application/xml; charset=utf-8');
    header('Cache-Control: no-cache');

    echo file_get_contents($url);
}
/*
 * Выгрузка в формате realt.by
 */
else {
	$table_id = is_numeric($_GET['table_id']) ? intval($_GET['table_id']) : false;

	if ($table_id == 1 || $table_id == 3) {
	    $login = array();
	    $login['user'] = LOGIN;
	    $login['pass'] = PASSWORD;

	    $config = array();
	    $config['zlib'] = 0;
	    $config['tableid'] = $table_id;
	    $config['type'] = 5;
	    $config['charset'] = 'utf-8';
	    $config['expand-shortcuts'] = 1;

	    $urlParams = GetUserUrlParams();
	    foreach ($login as $field => $value) {
	        $urlParams[] = 'login[' . urlencode($field) . ']=' . urlencode($value);
	    }
	    foreach ($config as $field => $value) {
	        $urlParams[] = 'config[' . urlencode($field) . ']=' . urlencode($value);
	    }

	    $url = 'https://realt.by/typo3conf/ext/uedb_core/getrecords.php?' . implode('&', $urlParams);

	    header('Content-type: application/xml; charset=utf-8');
	    header('Cache-Control: no-cache');

	    echo file_get_contents($url);
	}
	else {
	    header('Content-type: text/plain; charset=utf-8');
	    header('Cache-Control: no-cache');

	    echo 'configuration error';
	}
}

/**
 * get google data & user ip
 * @return array
 */
function GetUserUrlParams()
{
    $ipaddress = isset($_SERVER['HTTP_X_REAL_IP']) ?
        $_SERVER['HTTP_X_REAL_IP'] :
        $_SERVER['REMOTE_ADDR'];

    $params[] = 'user[ip]=' . urlencode($ipaddress);

    if (isset($_COOKIE['_ga'])) {
        $params[] = 'user[_ga]=' . urlencode($_COOKIE['_ga']);
    } elseif (isset($_COOKIE['__utma'])) {
        $params[] = 'user[__utma]=' . urlencode($_COOKIE['__utma']);
    }

    return $params;
}