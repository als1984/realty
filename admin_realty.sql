-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Хост: localhost
-- Время создания: Окт 03 2019 г., 05:42
-- Версия сервера: 5.5.60-MariaDB
-- Версия PHP: 7.2.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `admin_realty`
--

-- --------------------------------------------------------

--
-- Структура таблицы `blocks`
--

CREATE TABLE `blocks` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `blocks`
--

INSERT INTO `blocks` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Наши услуги', 'Наши услуги главная страница', '2019-08-12 19:56:30', '2019-08-12 19:56:30'),
(2, 'Слайдер', 'Слайдер главная', '2019-08-12 20:03:47', '2019-08-12 20:03:47'),
(3, 'Почему мы', 'Почему мы главная', '2019-08-18 06:25:23', '2019-08-18 06:25:23'),
(4, 'Мы работаем с', 'Мы работаем с  главная страница', '2019-08-18 06:34:22', '2019-08-18 06:34:22'),
(5, 'Рекомендуем', 'Блоки рекомендуем на странице объекта', '2019-08-27 08:33:53', '2019-08-27 08:33:53'),
(6, 'Работа в нашей компании', 'Работа в нашей компании описание', '2019-09-22 12:22:21', '2019-09-22 12:22:21'),
(7, 'Работа в нашей компании форма', 'Работа в нашей компании блоки рядом с формой', '2019-09-22 12:47:41', '2019-09-22 12:47:41');

-- --------------------------------------------------------

--
-- Структура таблицы `blocks_objects`
--

CREATE TABLE `blocks_objects` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `object_id` bigint(20) UNSIGNED NOT NULL,
  `block_id` bigint(20) UNSIGNED NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `text` text COLLATE utf8mb4_unicode_ci,
  `file` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `blocks_objects`
--

INSERT INTO `blocks_objects` (`id`, `object_id`, `block_id`, `image`, `title`, `text`, `file`, `created_at`, `updated_at`) VALUES
(1, 1, 5, NULL, 'Общая площадь 65 кв.м., больше чем в трехкомнатной квартире', '<p>Общая площадь составляет 30/ 18/ 5,4. Есть подвал.</p>', '[]', NULL, NULL),
(2, 1, 5, NULL, 'Общая площадь 65 кв.м., больше чем в трехкомнатной квартире', '<p>Дом кирпичный 1985 года постройки, расположен в 10 км от МКАД в могилевском... направлении</p>', '[]', NULL, NULL),
(3, 1, 5, NULL, 'Тихий центр - всегда удобно, всегда спокойно', '<p>Спокойное, тихое место, возле дома школа, рядом детский сад, поликлиника, банк,</p>', '[]', NULL, NULL),
(4, 2, 5, NULL, 'Тихий центр - всегда удобно, всегда спокойно', '<p>Спокойное, тихое место, возле дома школа, рядом детский сад, поликлиника, банк,</p>', '[]', NULL, NULL),
(5, 2, 5, NULL, 'Общая площадь 65 кв.м., больше чем в трехкомнатной квартире', '<p>Дом кирпичный 1985 года постройки, расположен в 10 км от МКАД в могилевском... направлении</p>', '[]', NULL, NULL),
(6, 2, 5, NULL, 'Общая площадь 65 кв.м., больше чем в трехкомнатной квартире', '<p>Общая площадь составляет 30/ 18/ 5,4. Есть подвал.</p>', '[]', NULL, NULL),
(7, 3, 5, NULL, 'Общая площадь 65 кв.м., больше чем в трехкомнатной квартире', '<p>Общая площадь составляет 30/ 18/ 5,4. Есть подвал.</p>', '[]', NULL, NULL),
(8, 3, 5, NULL, 'Общая площадь 65 кв.м., больше чем в трехкомнатной квартире', '<p>Дом кирпичный 1985 года постройки, расположен в 10 км от МКАД в могилевском... направлении</p>', '[]', NULL, NULL),
(9, 3, 5, NULL, 'Тихий центр - всегда удобно, всегда спокойно', '<p>Спокойное, тихое место, возле дома школа, рядом детский сад, поликлиника, банк,</p>', '[]', NULL, NULL),
(10, 4, 5, NULL, 'Общая площадь 65 кв.м., больше чем в трехкомнатной квартире', '<p>Общая площадь составляет 30/ 18/ 5,4. Есть подвал.</p>', '[]', NULL, NULL),
(11, 4, 5, NULL, 'Общая площадь 65 кв.м., больше чем в трехкомнатной квартире', '<p>Дом кирпичный 1985 года постройки, расположен в 10 км от МКАД в могилевском... направлении</p>', '[]', NULL, NULL),
(12, 4, 5, NULL, 'Тихий центр - всегда удобно, всегда спокойно', '<p>Спокойное, тихое место, возле дома школа, рядом детский сад, поликлиника, банк,</p>', '[]', NULL, NULL),
(13, 5, 5, NULL, 'Общая площадь 65 кв.м., больше чем в трехкомнатной квартире', '<p>Общая площадь составляет 30/ 18/ 5,4. Есть подвал.</p>', '[]', NULL, NULL),
(14, 5, 5, NULL, 'Общая площадь 65 кв.м., больше чем в трехкомнатной квартире', '<p>Дом кирпичный 1985 года постройки, расположен в 10 км от МКАД в могилевском... направлении</p>', '[]', NULL, NULL),
(15, 5, 5, NULL, 'Тихий центр - всегда удобно, всегда спокойно', '<p>Спокойное, тихое место, возле дома школа, рядом детский сад, поликлиника, банк,</p>', '[]', NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `blocks_pages`
--

CREATE TABLE `blocks_pages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `page_id` bigint(20) UNSIGNED NOT NULL,
  `block_id` bigint(20) UNSIGNED NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `text` text COLLATE utf8mb4_unicode_ci,
  `file` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `svg` text COLLATE utf8mb4_unicode_ci,
  `order` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `blocks_pages`
--

INSERT INTO `blocks_pages` (`id`, `page_id`, `block_id`, `image`, `title`, `text`, `file`, `svg`, `order`) VALUES
(1, 1, 2, NULL, 'Продавайте квартиры по выгодной цене', NULL, '[]', NULL, 0),
(2, 1, 2, NULL, 'Покупайти квартиры по выгодной цене', NULL, '[]', NULL, 0),
(3, 1, 2, NULL, 'Обменивайте квартиры выгодно', NULL, '[]', NULL, 0),
(4, 1, 1, NULL, 'Продажа недвижимости', '<ul>\r\n<li>- продажа на 5-7% дороже</li>\r\n<li>- реклама, которая продает</li>\r\n<li>- выбор покупателей</li>\r\n<li>- надежный расчет</li>\r\n</ul>', '[]', '<use xmlns:xlink=\"http://www.w3.org/1999/xlink\" xlink:href=\"img/sprite-svg.svg#auction-hammer\"></use>', 0),
(5, 1, 1, NULL, 'Обмен недвижимости', '<ul>\r\n<li>Шаг 1: выбираем вариант обмена</li>\r\n<li>Шаг 2: считаем доплату</li>\r\n<li>Шаг 3: оформляем сделки</li>\r\n<li>Шаг 4: переезжаем</li>\r\n</ul>', '[]', '<use xmlns:xlink=\"http://www.w3.org/1999/xlink\" xlink:href=\"img/sprite-svg.svg#property-exchange\"></use>', 0),
(6, 1, 1, NULL, 'Покупка недвижимости', '<ul>\r\n<li>- с нами цены ниже</li>\r\n<li>- кредит, лизинг</li>\r\n<li>- правовая экспертиза</li>\r\n<li>- страховка сделок</li>\r\n<li>- экономия времени и нервов</li>\r\n</ul>', '[]', '<use xmlns:xlink=\"http://www.w3.org/1999/xlink\" xlink:href=\"img/sprite-svg.svg#property-search\"></use>', 0),
(7, 1, 1, NULL, 'Аренда недвижимости', '<p>-</p>', '[]', '<use xmlns:xlink=\"http://www.w3.org/1999/xlink\" xlink:href=\"img/sprite-svg.svg#property-rent\"></use>', 0),
(8, 1, 3, NULL, 'Просто', '<p class=\"section-advantages-card_text\">С нами легко и просто, а все сложное делаем простым. Мы вас слышим, понимаем, и делаем, что надо именно вам.</p>', '[]', '<svg width=\"61\" height=\"61\">\r\n    <use xmlns:xlink=\"http://www.w3.org/1999/xlink\" xlink:href=\"img/sprite-svg.svg#reliably\"></use>\r\n</svg>', 0),
(9, 1, 3, NULL, 'Понятно', '<p class=\"section-advantages-card_text\">Рассказываем. Показываем. Объясняем. Убеждаем. Решение принимаете вы.</p>', '[]', '<svg width=\"61\" height=\"61\">\r\n    <use xmlns:xlink=\"http://www.w3.org/1999/xlink\" xlink:href=\"img/sprite-svg.svg#safely\"></use>\r\n</svg>', 0),
(10, 1, 3, NULL, 'Прогнозируемо', '<p class=\"section-advantages-card_text\">Всякий раз есть &laquo;дорожная карта&raquo; решения любого вопроса. Всегда есть &laquo;план Б&raquo;. Вы заранее знаете, что будет.</p>', '[]', '<svg width=\"61\" height=\"61\">\r\n    <use xmlns:xlink=\"http://www.w3.org/1999/xlink\" xlink:href=\"img/sprite-svg.svg#conveniently\"></use>\r\n</svg>', 0),
(11, 1, 3, NULL, 'Прозрачно', '<p class=\"section-advantages-card_text\">Государственная регистрация. Лицензия Министерства юстиции. Обязательное страхование. Государственные тарифы.</p>', '[]', '<svg width=\"61\" height=\"61\">\r\n    <use xmlns:xlink=\"http://www.w3.org/1999/xlink\" xlink:href=\"img/sprite-svg.svg#technologically\"></use>\r\n</svg>', 0),
(12, 1, 4, 'blocks-pages/August2019/RKxcHwitPuBvUBuBlRZQ.jpg', 'Коммерческой недвижимостью', NULL, '[]', NULL, 0),
(13, 1, 4, 'blocks-pages/August2019/sV4R75Sc3E8lMmD1TQmU.jpg', 'Новостройками', NULL, '[]', NULL, 0),
(14, 1, 4, 'blocks-pages/August2019/JxLRIm13hZVvt67JerR8.jpg', 'Вторичным жильём', NULL, '[]', NULL, 0),
(15, 1, 4, 'blocks-pages/August2019/PCpIIx1HoBYxGTp5xHpB.jpg', 'Загородной недвижимостью', NULL, '[]', NULL, 0),
(16, 2, 6, 'blocks-pages/September2019/IquaWtKK2DaBVg81gigy.jpg', 'Наш коллектив', '<p>Агентство &laquo;Уласны дах&raquo; &ndash; это команда профессионалов и единомышленников, которые любят свое дело и с радостью помогают людям продавать и покупать недвижимость. Мы практикуем передовые риэлторские технологии, постоянно внедряем новые фишки и техники, совершенствуем свои навыки. Работаем много и получаем соответствующую отдачу.</p>', '[]', NULL, 0),
(17, 2, 6, NULL, 'Хорошая репутация', '<p>Агентство &laquo;Уласны дах&raquo; &ndash; это команда профессионалов и единомышленников, которые любят свое дело и с радостью помогают людям продавать и покупать недвижимость. Мы практикуем передовые риэлторские технологии, постоянно внедряем новые фишки и техники, совершенствуем свои навыки. Работаем много и получаем соответствующую отдачу.</p>', '[]', NULL, 1),
(18, 2, 6, NULL, 'Внедрение высоких технологий', '<p>&laquo;Уласны дах&raquo; &ndash; современное агентство недвижимости. Мы используем передовые риэлторские технологии. Наша ниша &ndash; продажа, обмен и покупка жилой недвижимости в Минске и пригороде. Агентство &laquo;Уласны дах&raquo; &ndash; это команда профессионалов и единомышленников, которые любят свое дело и с радостью помогают людям продавать и покупать недвижимость. Мы практикуем передовые риэлторские технологии, постоянно внедряем новые фишки и техники, совершенствуем свои навыки. Работаем много и получаем соответствующую отдачу.</p>', '[]', NULL, 2),
(19, 2, 6, NULL, 'Заработная плата', '<p>Мы практикуем передовые риэлторские технологии, постоянно внедряем новые фишки и техники, совершенствуем свои навыки. Работаем много и получаем соответствующую отдачу.</p>', '[]', NULL, 3),
(20, 2, 6, NULL, 'График работы', '<p>Мы практикуем передовые риэлторские технологии, постоянно внедряем новые фишки и техники, совершенствуем свои навыки. Работаем много и получаем соответствующую отдачу.</p>', '[]', NULL, 4),
(21, 2, 7, NULL, 'Обязанности', '<p>&laquo;Уласны дах&raquo; &ndash; современное агентство недвижимости. Мы используем передовые риэлторские технологии. Наша ниша &ndash; продажа, обмен и покупка жилой недвижимости в Минске и пригороде. Агентство &laquo;Уласны дах&raquo; &ndash; это команда профессионалов и единомышленников, которые любят свое дело и с радостью помогают людям продавать и покупать недвижимость.</p>', '[]', NULL, 0),
(22, 2, 7, NULL, 'Требования', '<p>&laquo;Уласны дах&raquo; &ndash; современное агентство недвижимости. Мы используем передовые риэлторские технологии. Наша ниша &ndash; продажа, обмен и покупка жилой недвижимости в Минске и пригороде. Агентство &laquo;Уласны дах&raquo; &ndash; это команда профессионалов и единомышленников, которые любят свое дело и с радостью помогают людям продавать и покупать недвижимость.</p>', '[]', NULL, 1),
(23, 2, 7, NULL, 'Мы предлагаем', '<p>&laquo;Уласны дах&raquo; &ndash; современное агентство недвижимости. Мы используем передовые риэлторские технологии. Наша ниша &ndash; продажа, обмен и покупка жилой недвижимости в Минске и пригороде. Агентство &laquo;Уласны дах&raquo; &ndash; это команда профессионалов и единомышленников, которые любят свое дело и с радостью помогают людям продавать и покупать недвижимость.</p>', '[]', NULL, 2),
(24, 2, 7, NULL, 'Анкета', NULL, '[{\"download_link\":\"blocks-pages\\/September2019\\/l2yutiEjDEwZkbtdfxkk.jpg\",\"original_name\":\"team.jpg\"}]', NULL, 3);

-- --------------------------------------------------------

--
-- Структура таблицы `data_rows`
--

CREATE TABLE `data_rows` (
  `id` int(10) UNSIGNED NOT NULL,
  `data_type_id` int(10) UNSIGNED NOT NULL,
  `field` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `browse` tinyint(1) NOT NULL DEFAULT '1',
  `read` tinyint(1) NOT NULL DEFAULT '1',
  `edit` tinyint(1) NOT NULL DEFAULT '1',
  `add` tinyint(1) NOT NULL DEFAULT '1',
  `delete` tinyint(1) NOT NULL DEFAULT '1',
  `details` text COLLATE utf8mb4_unicode_ci,
  `order` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `data_rows`
--

INSERT INTO `data_rows` (`id`, `data_type_id`, `field`, `type`, `display_name`, `required`, `browse`, `read`, `edit`, `add`, `delete`, `details`, `order`) VALUES
(1, 1, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(2, 1, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(3, 1, 'email', 'text', 'Email', 1, 1, 1, 1, 1, 1, NULL, 3),
(4, 1, 'password', 'password', 'Password', 1, 0, 0, 1, 1, 0, NULL, 4),
(5, 1, 'remember_token', 'text', 'Remember Token', 0, 0, 0, 0, 0, 0, NULL, 5),
(6, 1, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, NULL, 6),
(7, 1, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 7),
(8, 1, 'avatar', 'image', 'Avatar', 0, 1, 1, 1, 1, 1, NULL, 8),
(9, 1, 'user_belongsto_role_relationship', 'relationship', 'Role', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"roles\",\"pivot\":0}', 10),
(10, 1, 'user_belongstomany_role_relationship', 'relationship', 'Roles', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"user_roles\",\"pivot\":\"1\",\"taggable\":\"0\"}', 11),
(11, 1, 'settings', 'hidden', 'Settings', 0, 0, 0, 0, 0, 0, NULL, 12),
(12, 2, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(13, 2, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(14, 2, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(15, 2, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(16, 3, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(17, 3, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(18, 3, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(19, 3, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(20, 3, 'display_name', 'text', 'Display Name', 1, 1, 1, 1, 1, 1, NULL, 5),
(21, 1, 'role_id', 'text', 'Role', 1, 1, 1, 1, 1, 1, NULL, 9),
(22, 4, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(23, 4, 'name', 'text', 'Название', 1, 1, 1, 1, 1, 1, '{}', 2),
(24, 4, 'description', 'text', 'Описание', 1, 1, 1, 1, 1, 1, '{}', 3),
(25, 4, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 4),
(26, 4, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 5),
(27, 5, 'id', 'text', 'Id', 1, 1, 0, 0, 0, 0, '{}', 1),
(28, 5, 'title', 'text', 'Pagetitle', 0, 1, 1, 1, 1, 1, '{}', 2),
(29, 5, 'name', 'text', 'Название', 1, 1, 1, 1, 1, 1, '{}', 3),
(30, 5, 'description', 'text', 'Description', 0, 0, 1, 1, 1, 1, '{}', 4),
(31, 5, 'content', 'rich_text_box', 'Содержимое', 0, 0, 1, 1, 1, 1, '{}', 5),
(32, 5, 'slug', 'text', 'Url', 0, 1, 1, 1, 1, 1, '{}', 6),
(33, 5, 'published', 'radio_btn', 'Опубликован?', 1, 1, 1, 1, 1, 1, '{\"default\":1,\"options\":{\"0\":\"\\u043d\\u0435\\u0442\",\"1\":\"\\u0434\\u0430\"}}', 7),
(34, 5, 'template', 'text', 'Шаблон', 0, 1, 1, 1, 1, 1, '{}', 8),
(35, 5, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 9),
(36, 5, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 10),
(37, 7, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(38, 7, 'page_id', 'text', 'Page Id', 1, 0, 0, 1, 1, 1, '{}', 2),
(39, 7, 'block_id', 'text', 'Block Id', 1, 0, 0, 1, 1, 1, '{}', 3),
(40, 7, 'image', 'image', 'Картинка', 0, 1, 1, 1, 1, 1, '{}', 4),
(41, 7, 'title', 'text', 'Заголовок', 0, 1, 1, 1, 1, 1, '{}', 5),
(42, 7, 'text', 'rich_text_box', 'Текст', 0, 0, 1, 1, 1, 1, '{}', 6),
(43, 7, 'file', 'file', 'Файл', 0, 0, 1, 1, 1, 1, '{}', 7),
(44, 7, 'blocks_page_belongsto_block_relationship', 'relationship', 'Блок', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Block\",\"table\":\"blocks\",\"type\":\"belongsTo\",\"column\":\"block_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"blocks\",\"pivot\":\"0\",\"taggable\":\"0\"}', 8),
(45, 7, 'blocks_page_belongsto_page_relationship', 'relationship', 'pages', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Page\",\"table\":\"pages\",\"type\":\"belongsTo\",\"column\":\"page_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"blocks\",\"pivot\":\"0\",\"taggable\":\"0\"}', 9),
(46, 7, 'svg', 'code_editor', 'Svg-иконка', 0, 0, 1, 1, 1, 1, '{}', 8),
(47, 5, 'image', 'image', 'Image', 0, 1, 1, 1, 1, 1, '{}', 11),
(48, 8, 'id', 'text', 'Id', 1, 1, 0, 0, 0, 0, '{}', 1),
(49, 8, 'first_name', 'text', 'Имя', 1, 1, 1, 1, 1, 1, '{}', 2),
(50, 8, 'last_name', 'text', 'Фамилия', 1, 1, 1, 1, 1, 1, '{}', 3),
(51, 8, 'email', 'text', 'Email', 0, 1, 1, 1, 1, 1, '{}', 4),
(52, 8, 'phones', 'text', 'Телефоны', 0, 0, 1, 1, 1, 1, '{}', 5),
(53, 8, 'department', 'text', 'Отдел', 0, 1, 1, 1, 1, 1, '{}', 6),
(54, 8, 'position', 'text', 'Должность', 0, 1, 1, 1, 1, 1, '{}', 7),
(55, 8, 'description', 'text_area', 'Описание', 0, 0, 1, 1, 1, 1, '{}', 8),
(56, 8, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 9),
(57, 8, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 10),
(58, 8, 'photo', 'image', 'Фото', 0, 1, 1, 1, 1, 1, '{}', 11),
(59, 11, 'id', 'text', 'Id', 1, 1, 0, 0, 0, 0, '{}', 1),
(60, 11, 'slug', 'text', 'Url', 0, 1, 1, 1, 1, 1, '{}', 2),
(61, 11, 'main_image', 'image', 'Главное фото', 0, 1, 1, 1, 1, 1, '{}', 3),
(62, 11, 'images', 'multiple_images', 'Фото', 0, 0, 1, 1, 1, 1, '{}', 4),
(63, 11, 'name', 'text', 'Название', 1, 1, 1, 1, 1, 1, '{}', 5),
(64, 11, 'price', 'number', 'Цена', 1, 1, 1, 1, 1, 1, '{\"step\":0.01,\"min\":0,\"max\":10000000}', 6),
(65, 11, 'price_usd', 'number', 'Цена Usd', 1, 1, 1, 1, 1, 1, '{\"step\":0.01,\"min\":0,\"max\":10000000}', 7),
(66, 11, 'price_m2', 'number', 'Цена за метр', 1, 1, 1, 1, 1, 1, '{\"step\":0.01,\"min\":0,\"max\":100000}', 8),
(67, 11, 'price_m2_usd', 'number', 'Цена за метр Usd', 1, 1, 1, 1, 1, 1, '{\"step\":0.01,\"min\":0,\"max\":100000}', 9),
(68, 11, 'type', 'select_multiple', 'Тип', 0, 1, 1, 1, 1, 1, '{\"default\":\"sell\",\"options\":{\"sell\":\"\\u041f\\u0440\\u043e\\u0434\\u0430\\u0436\\u0430\",\"buy\":\"\\u041f\\u043e\\u043a\\u0443\\u043f\\u043a\\u0430\",\"rent\":\"\\u0410\\u0440\\u0435\\u043d\\u0434\\u0430\",\"change\":\"\\u041e\\u0431\\u043c\\u0435\\u043d\"}}', 10),
(69, 11, 'property_type', 'text', 'Тип недвижимости', 0, 1, 1, 1, 1, 1, '{}', 11),
(70, 11, 'house_type', 'text', 'Тип дома', 0, 0, 1, 1, 1, 1, '{}', 12),
(71, 11, 'rooms', 'number', 'Кол-во комнат', 1, 0, 1, 1, 1, 1, '{\"step\":1,\"min\":0,\"max\":10}', 13),
(72, 11, 'square', 'number', 'Площадь', 1, 0, 1, 1, 1, 1, '{\"step\":0.01,\"min\":0,\"max\":1000}', 14),
(73, 11, 'living_square', 'number', 'Жилая площадь', 1, 0, 1, 1, 1, 1, '{\"step\":0.01,\"min\":0,\"max\":1000}', 15),
(74, 11, 'kitchen_square', 'number', 'Площадь кухни', 1, 0, 1, 1, 1, 1, '{\"step\":0.01,\"min\":0,\"max\":1000}', 16),
(75, 11, 'floor', 'number', 'Этаж', 1, 0, 1, 1, 1, 1, '{\"step\":1,\"min\":1,\"max\":100}', 17),
(76, 11, 'year', 'number', 'Год', 1, 0, 1, 1, 1, 1, '{\"step\":1,\"min\":1900,\"max\":2100}', 18),
(77, 11, 'repair', 'radio_btn', 'Наличие ремонта', 1, 0, 1, 1, 1, 1, '{\"default\":\"0\",\"options\":{\"0\":\"\\u041d\\u0435\\u0442\",\"1\":\"\\u0414\\u0430\"}}', 19),
(78, 11, 'furniture', 'radio_btn', 'Наличие мебели', 1, 0, 1, 1, 1, 1, '{\"default\":\"0\",\"options\":{\"0\":\"\\u041d\\u0435\\u0442\",\"1\":\"\\u0414\\u0430\"}}', 20),
(79, 11, 'title', 'text', 'Заголовок', 0, 1, 1, 1, 1, 1, '{}', 21),
(80, 11, 'description', 'text_area', 'Описание', 0, 0, 1, 1, 1, 1, '{}', 22),
(81, 11, 'content', 'text', 'Содержимое', 0, 0, 1, 1, 1, 1, '{}', 23),
(82, 11, 'address', 'text', 'Адрес', 0, 0, 1, 1, 1, 1, '{}', 24),
(83, 11, 'coordinates', 'coordinates', 'Координаты', 1, 0, 1, 1, 1, 1, '{}', 25),
(84, 11, 'published', 'radio_btn', 'Опубликован?', 1, 1, 1, 1, 1, 1, '{\"default\":\"1\",\"options\":{\"0\":\"\\u041d\\u0435\\u0442\",\"1\":\"\\u0414\\u0430\"}}', 26),
(85, 11, 'created_at', 'timestamp', 'Created At', 0, 0, 1, 1, 0, 1, '{}', 27),
(86, 11, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 28),
(87, 11, 'districts', 'text', 'Район', 0, 0, 1, 1, 1, 1, '{}', 29),
(88, 11, 'bitrix_id', 'text', 'Bitrix Id', 0, 0, 1, 1, 1, 1, '{}', 30),
(89, 11, 'popular', 'radio_btn', 'Популярный (на главной стр)', 1, 0, 1, 1, 1, 1, '{\"default\":\"0\",\"options\":{\"0\":\"\\u041d\\u0435\\u0442\",\"1\":\"\\u0414\\u0430\"}}', 31),
(90, 12, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(91, 12, 'user_id', 'text', 'User Id', 1, 0, 0, 1, 1, 1, '{}', 2),
(92, 12, 'item_type', 'select_dropdown', 'На что отзыв', 1, 1, 1, 1, 1, 1, '{\"default\":\"App\\\\Page\",\"options\":{\"App\\\\Page\":\"\\u0421\\u0442\\u0440\\u0430\\u043d\\u0438\\u0446\\u0430\",\"App\\\\Facility\":\"\\u041e\\u0431\\u044a\\u0435\\u043a\\u0442\",\"App\\\\Employer\":\"\\u0421\\u043e\\u0442\\u0440\\u0443\\u0434\\u043d\\u0438\\u043a\"}}', 3),
(93, 12, 'item_id', 'text', 'Id Страницы, объекта, сотрудника', 1, 1, 1, 1, 1, 1, '{}', 4),
(94, 12, 'text', 'rich_text_box', 'Текст отзыва', 0, 0, 1, 1, 1, 1, '{}', 5),
(95, 12, 'photo', 'image', 'Картинка', 0, 1, 1, 1, 1, 1, '{}', 6),
(96, 12, 'video', 'text', 'Видео (id с youtube)', 0, 1, 1, 1, 1, 1, '{}', 7),
(97, 12, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 8),
(98, 12, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 9),
(99, 12, 'review_belongsto_user_relationship', 'relationship', 'users', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\User\",\"table\":\"users\",\"type\":\"belongsTo\",\"column\":\"user_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"blocks\",\"pivot\":\"0\",\"taggable\":\"0\"}', 10),
(100, 13, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(101, 13, 'object_id', 'text', 'Object Id', 1, 0, 0, 1, 1, 1, '{}', 2),
(102, 13, 'block_id', 'text', 'Block Id', 1, 0, 0, 1, 1, 1, '{}', 3),
(103, 13, 'image', 'image', 'Картинка', 0, 1, 1, 1, 1, 1, '{}', 4),
(104, 13, 'title', 'text', 'Заголовок', 0, 1, 1, 1, 1, 1, '{}', 5),
(105, 13, 'text', 'rich_text_box', 'Текст', 0, 1, 1, 1, 1, 1, '{}', 6),
(106, 13, 'file', 'file', 'Файл', 0, 1, 1, 1, 1, 1, '{}', 7),
(107, 13, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 8),
(108, 13, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 9),
(109, 13, 'blocks_object_belongsto_block_relationship', 'relationship', 'blocks', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Block\",\"table\":\"blocks\",\"type\":\"belongsTo\",\"column\":\"block_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"blocks\",\"pivot\":\"0\",\"taggable\":null}', 10),
(110, 13, 'blocks_object_belongsto_object_relationship', 'relationship', 'objects', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Facility\",\"table\":\"objects\",\"type\":\"belongsTo\",\"column\":\"object_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"blocks\",\"pivot\":\"0\",\"taggable\":null}', 11),
(112, 8, 'employer_belongstomany_object_relationship', 'relationship', 'objects', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Facility\",\"table\":\"objects\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"employer_objects\",\"pivot\":\"1\",\"taggable\":\"0\"}', 12),
(113, 7, 'order', 'number', 'Порядок', 1, 1, 1, 1, 1, 1, '{}', 9);

-- --------------------------------------------------------

--
-- Структура таблицы `data_types`
--

CREATE TABLE `data_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT '0',
  `server_side` tinyint(4) NOT NULL DEFAULT '0',
  `details` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `data_types`
--

INSERT INTO `data_types` (`id`, `name`, `slug`, `display_name_singular`, `display_name_plural`, `icon`, `model_name`, `policy_name`, `controller`, `description`, `generate_permissions`, `server_side`, `details`, `created_at`, `updated_at`) VALUES
(1, 'users', 'users', 'User', 'Users', 'voyager-person', 'TCG\\Voyager\\Models\\User', 'TCG\\Voyager\\Policies\\UserPolicy', 'TCG\\Voyager\\Http\\Controllers\\VoyagerUserController', '', 1, 0, NULL, '2019-08-03 08:40:01', '2019-08-03 08:40:01'),
(2, 'menus', 'menus', 'Menu', 'Menus', 'voyager-list', 'TCG\\Voyager\\Models\\Menu', NULL, '', '', 1, 0, NULL, '2019-08-03 08:40:01', '2019-08-03 08:40:01'),
(3, 'roles', 'roles', 'Role', 'Roles', 'voyager-lock', 'TCG\\Voyager\\Models\\Role', NULL, '', '', 1, 0, NULL, '2019-08-03 08:40:01', '2019-08-03 08:40:01'),
(4, 'blocks', 'blocks', 'Блок', 'Блоки', NULL, 'App\\Block', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2019-08-11 16:41:14', '2019-08-11 16:41:14'),
(5, 'pages', 'pages', 'Страница', 'Страницы', NULL, 'App\\Page', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2019-08-11 16:52:56', '2019-08-23 15:34:02'),
(7, 'blocks_pages', 'blocks-pages', 'Страница-блок', 'Страница-блоки', NULL, 'App\\BlockPage', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2019-08-11 17:15:01', '2019-09-22 12:39:54'),
(8, 'employers', 'employers', 'Сотрудник', 'Сотрудники', NULL, 'App\\Employer', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2019-08-18 06:45:18', '2019-08-23 17:08:34'),
(11, 'objects', 'objects', 'Объект', 'Объекты', NULL, 'App\\Facility', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2019-08-18 07:25:29', '2019-09-29 05:48:12'),
(12, 'reviews', 'reviews', 'Отзыв', 'Отзывы', NULL, 'App\\Review', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2019-08-23 10:36:35', '2019-08-23 16:25:06'),
(13, 'blocks_objects', 'blocks-objects', 'Объект-блок', 'Объект-блоки', NULL, 'App\\BlockObject', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2019-08-23 16:33:45', '2019-08-23 16:37:20');

-- --------------------------------------------------------

--
-- Структура таблицы `employers`
--

CREATE TABLE `employers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phones` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `department` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `position` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `employers`
--

INSERT INTO `employers` (`id`, `first_name`, `last_name`, `email`, `phones`, `department`, `position`, `description`, `created_at`, `updated_at`, `photo`) VALUES
(1, 'Владислав', 'Шишкин', NULL, NULL, NULL, NULL, '17 лет опыта оказания услуг на рынке аренды жилья. Среди наших постоянных               клиентов представители белорусских и иностранных компаний.', '2019-08-18 06:48:00', '2019-08-18 06:50:47', 'employers/August2019/OMrDpp1TsWI03DRYyhor.png'),
(2, 'Tatyana', 'Glazyrina', NULL, NULL, NULL, NULL, 'Все сделки аренды жилья проходят в рамках законодательства РБ. Наши услуги оплачиваются по факту – только после заселения.', '2019-08-18 06:51:00', '2019-08-27 08:16:36', 'employers/August2019/5BX3NJhgGUgAwUc7rz8S.png'),
(3, 'Мария', 'Иванова', NULL, NULL, NULL, NULL, '17 лет опыта оказания услуг на рынке аренды жилья. Среди наших постоянных  клиентов представители белорусских и иностранных компаний.', '2019-08-18 06:52:00', '2019-08-27 08:15:12', 'employers/August2019/2hMOC2dB1NJfEAf9N968.png'),
(4, 'Владислав', 'Шишкин', NULL, NULL, NULL, NULL, '17 лет опыта оказания услуг на рынке аренды жилья. Среди наших постоянных клиентов представители белорусских и иностранных компаний.', '2019-08-18 06:53:00', '2019-08-23 17:08:49', 'employers/August2019/jZ7GizNNfnjBHDWfn3yH.png');

-- --------------------------------------------------------

--
-- Структура таблицы `employer_objects`
--

CREATE TABLE `employer_objects` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `facility_id` bigint(20) UNSIGNED NOT NULL,
  `employer_id` bigint(20) UNSIGNED NOT NULL,
  `chat_script` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `employer_objects`
--

INSERT INTO `employer_objects` (`id`, `facility_id`, `employer_id`, `chat_script`) VALUES
(1, 1, 4, NULL),
(2, 2, 3, NULL),
(3, 3, 2, NULL),
(4, 4, 1, NULL),
(5, 5, 1, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `menus`
--

CREATE TABLE `menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `menus`
--

INSERT INTO `menus` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'admin', '2019-08-03 08:40:03', '2019-08-03 08:40:03');

-- --------------------------------------------------------

--
-- Структура таблицы `menu_items`
--

CREATE TABLE `menu_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `menu_items`
--

INSERT INTO `menu_items` (`id`, `menu_id`, `title`, `url`, `target`, `icon_class`, `color`, `parent_id`, `order`, `created_at`, `updated_at`, `route`, `parameters`) VALUES
(1, 1, 'Dashboard', '', '_self', 'voyager-boat', NULL, NULL, 1, '2019-08-03 08:40:03', '2019-08-03 08:40:03', 'voyager.dashboard', NULL),
(2, 1, 'Media', '', '_self', 'voyager-images', NULL, NULL, 5, '2019-08-03 08:40:03', '2019-08-03 08:40:03', 'voyager.media.index', NULL),
(3, 1, 'Users', '', '_self', 'voyager-person', NULL, NULL, 3, '2019-08-03 08:40:03', '2019-08-03 08:40:03', 'voyager.users.index', NULL),
(4, 1, 'Roles', '', '_self', 'voyager-lock', NULL, NULL, 2, '2019-08-03 08:40:03', '2019-08-03 08:40:03', 'voyager.roles.index', NULL),
(5, 1, 'Tools', '', '_self', 'voyager-tools', NULL, NULL, 9, '2019-08-03 08:40:03', '2019-08-03 08:40:03', NULL, NULL),
(6, 1, 'Menu Builder', '', '_self', 'voyager-list', NULL, 5, 10, '2019-08-03 08:40:03', '2019-08-03 08:40:03', 'voyager.menus.index', NULL),
(7, 1, 'Database', '', '_self', 'voyager-data', NULL, 5, 11, '2019-08-03 08:40:03', '2019-08-03 08:40:03', 'voyager.database.index', NULL),
(8, 1, 'Compass', '', '_self', 'voyager-compass', NULL, 5, 12, '2019-08-03 08:40:03', '2019-08-03 08:40:03', 'voyager.compass.index', NULL),
(9, 1, 'BREAD', '', '_self', 'voyager-bread', NULL, 5, 13, '2019-08-03 08:40:03', '2019-08-03 08:40:03', 'voyager.bread.index', NULL),
(10, 1, 'Settings', '', '_self', 'voyager-settings', NULL, NULL, 14, '2019-08-03 08:40:03', '2019-08-03 08:40:03', 'voyager.settings.index', NULL),
(11, 1, 'Hooks', '', '_self', 'voyager-hook', NULL, 5, 13, '2019-08-03 08:40:07', '2019-08-03 08:40:07', 'voyager.hooks', NULL),
(12, 1, 'Блоки', '', '_self', NULL, NULL, NULL, 15, '2019-08-11 16:41:15', '2019-08-11 16:41:15', 'voyager.blocks.index', NULL),
(13, 1, 'Страницы', '', '_self', NULL, NULL, NULL, 16, '2019-08-11 16:52:56', '2019-08-11 16:52:56', 'voyager.pages.index', NULL),
(14, 1, 'Страница-блоки', '', '_self', NULL, NULL, NULL, 17, '2019-08-11 17:15:01', '2019-08-11 17:15:01', 'voyager.blocks-pages.index', NULL),
(15, 1, 'Сотрудники', '', '_self', NULL, '#000000', NULL, 18, '2019-08-18 06:45:20', '2019-08-18 06:46:31', 'voyager.employers.index', 'null'),
(16, 1, 'Объекты', '', '_self', NULL, NULL, NULL, 19, '2019-08-18 07:25:32', '2019-08-18 07:25:32', 'voyager.objects.index', NULL),
(17, 1, 'Отзывы', '', '_self', NULL, NULL, NULL, 20, '2019-08-23 10:36:35', '2019-08-23 10:36:35', 'voyager.reviews.index', NULL),
(18, 1, 'Объект-блоки', '', '_self', NULL, NULL, NULL, 21, '2019-08-23 16:33:45', '2019-08-23 16:33:45', 'voyager.blocks-objects.index', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_01_01_000000_add_voyager_user_fields', 1),
(4, '2016_01_01_000000_create_data_types_table', 1),
(5, '2016_05_19_173453_create_menu_table', 1),
(6, '2016_10_21_190000_create_roles_table', 1),
(7, '2016_10_21_190000_create_settings_table', 1),
(8, '2016_11_30_135954_create_permission_table', 1),
(9, '2016_11_30_141208_create_permission_role_table', 1),
(10, '2016_12_26_201236_data_types__add__server_side', 1),
(11, '2017_01_13_000000_add_route_to_menu_items_table', 1),
(12, '2017_01_14_005015_create_translations_table', 1),
(13, '2017_01_15_000000_make_table_name_nullable_in_permissions_table', 1),
(14, '2017_03_06_000000_add_controller_to_data_types_table', 1),
(15, '2017_04_21_000000_add_order_to_data_rows_table', 1),
(16, '2017_07_05_210000_add_policyname_to_data_types_table', 1),
(17, '2017_08_05_000000_add_group_to_settings_table', 1),
(18, '2017_11_26_013050_add_user_role_relationship', 1),
(19, '2017_11_26_015000_create_user_roles_table', 1),
(20, '2018_03_11_000000_add_user_settings', 1),
(21, '2018_03_14_000000_add_details_to_data_types_table', 1),
(22, '2018_03_16_000000_make_settings_value_nullable', 1),
(23, '2019_08_10_073229_create_pages_table', 1),
(24, '2019_08_10_075226_create_blocks_table', 1),
(25, '2019_08_10_075810_create_blocks_pages_table', 1),
(26, '2019_08_10_081400_create_employers_table', 1),
(27, '2019_08_10_081829_create_reviews_table', 1),
(28, '2019_08_10_083014_create_saved_forms_table', 1),
(29, '2019_08_10_084158_create_objects_table', 1),
(30, '2019_08_10_092448_create_blocks_objects_table', 1),
(31, '2019_08_10_092605_create_employers_objects_table', 1),
(32, '2019_08_10_093101_edit_objects_table', 1),
(33, '2019_08_18_083924_edit_blocks_pages_table', 2),
(34, '2019_08_18_085024_edit_pages_table', 3),
(35, '2019_08_18_094856_edit_employers_table', 4),
(36, '2019_08_23_194948_edit_employers_object_table', 5),
(37, '2019_09_22_153802_add_order_to_blocks_pages_table', 6);

-- --------------------------------------------------------

--
-- Структура таблицы `objects`
--

CREATE TABLE `objects` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `main_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `images` text COLLATE utf8mb4_unicode_ci,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double NOT NULL DEFAULT '0',
  `price_usd` double NOT NULL DEFAULT '0',
  `price_m2` double NOT NULL DEFAULT '0',
  `price_m2_usd` double NOT NULL DEFAULT '0',
  `type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `property_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'тип недвижимости',
  `house_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rooms` int(11) NOT NULL DEFAULT '1',
  `square` double NOT NULL DEFAULT '0',
  `living_square` double NOT NULL DEFAULT '0',
  `kitchen_square` double NOT NULL DEFAULT '0',
  `floor` int(11) NOT NULL DEFAULT '0',
  `year` int(11) NOT NULL DEFAULT '2000',
  `repair` tinyint(1) NOT NULL DEFAULT '0',
  `furniture` tinyint(1) NOT NULL DEFAULT '0',
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci COMMENT 'описание',
  `address` text COLLATE utf8mb4_unicode_ci,
  `coordinates` point NOT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `districts` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bitrix_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `popular` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `objects`
--

INSERT INTO `objects` (`id`, `slug`, `main_image`, `images`, `name`, `price`, `price_usd`, `price_m2`, `price_m2_usd`, `type`, `property_type`, `house_type`, `rooms`, `square`, `living_square`, `kitchen_square`, `floor`, `year`, `repair`, `furniture`, `title`, `description`, `content`, `address`, `coordinates`, `published`, `created_at`, `updated_at`, `districts`, `bitrix_id`, `popular`) VALUES
(1, '2-komnatnaya', 'objects/August2019/VQbgngdDFb3CRKg0XGYp.jpg', '[\"objects\\/August2019\\/Q5YdALVgg6ZAQRrCfnRu.jpg\",\"objects\\/August2019\\/D5rfvWgExb8kJvpFtpsy.jpg\",\"objects\\/August2019\\/65PeCgmq0T6Yi0brP8sW.jpg\",\"objects\\/August2019\\/OaYYu5Mb9XqdJD0JBRSy.jpg\"]', '2-комнатная квартира премиального класса около вдхр. Дрозды с дизайнерским ремонтом.', 228000, 114000, 1440, 720, '[\"sell\"]', 'Квартира', 'Панельный', 2, 65, 20, 18, 3, 1996, 1, 1, '2-комнатная квартира премиального класса около вдхр. Дрозды с дизайнерским ремонтом.', 'Квартира находится в обыкновенном жилом состоянии, что позволяет при желании сразу заехать и жить или, не переплачивая деньги за чужой ремонт, сделать все по своему вкусу...', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestiae tenetur odio sequi quo quidem labore, reiciendis quod eos atque repellat illo sint quia hic doloribus commodi corporis animi, pariatur nesciunt! Nihil quaerat repellendus veritatis eligendi! Dolorum exercitationem odit consectetur excepturi!  Lorem ipsum dolor sit amet consectetur adipisicing elit. Consequatur ab iure soluta atque iste pariatur sapiente blanditiis eligendi dolorem ut aut animi quis maxime qui, quia quasi. Nihil, dicta. Sequi voluptate architecto numquam quia distinctio iusto obcaecati exercitationem recusandae veritatis cumque, ratione, incidunt necessitatibus illum modi? Corrupti, commodi voluptates quidem repellat nobis perferendis quod laborum ducimus qui voluptatum ipsum pariatur doloremque deserunt quaerat quos obcaecati, labore dolorum! Ullam, enim! Vitae unde ex amet magni laudantium minima est veniam labore explicabo voluptas non dolore accusantium, alias necessitatibus sit a nihil suscipit perspiciatis dolorum provident, natus deserunt earum dicta eligendi. Quos possimus totam consectetur molestiae delectus expedita maiores nemo aspernatur non nam sequi cumque quae deleniti soluta, exercitationem harum! Voluptatum exercitationem excepturi quo eaque in soluta placeat, magnam officia non unde! Atque vero ad temporibus. Possimus blanditiis dolores fuga, ea alias vero, quasi nostrum ducimus nam numquam non autem sint. Error corporis molestias ipsam! Tenetur cum pariatur fugiat est soluta qui ullam repellendus, eum quaerat vitae, illo sequi error adipisci aspernatur! Facilis, inventore ipsam nesciunt labore dicta id debitis blanditiis ex in dolorem ducimus repellat impedit et recusandae sunt voluptatem repudiandae doloremque fugiat consectetur reiciendis! Tempora, excepturi voluptate. Iusto labore quasi atque? Minima iste officia, dolores earum corporis atque qui aliquam dolorem! Blanditiis, in. Incidunt facere officia debitis culpa error, explicabo neque? Culpa, cupiditate molestias. Tenetur id ducimus aspernatur pariatur necessitatibus atque, facere incidunt aperiam aut, optio dolorum ab modi fugiat quas cumque eligendi obcaecati, expedita veritatis voluptatibus. Suscipit blanditiis aliquam quaerat quod natus minus enim nobis unde iste fugit cupiditate quisquam praesentium quasi obcaecati eaque quas, minima saepe perferendis, ullam nemo numquam ab aut dicta id? Iusto esse voluptatibus, qui quidem suscipit adipisci voluptas ut ab fuga, libero, quos saepe placeat! Esse exercitationem eveniet rerum dicta doloremque commodi, placeat animi voluptatum architecto praesentium labore? Aperiam maxime, impedit animi, harum error ipsam, hic expedita architecto tenetur porro earum at et! Consequuntur ipsam amet quos numquam, velit exercitationem eum in ipsa assumenda ab natus dolorum sapiente deserunt soluta corrupti distinctio? Consequuntur ipsam explicabo aspernatur eaque praesentium animi fugit eum sit nisi commodi iure amet magni doloremque iste saepe, deserunt maxime accusantium recusandae. Esse error aliquam quos laboriosam sint unde dolore libero. Fugit nesciunt magni doloribus, beatae, sapiente praesentium rerum, doloremque ea ducimus fugiat repudiandae alias. Voluptatem nisi dolorem ratione expedita inventore pariatur quis blanditiis optio, ullam reiciendis aut tempora necessitatibus similique quod nam accusamus perspiciatis, quos nulla ad minima eius recusandae, autem maxime id. Quas eveniet repudiandae sapiente aliquid nihil? Quis porro obcaecati nemo eligendi vitae nulla illum consequuntur. At consectetur accusamus quibusdam unde veritatis nulla eligendi doloremque eius corrupti consequuntur? Vitae consequuntur dolores animi adipisci, sed autem eveniet magnam praesentium, ab a hic laborum quasi nobis aperiam. Non ducimus doloribus consequuntur facere est adipisci veniam, ipsam iste itaque optio, qui hic. Illo consequatur cum esse fuga! Obcaecati mollitia ex consectetur, veniam, facere amet dicta explicabo blanditiis et asperiores totam quisquam dolorum rerum, provident magni. Quaerat tempora ducimus esse maxime. Nam, provident cupiditate!', 'проспект Победителей, 95к3', 0x000000000101000000e38dcc237f803b4061a6ed5f59f74a40, 1, '2019-08-18 12:26:00', '2019-09-25 03:07:22', 'Центральный', NULL, 1),
(2, '3-komnatnaya1', 'objects/August2019/VQbgngdDFb3CRKg0XGYp.jpg', '[\"objects\\/August2019\\/Q5YdALVgg6ZAQRrCfnRu.jpg\",\"objects\\/August2019\\/D5rfvWgExb8kJvpFtpsy.jpg\",\"objects\\/August2019\\/65PeCgmq0T6Yi0brP8sW.jpg\",\"objects\\/August2019\\/OaYYu5Mb9XqdJD0JBRSy.jpg\"]', '3-комнатная квартира премиального класса около вдхр. Дрозды с дизайнерским ремонтом.', 229000, 114000, 1440, 720, '[\"sell\",\"rent\"]', 'Квартира', 'Панельный', 3, 65, 20, 18, 3, 1996, 1, 1, '2-комнатная квартира премиального класса около вдхр. Дрозды с дизайнерским ремонтом.', 'Квартира находится в обыкновенном жилом состоянии, что позволяет при желании сразу заехать и жить или, не переплачивая деньги за чужой ремонт, сделать все по своему вкусу...', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestiae tenetur odio sequi quo quidem labore, reiciendis quod eos atque repellat illo sint quia hic doloribus commodi corporis animi, pariatur nesciunt! Nihil quaerat repellendus veritatis eligendi! Dolorum exercitationem odit consectetur excepturi!  Lorem ipsum dolor sit amet consectetur adipisicing elit. Consequatur ab iure soluta atque iste pariatur sapiente blanditiis eligendi dolorem ut aut animi quis maxime qui, quia quasi. Nihil, dicta. Sequi voluptate architecto numquam quia distinctio iusto obcaecati exercitationem recusandae veritatis cumque, ratione, incidunt necessitatibus illum modi? Corrupti, commodi voluptates quidem repellat nobis perferendis quod laborum ducimus qui voluptatum ipsum pariatur doloremque deserunt quaerat quos obcaecati, labore dolorum! Ullam, enim! Vitae unde ex amet magni laudantium minima est veniam labore explicabo voluptas non dolore accusantium, alias necessitatibus sit a nihil suscipit perspiciatis dolorum provident, natus deserunt earum dicta eligendi. Quos possimus totam consectetur molestiae delectus expedita maiores nemo aspernatur non nam sequi cumque quae deleniti soluta, exercitationem harum! Voluptatum exercitationem excepturi quo eaque in soluta placeat, magnam officia non unde! Atque vero ad temporibus. Possimus blanditiis dolores fuga, ea alias vero, quasi nostrum ducimus nam numquam non autem sint. Error corporis molestias ipsam! Tenetur cum pariatur fugiat est soluta qui ullam repellendus, eum quaerat vitae, illo sequi error adipisci aspernatur! Facilis, inventore ipsam nesciunt labore dicta id debitis blanditiis ex in dolorem ducimus repellat impedit et recusandae sunt voluptatem repudiandae doloremque fugiat consectetur reiciendis! Tempora, excepturi voluptate. Iusto labore quasi atque? Minima iste officia, dolores earum corporis atque qui aliquam dolorem! Blanditiis, in. Incidunt facere officia debitis culpa error, explicabo neque? Culpa, cupiditate molestias. Tenetur id ducimus aspernatur pariatur necessitatibus atque, facere incidunt aperiam aut, optio dolorum ab modi fugiat quas cumque eligendi obcaecati, expedita veritatis voluptatibus. Suscipit blanditiis aliquam quaerat quod natus minus enim nobis unde iste fugit cupiditate quisquam praesentium quasi obcaecati eaque quas, minima saepe perferendis, ullam nemo numquam ab aut dicta id? Iusto esse voluptatibus, qui quidem suscipit adipisci voluptas ut ab fuga, libero, quos saepe placeat! Esse exercitationem eveniet rerum dicta doloremque commodi, placeat animi voluptatum architecto praesentium labore? Aperiam maxime, impedit animi, harum error ipsam, hic expedita architecto tenetur porro earum at et! Consequuntur ipsam amet quos numquam, velit exercitationem eum in ipsa assumenda ab natus dolorum sapiente deserunt soluta corrupti distinctio? Consequuntur ipsam explicabo aspernatur eaque praesentium animi fugit eum sit nisi commodi iure amet magni doloremque iste saepe, deserunt maxime accusantium recusandae. Esse error aliquam quos laboriosam sint unde dolore libero. Fugit nesciunt magni doloribus, beatae, sapiente praesentium rerum, doloremque ea ducimus fugiat repudiandae alias. Voluptatem nisi dolorem ratione expedita inventore pariatur quis blanditiis optio, ullam reiciendis aut tempora necessitatibus similique quod nam accusamus perspiciatis, quos nulla ad minima eius recusandae, autem maxime id. Quas eveniet repudiandae sapiente aliquid nihil? Quis porro obcaecati nemo eligendi vitae nulla illum consequuntur. At consectetur accusamus quibusdam unde veritatis nulla eligendi doloremque eius corrupti consequuntur? Vitae consequuntur dolores animi adipisci, sed autem eveniet magnam praesentium, ab a hic laborum quasi nobis aperiam. Non ducimus doloribus consequuntur facere est adipisci veniam, ipsam iste itaque optio, qui hic. Illo consequatur cum esse fuga! Obcaecati mollitia ex consectetur, veniam, facere amet dicta explicabo blanditiis et asperiores totam quisquam dolorum rerum, provident magni. Quaerat tempora ducimus esse maxime. Nam, provident cupiditate!', 'проспект Победителей, 95к3', 0x000000000101000000e38dcc237f803b4061a6ed5f59f74a40, 1, '2019-08-18 12:26:00', '2019-09-25 03:05:21', 'Центральный', NULL, 1),
(3, '1-komnatnaya1', 'objects/August2019/VQbgngdDFb3CRKg0XGYp.jpg', '[\"objects\\/August2019\\/Q5YdALVgg6ZAQRrCfnRu.jpg\",\"objects\\/August2019\\/D5rfvWgExb8kJvpFtpsy.jpg\",\"objects\\/August2019\\/65PeCgmq0T6Yi0brP8sW.jpg\",\"objects\\/August2019\\/OaYYu5Mb9XqdJD0JBRSy.jpg\"]', '1-комнатная квартира премиального класса около вдхр. Дрозды с дизайнерским ремонтом.', 195000, 97000, 1440, 720, '[\"sell\",\"rent\"]', 'Квартира', 'Панельный', 1, 50, 10, 18, 3, 1998, 1, 1, '2-комнатная квартира премиального класса около вдхр. Дрозды с дизайнерским ремонтом.', 'Квартира находится в обыкновенном жилом состоянии, что позволяет при желании сразу заехать и жить или, не переплачивая деньги за чужой ремонт, сделать все по своему вкусу...', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestiae tenetur odio sequi quo quidem labore, reiciendis quod eos atque repellat illo sint quia hic doloribus commodi corporis animi, pariatur nesciunt! Nihil quaerat repellendus veritatis eligendi! Dolorum exercitationem odit consectetur excepturi!  Lorem ipsum dolor sit amet consectetur adipisicing elit. Consequatur ab iure soluta atque iste pariatur sapiente blanditiis eligendi dolorem ut aut animi quis maxime qui, quia quasi. Nihil, dicta. Sequi voluptate architecto numquam quia distinctio iusto obcaecati exercitationem recusandae veritatis cumque, ratione, incidunt necessitatibus illum modi? Corrupti, commodi voluptates quidem repellat nobis perferendis quod laborum ducimus qui voluptatum ipsum pariatur doloremque deserunt quaerat quos obcaecati, labore dolorum! Ullam, enim! Vitae unde ex amet magni laudantium minima est veniam labore explicabo voluptas non dolore accusantium, alias necessitatibus sit a nihil suscipit perspiciatis dolorum provident, natus deserunt earum dicta eligendi. Quos possimus totam consectetur molestiae delectus expedita maiores nemo aspernatur non nam sequi cumque quae deleniti soluta, exercitationem harum! Voluptatum exercitationem excepturi quo eaque in soluta placeat, magnam officia non unde! Atque vero ad temporibus. Possimus blanditiis dolores fuga, ea alias vero, quasi nostrum ducimus nam numquam non autem sint. Error corporis molestias ipsam! Tenetur cum pariatur fugiat est soluta qui ullam repellendus, eum quaerat vitae, illo sequi error adipisci aspernatur! Facilis, inventore ipsam nesciunt labore dicta id debitis blanditiis ex in dolorem ducimus repellat impedit et recusandae sunt voluptatem repudiandae doloremque fugiat consectetur reiciendis! Tempora, excepturi voluptate. Iusto labore quasi atque? Minima iste officia, dolores earum corporis atque qui aliquam dolorem! Blanditiis, in. Incidunt facere officia debitis culpa error, explicabo neque? Culpa, cupiditate molestias. Tenetur id ducimus aspernatur pariatur necessitatibus atque, facere incidunt aperiam aut, optio dolorum ab modi fugiat quas cumque eligendi obcaecati, expedita veritatis voluptatibus. Suscipit blanditiis aliquam quaerat quod natus minus enim nobis unde iste fugit cupiditate quisquam praesentium quasi obcaecati eaque quas, minima saepe perferendis, ullam nemo numquam ab aut dicta id? Iusto esse voluptatibus, qui quidem suscipit adipisci voluptas ut ab fuga, libero, quos saepe placeat! Esse exercitationem eveniet rerum dicta doloremque commodi, placeat animi voluptatum architecto praesentium labore? Aperiam maxime, impedit animi, harum error ipsam, hic expedita architecto tenetur porro earum at et! Consequuntur ipsam amet quos numquam, velit exercitationem eum in ipsa assumenda ab natus dolorum sapiente deserunt soluta corrupti distinctio? Consequuntur ipsam explicabo aspernatur eaque praesentium animi fugit eum sit nisi commodi iure amet magni doloremque iste saepe, deserunt maxime accusantium recusandae. Esse error aliquam quos laboriosam sint unde dolore libero. Fugit nesciunt magni doloribus, beatae, sapiente praesentium rerum, doloremque ea ducimus fugiat repudiandae alias. Voluptatem nisi dolorem ratione expedita inventore pariatur quis blanditiis optio, ullam reiciendis aut tempora necessitatibus similique quod nam accusamus perspiciatis, quos nulla ad minima eius recusandae, autem maxime id. Quas eveniet repudiandae sapiente aliquid nihil? Quis porro obcaecati nemo eligendi vitae nulla illum consequuntur. At consectetur accusamus quibusdam unde veritatis nulla eligendi doloremque eius corrupti consequuntur? Vitae consequuntur dolores animi adipisci, sed autem eveniet magnam praesentium, ab a hic laborum quasi nobis aperiam. Non ducimus doloribus consequuntur facere est adipisci veniam, ipsam iste itaque optio, qui hic. Illo consequatur cum esse fuga! Obcaecati mollitia ex consectetur, veniam, facere amet dicta explicabo blanditiis et asperiores totam quisquam dolorum rerum, provident magni. Quaerat tempora ducimus esse maxime. Nam, provident cupiditate!', 'проспект Победителей, 94к3', 0x00000000010100000008af5ed98d793b40607da2a676f84a40, 1, '2019-08-18 12:26:00', '2019-09-02 12:03:27', 'Центральный', NULL, 1),
(4, '4-komnatnaya1', 'objects/August2019/VQbgngdDFb3CRKg0XGYp.jpg', '[\"objects\\/August2019\\/Q5YdALVgg6ZAQRrCfnRu.jpg\",\"objects\\/August2019\\/D5rfvWgExb8kJvpFtpsy.jpg\",\"objects\\/August2019\\/65PeCgmq0T6Yi0brP8sW.jpg\",\"objects\\/August2019\\/OaYYu5Mb9XqdJD0JBRSy.jpg\"]', '1-комнатная квартира премиального класса около вдхр. Дрозды с дизайнерским ремонтом.', 350000, 175000, 1440, 720, '[\"sell\",\"buy\"]', 'Квартира', 'Панельный', 4, 50, 10, 18, 3, 1998, 1, 1, '4-комнатная квартира премиального класса около вдхр. Дрозды с дизайнерским ремонтом.', 'Квартира находится в обыкновенном жилом состоянии, что позволяет при желании сразу заехать и жить или, не переплачивая деньги за чужой ремонт, сделать все по своему вкусу...', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestiae tenetur odio sequi quo quidem labore, reiciendis quod eos atque repellat illo sint quia hic doloribus commodi corporis animi, pariatur nesciunt! Nihil quaerat repellendus veritatis eligendi! Dolorum exercitationem odit consectetur excepturi!  Lorem ipsum dolor sit amet consectetur adipisicing elit. Consequatur ab iure soluta atque iste pariatur sapiente blanditiis eligendi dolorem ut aut animi quis maxime qui, quia quasi. Nihil, dicta. Sequi voluptate architecto numquam quia distinctio iusto obcaecati exercitationem recusandae veritatis cumque, ratione, incidunt necessitatibus illum modi? Corrupti, commodi voluptates quidem repellat nobis perferendis quod laborum ducimus qui voluptatum ipsum pariatur doloremque deserunt quaerat quos obcaecati, labore dolorum! Ullam, enim! Vitae unde ex amet magni laudantium minima est veniam labore explicabo voluptas non dolore accusantium, alias necessitatibus sit a nihil suscipit perspiciatis dolorum provident, natus deserunt earum dicta eligendi. Quos possimus totam consectetur molestiae delectus expedita maiores nemo aspernatur non nam sequi cumque quae deleniti soluta, exercitationem harum! Voluptatum exercitationem excepturi quo eaque in soluta placeat, magnam officia non unde! Atque vero ad temporibus. Possimus blanditiis dolores fuga, ea alias vero, quasi nostrum ducimus nam numquam non autem sint. Error corporis molestias ipsam! Tenetur cum pariatur fugiat est soluta qui ullam repellendus, eum quaerat vitae, illo sequi error adipisci aspernatur! Facilis, inventore ipsam nesciunt labore dicta id debitis blanditiis ex in dolorem ducimus repellat impedit et recusandae sunt voluptatem repudiandae doloremque fugiat consectetur reiciendis! Tempora, excepturi voluptate. Iusto labore quasi atque? Minima iste officia, dolores earum corporis atque qui aliquam dolorem! Blanditiis, in. Incidunt facere officia debitis culpa error, explicabo neque? Culpa, cupiditate molestias. Tenetur id ducimus aspernatur pariatur necessitatibus atque, facere incidunt aperiam aut, optio dolorum ab modi fugiat quas cumque eligendi obcaecati, expedita veritatis voluptatibus. Suscipit blanditiis aliquam quaerat quod natus minus enim nobis unde iste fugit cupiditate quisquam praesentium quasi obcaecati eaque quas, minima saepe perferendis, ullam nemo numquam ab aut dicta id? Iusto esse voluptatibus, qui quidem suscipit adipisci voluptas ut ab fuga, libero, quos saepe placeat! Esse exercitationem eveniet rerum dicta doloremque commodi, placeat animi voluptatum architecto praesentium labore? Aperiam maxime, impedit animi, harum error ipsam, hic expedita architecto tenetur porro earum at et! Consequuntur ipsam amet quos numquam, velit exercitationem eum in ipsa assumenda ab natus dolorum sapiente deserunt soluta corrupti distinctio? Consequuntur ipsam explicabo aspernatur eaque praesentium animi fugit eum sit nisi commodi iure amet magni doloremque iste saepe, deserunt maxime accusantium recusandae. Esse error aliquam quos laboriosam sint unde dolore libero. Fugit nesciunt magni doloribus, beatae, sapiente praesentium rerum, doloremque ea ducimus fugiat repudiandae alias. Voluptatem nisi dolorem ratione expedita inventore pariatur quis blanditiis optio, ullam reiciendis aut tempora necessitatibus similique quod nam accusamus perspiciatis, quos nulla ad minima eius recusandae, autem maxime id. Quas eveniet repudiandae sapiente aliquid nihil? Quis porro obcaecati nemo eligendi vitae nulla illum consequuntur. At consectetur accusamus quibusdam unde veritatis nulla eligendi doloremque eius corrupti consequuntur? Vitae consequuntur dolores animi adipisci, sed autem eveniet magnam praesentium, ab a hic laborum quasi nobis aperiam. Non ducimus doloribus consequuntur facere est adipisci veniam, ipsam iste itaque optio, qui hic. Illo consequatur cum esse fuga! Obcaecati mollitia ex consectetur, veniam, facere amet dicta explicabo blanditiis et asperiores totam quisquam dolorum rerum, provident magni. Quaerat tempora ducimus esse maxime. Nam, provident cupiditate!', 'проспект Победителей, 91к3', 0x000000000101000000e9297387d57a3b40607da2a676f84a40, 1, '2019-08-18 12:26:00', '2019-09-02 12:02:57', 'Центральный', NULL, 1),
(5, '2-komnatnaya1', 'objects/August2019/VQbgngdDFb3CRKg0XGYp.jpg', '[\"objects\\/August2019\\/Q5YdALVgg6ZAQRrCfnRu.jpg\",\"objects\\/August2019\\/D5rfvWgExb8kJvpFtpsy.jpg\",\"objects\\/August2019\\/65PeCgmq0T6Yi0brP8sW.jpg\",\"objects\\/August2019\\/OaYYu5Mb9XqdJD0JBRSy.jpg\"]', '2-комнатная квартира премиального класса около вдхр. Дрозды с дизайнерским ремонтом.', 240000, 120000, 1440, 720, '[\"buy\"]', 'Квартира', 'Панельный', 2, 50, 10, 18, 5, 1998, 1, 0, '4-комнатная квартира премиального класса около вдхр. Дрозды с дизайнерским ремонтом.', 'Квартира находится в обыкновенном жилом состоянии, что позволяет при желании сразу заехать и жить или, не переплачивая деньги за чужой ремонт, сделать все по своему вкусу...', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestiae tenetur odio sequi quo quidem labore, reiciendis quod eos atque repellat illo sint quia hic doloribus commodi corporis animi, pariatur nesciunt! Nihil quaerat repellendus veritatis eligendi! Dolorum exercitationem odit consectetur excepturi!  Lorem ipsum dolor sit amet consectetur adipisicing elit. Consequatur ab iure soluta atque iste pariatur sapiente blanditiis eligendi dolorem ut aut animi quis maxime qui, quia quasi. Nihil, dicta. Sequi voluptate architecto numquam quia distinctio iusto obcaecati exercitationem recusandae veritatis cumque, ratione, incidunt necessitatibus illum modi? Corrupti, commodi voluptates quidem repellat nobis perferendis quod laborum ducimus qui voluptatum ipsum pariatur doloremque deserunt quaerat quos obcaecati, labore dolorum! Ullam, enim! Vitae unde ex amet magni laudantium minima est veniam labore explicabo voluptas non dolore accusantium, alias necessitatibus sit a nihil suscipit perspiciatis dolorum provident, natus deserunt earum dicta eligendi. Quos possimus totam consectetur molestiae delectus expedita maiores nemo aspernatur non nam sequi cumque quae deleniti soluta, exercitationem harum! Voluptatum exercitationem excepturi quo eaque in soluta placeat, magnam officia non unde! Atque vero ad temporibus. Possimus blanditiis dolores fuga, ea alias vero, quasi nostrum ducimus nam numquam non autem sint. Error corporis molestias ipsam! Tenetur cum pariatur fugiat est soluta qui ullam repellendus, eum quaerat vitae, illo sequi error adipisci aspernatur! Facilis, inventore ipsam nesciunt labore dicta id debitis blanditiis ex in dolorem ducimus repellat impedit et recusandae sunt voluptatem repudiandae doloremque fugiat consectetur reiciendis! Tempora, excepturi voluptate. Iusto labore quasi atque? Minima iste officia, dolores earum corporis atque qui aliquam dolorem! Blanditiis, in. Incidunt facere officia debitis culpa error, explicabo neque? Culpa, cupiditate molestias. Tenetur id ducimus aspernatur pariatur necessitatibus atque, facere incidunt aperiam aut, optio dolorum ab modi fugiat quas cumque eligendi obcaecati, expedita veritatis voluptatibus. Suscipit blanditiis aliquam quaerat quod natus minus enim nobis unde iste fugit cupiditate quisquam praesentium quasi obcaecati eaque quas, minima saepe perferendis, ullam nemo numquam ab aut dicta id? Iusto esse voluptatibus, qui quidem suscipit adipisci voluptas ut ab fuga, libero, quos saepe placeat! Esse exercitationem eveniet rerum dicta doloremque commodi, placeat animi voluptatum architecto praesentium labore? Aperiam maxime, impedit animi, harum error ipsam, hic expedita architecto tenetur porro earum at et! Consequuntur ipsam amet quos numquam, velit exercitationem eum in ipsa assumenda ab natus dolorum sapiente deserunt soluta corrupti distinctio? Consequuntur ipsam explicabo aspernatur eaque praesentium animi fugit eum sit nisi commodi iure amet magni doloremque iste saepe, deserunt maxime accusantium recusandae. Esse error aliquam quos laboriosam sint unde dolore libero. Fugit nesciunt magni doloribus, beatae, sapiente praesentium rerum, doloremque ea ducimus fugiat repudiandae alias. Voluptatem nisi dolorem ratione expedita inventore pariatur quis blanditiis optio, ullam reiciendis aut tempora necessitatibus similique quod nam accusamus perspiciatis, quos nulla ad minima eius recusandae, autem maxime id. Quas eveniet repudiandae sapiente aliquid nihil? Quis porro obcaecati nemo eligendi vitae nulla illum consequuntur. At consectetur accusamus quibusdam unde veritatis nulla eligendi doloremque eius corrupti consequuntur? Vitae consequuntur dolores animi adipisci, sed autem eveniet magnam praesentium, ab a hic laborum quasi nobis aperiam. Non ducimus doloribus consequuntur facere est adipisci veniam, ipsam iste itaque optio, qui hic. Illo consequatur cum esse fuga! Obcaecati mollitia ex consectetur, veniam, facere amet dicta explicabo blanditiis et asperiores totam quisquam dolorum rerum, provident magni. Quaerat tempora ducimus esse maxime. Nam, provident cupiditate!', 'проспект Победителей, 99к3', 0x0000000001010000005d2973873d7c3b40785b0ce583f84a40, 1, '2019-08-18 12:26:00', '2019-08-18 12:39:27', 'Центральный', NULL, 1),
(15, 'test-1', 'objects/August2019/VQbgngdDFb3CRKg0XGYp.jpg', '[\"objects\\/August2019\\/Q5YdALVgg6ZAQRrCfnRu.jpg\",\"objects\\/August2019\\/D5rfvWgExb8kJvpFtpsy.jpg\",\"objects\\/August2019\\/65PeCgmq0T6Yi0brP8sW.jpg\",\"objects\\/August2019\\/OaYYu5Mb9XqdJD0JBRSy.jpg\"]', '4-комнатная квартира премиального класса около вдхр. Дрозды с дизайнерским ремонтом.', 250000, 110000, 1400, 720, '[\"buy\"]', 'Квартира', 'Панельный', 4, 50, 10, 18, 5, 1986, 1, 0, '4-комнатная квартира премиального класса около вдхр. Дрозды с дизайнерским ремонтом.', 'Квартира находится в обыкновенном жилом состоянии, что позволяет при желании сразу заехать и жить или, не переплачивая деньги за чужой ремонт, сделать все по своему вкусу...', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestiae tenetur odio sequi quo quidem labore, reiciendis quod eos atque repellat illo sint quia hic doloribus commodi corporis animi, pariatur nesciunt! Nihil quaerat repellendus veritatis eligendi! Dolorum exercitationem odit consectetur excepturi!  Lorem ipsum dolor sit amet consectetur adipisicing elit. Consequatur ab iure soluta atque iste pariatur sapiente blanditiis eligendi dolorem ut aut animi quis maxime qui, quia quasi. Nihil, dicta. Sequi voluptate architecto numquam quia distinctio iusto obcaecati exercitationem recusandae veritatis cumque, ratione, incidunt necessitatibus illum modi? Corrupti, commodi voluptates quidem repellat nobis perferendis quod laborum ducimus qui voluptatum ipsum pariatur doloremque deserunt quaerat quos obcaecati, labore dolorum! Ullam, enim! Vitae unde ex amet magni laudantium minima est veniam labore explicabo voluptas non dolore accusantium, alias necessitatibus sit a nihil suscipit perspiciatis dolorum provident, natus deserunt earum dicta eligendi. Quos possimus totam consectetur molestiae delectus expedita maiores nemo aspernatur non nam sequi cumque quae deleniti soluta, exercitationem harum! Voluptatum exercitationem excepturi quo eaque in soluta placeat, magnam officia non unde! Atque vero ad temporibus. Possimus blanditiis dolores fuga, ea alias vero, quasi nostrum ducimus nam numquam non autem sint. Error corporis molestias ipsam! Tenetur cum pariatur fugiat est soluta qui ullam repellendus, eum quaerat vitae, illo sequi error adipisci aspernatur! Facilis, inventore ipsam nesciunt labore dicta id debitis blanditiis ex in dolorem ducimus repellat impedit et recusandae sunt voluptatem repudiandae doloremque fugiat consectetur reiciendis! Tempora, excepturi voluptate. Iusto labore quasi atque? Minima iste officia, dolores earum corporis atque qui aliquam dolorem! Blanditiis, in. Incidunt facere officia debitis culpa error, explicabo neque? Culpa, cupiditate molestias. Tenetur id ducimus aspernatur pariatur necessitatibus atque, facere incidunt aperiam aut, optio dolorum ab modi fugiat quas cumque eligendi obcaecati, expedita veritatis voluptatibus. Suscipit blanditiis aliquam quaerat quod natus minus enim nobis unde iste fugit cupiditate quisquam praesentium quasi obcaecati eaque quas, minima saepe perferendis, ullam nemo numquam ab aut dicta id? Iusto esse voluptatibus, qui quidem suscipit adipisci voluptas ut ab fuga, libero, quos saepe placeat! Esse exercitationem eveniet rerum dicta doloremque commodi, placeat animi voluptatum architecto praesentium labore? Aperiam maxime, impedit animi, harum error ipsam, hic expedita architecto tenetur porro earum at et! Consequuntur ipsam amet quos numquam, velit exercitationem eum in ipsa assumenda ab natus dolorum sapiente deserunt soluta corrupti distinctio? Consequuntur ipsam explicabo aspernatur eaque praesentium animi fugit eum sit nisi commodi iure amet magni doloremque iste saepe, deserunt maxime accusantium recusandae. Esse error aliquam quos laboriosam sint unde dolore libero. Fugit nesciunt magni doloribus, beatae, sapiente praesentium rerum, doloremque ea ducimus fugiat repudiandae alias. Voluptatem nisi dolorem ratione expedita inventore pariatur quis blanditiis optio, ullam reiciendis aut tempora necessitatibus similique quod nam accusamus perspiciatis, quos nulla ad minima eius recusandae, autem maxime id. Quas eveniet repudiandae sapiente aliquid nihil? Quis porro obcaecati nemo eligendi vitae nulla illum consequuntur. At consectetur accusamus quibusdam unde veritatis nulla eligendi doloremque eius corrupti consequuntur? Vitae consequuntur dolores animi adipisci, sed autem eveniet magnam praesentium, ab a hic laborum quasi nobis aperiam. Non ducimus doloribus consequuntur facere est adipisci veniam, ipsam iste itaque optio, qui hic. Illo consequatur cum esse fuga! Obcaecati mollitia ex consectetur, veniam, facere amet dicta explicabo blanditiis et asperiores totam quisquam dolorum rerum, provident magni. Quaerat tempora ducimus esse maxime. Nam, provident cupiditate!', 'проспект Победителей, 99к3', 0x0000000001010000005d2973873d7c3b40785b0ce583f84a40, 1, '2019-08-18 12:26:00', '2019-08-18 12:39:27', 'Центральный', NULL, 0),
(16, 'test-2', 'objects/August2019/VQbgngdDFb3CRKg0XGYp.jpg', '[\"objects\\/August2019\\/Q5YdALVgg6ZAQRrCfnRu.jpg\",\"objects\\/August2019\\/D5rfvWgExb8kJvpFtpsy.jpg\",\"objects\\/August2019\\/65PeCgmq0T6Yi0brP8sW.jpg\",\"objects\\/August2019\\/OaYYu5Mb9XqdJD0JBRSy.jpg\"]', '4-комнатная квартира премиального класса около вдхр. Дрозды с дизайнерским ремонтом.', 280000, 115000, 1470, 620, '[\"buy\",\"rent\"]', 'Квартира', 'Панельный', 3, 50, 23, 18, 4, 1988, 1, 0, '4-комнатная квартира премиального класса около вдхр. Дрозды с дизайнерским ремонтом.', 'Квартира находится в обыкновенном жилом состоянии, что позволяет при желании сразу заехать и жить или, не переплачивая деньги за чужой ремонт, сделать все по своему вкусу...', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestiae tenetur odio sequi quo quidem labore, reiciendis quod eos atque repellat illo sint quia hic doloribus commodi corporis animi, pariatur nesciunt! Nihil quaerat repellendus veritatis eligendi! Dolorum exercitationem odit consectetur excepturi!  Lorem ipsum dolor sit amet consectetur adipisicing elit. Consequatur ab iure soluta atque iste pariatur sapiente blanditiis eligendi dolorem ut aut animi quis maxime qui, quia quasi. Nihil, dicta. Sequi voluptate architecto numquam quia distinctio iusto obcaecati exercitationem recusandae veritatis cumque, ratione, incidunt necessitatibus illum modi? Corrupti, commodi voluptates quidem repellat nobis perferendis quod laborum ducimus qui voluptatum ipsum pariatur doloremque deserunt quaerat quos obcaecati, labore dolorum! Ullam, enim! Vitae unde ex amet magni laudantium minima est veniam labore explicabo voluptas non dolore accusantium, alias necessitatibus sit a nihil suscipit perspiciatis dolorum provident, natus deserunt earum dicta eligendi. Quos possimus totam consectetur molestiae delectus expedita maiores nemo aspernatur non nam sequi cumque quae deleniti soluta, exercitationem harum! Voluptatum exercitationem excepturi quo eaque in soluta placeat, magnam officia non unde! Atque vero ad temporibus. Possimus blanditiis dolores fuga, ea alias vero, quasi nostrum ducimus nam numquam non autem sint. Error corporis molestias ipsam! Tenetur cum pariatur fugiat est soluta qui ullam repellendus, eum quaerat vitae, illo sequi error adipisci aspernatur! Facilis, inventore ipsam nesciunt labore dicta id debitis blanditiis ex in dolorem ducimus repellat impedit et recusandae sunt voluptatem repudiandae doloremque fugiat consectetur reiciendis! Tempora, excepturi voluptate. Iusto labore quasi atque? Minima iste officia, dolores earum corporis atque qui aliquam dolorem! Blanditiis, in. Incidunt facere officia debitis culpa error, explicabo neque? Culpa, cupiditate molestias. Tenetur id ducimus aspernatur pariatur necessitatibus atque, facere incidunt aperiam aut, optio dolorum ab modi fugiat quas cumque eligendi obcaecati, expedita veritatis voluptatibus. Suscipit blanditiis aliquam quaerat quod natus minus enim nobis unde iste fugit cupiditate quisquam praesentium quasi obcaecati eaque quas, minima saepe perferendis, ullam nemo numquam ab aut dicta id? Iusto esse voluptatibus, qui quidem suscipit adipisci voluptas ut ab fuga, libero, quos saepe placeat! Esse exercitationem eveniet rerum dicta doloremque commodi, placeat animi voluptatum architecto praesentium labore? Aperiam maxime, impedit animi, harum error ipsam, hic expedita architecto tenetur porro earum at et! Consequuntur ipsam amet quos numquam, velit exercitationem eum in ipsa assumenda ab natus dolorum sapiente deserunt soluta corrupti distinctio? Consequuntur ipsam explicabo aspernatur eaque praesentium animi fugit eum sit nisi commodi iure amet magni doloremque iste saepe, deserunt maxime accusantium recusandae. Esse error aliquam quos laboriosam sint unde dolore libero. Fugit nesciunt magni doloribus, beatae, sapiente praesentium rerum, doloremque ea ducimus fugiat repudiandae alias. Voluptatem nisi dolorem ratione expedita inventore pariatur quis blanditiis optio, ullam reiciendis aut tempora necessitatibus similique quod nam accusamus perspiciatis, quos nulla ad minima eius recusandae, autem maxime id. Quas eveniet repudiandae sapiente aliquid nihil? Quis porro obcaecati nemo eligendi vitae nulla illum consequuntur. At consectetur accusamus quibusdam unde veritatis nulla eligendi doloremque eius corrupti consequuntur? Vitae consequuntur dolores animi adipisci, sed autem eveniet magnam praesentium, ab a hic laborum quasi nobis aperiam. Non ducimus doloribus consequuntur facere est adipisci veniam, ipsam iste itaque optio, qui hic. Illo consequatur cum esse fuga! Obcaecati mollitia ex consectetur, veniam, facere amet dicta explicabo blanditiis et asperiores totam quisquam dolorum rerum, provident magni. Quaerat tempora ducimus esse maxime. Nam, provident cupiditate!', 'проспект Победителей, 99к3', 0x000000000101000000922973874a8d3b4045d16d2391f84a40, 1, '2019-08-18 12:26:00', '2019-09-04 16:27:17', 'Центральный', NULL, 0),
(17, 'test-3', 'objects/August2019/VQbgngdDFb3CRKg0XGYp.jpg', '[\"objects\\/August2019\\/Q5YdALVgg6ZAQRrCfnRu.jpg\",\"objects\\/August2019\\/D5rfvWgExb8kJvpFtpsy.jpg\",\"objects\\/August2019\\/65PeCgmq0T6Yi0brP8sW.jpg\",\"objects\\/August2019\\/OaYYu5Mb9XqdJD0JBRSy.jpg\"]', '4-комнатная квартира премиального класса около вдхр. Дрозды с дизайнерским ремонтом.', 284000, 100000, 1570, 720, '[\"buy\"]', 'Квартира', 'Кирпичный', 3, 72, 45, 17, 1, 1975, 1, 0, '4-комнатная квартира премиального класса около вдхр. Дрозды с дизайнерским ремонтом.', 'Квартира находится в обыкновенном жилом состоянии, что позволяет при желании сразу заехать и жить или, не переплачивая деньги за чужой ремонт, сделать все по своему вкусу...', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestiae tenetur odio sequi quo quidem labore, reiciendis quod eos atque repellat illo sint quia hic doloribus commodi corporis animi, pariatur nesciunt! Nihil quaerat repellendus veritatis eligendi! Dolorum exercitationem odit consectetur excepturi!  Lorem ipsum dolor sit amet consectetur adipisicing elit. Consequatur ab iure soluta atque iste pariatur sapiente blanditiis eligendi dolorem ut aut animi quis maxime qui, quia quasi. Nihil, dicta. Sequi voluptate architecto numquam quia distinctio iusto obcaecati exercitationem recusandae veritatis cumque, ratione, incidunt necessitatibus illum modi? Corrupti, commodi voluptates quidem repellat nobis perferendis quod laborum ducimus qui voluptatum ipsum pariatur doloremque deserunt quaerat quos obcaecati, labore dolorum! Ullam, enim! Vitae unde ex amet magni laudantium minima est veniam labore explicabo voluptas non dolore accusantium, alias necessitatibus sit a nihil suscipit perspiciatis dolorum provident, natus deserunt earum dicta eligendi. Quos possimus totam consectetur molestiae delectus expedita maiores nemo aspernatur non nam sequi cumque quae deleniti soluta, exercitationem harum! Voluptatum exercitationem excepturi quo eaque in soluta placeat, magnam officia non unde! Atque vero ad temporibus. Possimus blanditiis dolores fuga, ea alias vero, quasi nostrum ducimus nam numquam non autem sint. Error corporis molestias ipsam! Tenetur cum pariatur fugiat est soluta qui ullam repellendus, eum quaerat vitae, illo sequi error adipisci aspernatur! Facilis, inventore ipsam nesciunt labore dicta id debitis blanditiis ex in dolorem ducimus repellat impedit et recusandae sunt voluptatem repudiandae doloremque fugiat consectetur reiciendis! Tempora, excepturi voluptate. Iusto labore quasi atque? Minima iste officia, dolores earum corporis atque qui aliquam dolorem! Blanditiis, in. Incidunt facere officia debitis culpa error, explicabo neque? Culpa, cupiditate molestias. Tenetur id ducimus aspernatur pariatur necessitatibus atque, facere incidunt aperiam aut, optio dolorum ab modi fugiat quas cumque eligendi obcaecati, expedita veritatis voluptatibus. Suscipit blanditiis aliquam quaerat quod natus minus enim nobis unde iste fugit cupiditate quisquam praesentium quasi obcaecati eaque quas, minima saepe perferendis, ullam nemo numquam ab aut dicta id? Iusto esse voluptatibus, qui quidem suscipit adipisci voluptas ut ab fuga, libero, quos saepe placeat! Esse exercitationem eveniet rerum dicta doloremque commodi, placeat animi voluptatum architecto praesentium labore? Aperiam maxime, impedit animi, harum error ipsam, hic expedita architecto tenetur porro earum at et! Consequuntur ipsam amet quos numquam, velit exercitationem eum in ipsa assumenda ab natus dolorum sapiente deserunt soluta corrupti distinctio? Consequuntur ipsam explicabo aspernatur eaque praesentium animi fugit eum sit nisi commodi iure amet magni doloremque iste saepe, deserunt maxime accusantium recusandae. Esse error aliquam quos laboriosam sint unde dolore libero. Fugit nesciunt magni doloribus, beatae, sapiente praesentium rerum, doloremque ea ducimus fugiat repudiandae alias. Voluptatem nisi dolorem ratione expedita inventore pariatur quis blanditiis optio, ullam reiciendis aut tempora necessitatibus similique quod nam accusamus perspiciatis, quos nulla ad minima eius recusandae, autem maxime id. Quas eveniet repudiandae sapiente aliquid nihil? Quis porro obcaecati nemo eligendi vitae nulla illum consequuntur. At consectetur accusamus quibusdam unde veritatis nulla eligendi doloremque eius corrupti consequuntur? Vitae consequuntur dolores animi adipisci, sed autem eveniet magnam praesentium, ab a hic laborum quasi nobis aperiam. Non ducimus doloribus consequuntur facere est adipisci veniam, ipsam iste itaque optio, qui hic. Illo consequatur cum esse fuga! Obcaecati mollitia ex consectetur, veniam, facere amet dicta explicabo blanditiis et asperiores totam quisquam dolorum rerum, provident magni. Quaerat tempora ducimus esse maxime. Nam, provident cupiditate!', 'проспект Победителей, 99к3', 0x000000000101000000a3297387597e3b4074bb3aaf18f24a40, 1, '2019-08-18 12:26:00', '2019-09-04 16:28:33', 'Центральный', NULL, 0),
(18, 'test-4', 'objects/August2019/VQbgngdDFb3CRKg0XGYp.jpg', '[\"objects\\/August2019\\/Q5YdALVgg6ZAQRrCfnRu.jpg\",\"objects\\/August2019\\/D5rfvWgExb8kJvpFtpsy.jpg\",\"objects\\/August2019\\/65PeCgmq0T6Yi0brP8sW.jpg\",\"objects\\/August2019\\/OaYYu5Mb9XqdJD0JBRSy.jpg\"]', '4-комнатная квартира премиального класса около вдхр. Дрозды с дизайнерским ремонтом.', 180000, 90000, 1202, 610, '[\"sell\",\"rent\"]', 'Дом', 'Брус', 2, 40, 20, 15, 5, 1996, 0, 1, '4-комнатная квартира премиального класса около вдхр. Дрозды с дизайнерским ремонтом.', 'Квартира находится в обыкновенном жилом состоянии, что позволяет при желании сразу заехать и жить или, не переплачивая деньги за чужой ремонт, сделать все по своему вкусу...', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestiae tenetur odio sequi quo quidem labore, reiciendis quod eos atque repellat illo sint quia hic doloribus commodi corporis animi, pariatur nesciunt! Nihil quaerat repellendus veritatis eligendi! Dolorum exercitationem odit consectetur excepturi!  Lorem ipsum dolor sit amet consectetur adipisicing elit. Consequatur ab iure soluta atque iste pariatur sapiente blanditiis eligendi dolorem ut aut animi quis maxime qui, quia quasi. Nihil, dicta. Sequi voluptate architecto numquam quia distinctio iusto obcaecati exercitationem recusandae veritatis cumque, ratione, incidunt necessitatibus illum modi? Corrupti, commodi voluptates quidem repellat nobis perferendis quod laborum ducimus qui voluptatum ipsum pariatur doloremque deserunt quaerat quos obcaecati, labore dolorum! Ullam, enim! Vitae unde ex amet magni laudantium minima est veniam labore explicabo voluptas non dolore accusantium, alias necessitatibus sit a nihil suscipit perspiciatis dolorum provident, natus deserunt earum dicta eligendi. Quos possimus totam consectetur molestiae delectus expedita maiores nemo aspernatur non nam sequi cumque quae deleniti soluta, exercitationem harum! Voluptatum exercitationem excepturi quo eaque in soluta placeat, magnam officia non unde! Atque vero ad temporibus. Possimus blanditiis dolores fuga, ea alias vero, quasi nostrum ducimus nam numquam non autem sint. Error corporis molestias ipsam! Tenetur cum pariatur fugiat est soluta qui ullam repellendus, eum quaerat vitae, illo sequi error adipisci aspernatur! Facilis, inventore ipsam nesciunt labore dicta id debitis blanditiis ex in dolorem ducimus repellat impedit et recusandae sunt voluptatem repudiandae doloremque fugiat consectetur reiciendis! Tempora, excepturi voluptate. Iusto labore quasi atque? Minima iste officia, dolores earum corporis atque qui aliquam dolorem! Blanditiis, in. Incidunt facere officia debitis culpa error, explicabo neque? Culpa, cupiditate molestias. Tenetur id ducimus aspernatur pariatur necessitatibus atque, facere incidunt aperiam aut, optio dolorum ab modi fugiat quas cumque eligendi obcaecati, expedita veritatis voluptatibus. Suscipit blanditiis aliquam quaerat quod natus minus enim nobis unde iste fugit cupiditate quisquam praesentium quasi obcaecati eaque quas, minima saepe perferendis, ullam nemo numquam ab aut dicta id? Iusto esse voluptatibus, qui quidem suscipit adipisci voluptas ut ab fuga, libero, quos saepe placeat! Esse exercitationem eveniet rerum dicta doloremque commodi, placeat animi voluptatum architecto praesentium labore? Aperiam maxime, impedit animi, harum error ipsam, hic expedita architecto tenetur porro earum at et! Consequuntur ipsam amet quos numquam, velit exercitationem eum in ipsa assumenda ab natus dolorum sapiente deserunt soluta corrupti distinctio? Consequuntur ipsam explicabo aspernatur eaque praesentium animi fugit eum sit nisi commodi iure amet magni doloremque iste saepe, deserunt maxime accusantium recusandae. Esse error aliquam quos laboriosam sint unde dolore libero. Fugit nesciunt magni doloribus, beatae, sapiente praesentium rerum, doloremque ea ducimus fugiat repudiandae alias. Voluptatem nisi dolorem ratione expedita inventore pariatur quis blanditiis optio, ullam reiciendis aut tempora necessitatibus similique quod nam accusamus perspiciatis, quos nulla ad minima eius recusandae, autem maxime id. Quas eveniet repudiandae sapiente aliquid nihil? Quis porro obcaecati nemo eligendi vitae nulla illum consequuntur. At consectetur accusamus quibusdam unde veritatis nulla eligendi doloremque eius corrupti consequuntur? Vitae consequuntur dolores animi adipisci, sed autem eveniet magnam praesentium, ab a hic laborum quasi nobis aperiam. Non ducimus doloribus consequuntur facere est adipisci veniam, ipsam iste itaque optio, qui hic. Illo consequatur cum esse fuga! Obcaecati mollitia ex consectetur, veniam, facere amet dicta explicabo blanditiis et asperiores totam quisquam dolorum rerum, provident magni. Quaerat tempora ducimus esse maxime. Nam, provident cupiditate!', 'проспект Победителей, 99к3', 0x000000000101000000c6297387879b3b40a73cb2ade7f04a40, 1, '2019-08-18 12:26:00', '2019-09-04 16:30:06', 'Центральный', NULL, 0);
INSERT INTO `objects` (`id`, `slug`, `main_image`, `images`, `name`, `price`, `price_usd`, `price_m2`, `price_m2_usd`, `type`, `property_type`, `house_type`, `rooms`, `square`, `living_square`, `kitchen_square`, `floor`, `year`, `repair`, `furniture`, `title`, `description`, `content`, `address`, `coordinates`, `published`, `created_at`, `updated_at`, `districts`, `bitrix_id`, `popular`) VALUES
(19, 'test-5', 'objects/August2019/VQbgngdDFb3CRKg0XGYp.jpg', '[\"objects\\/August2019\\/Q5YdALVgg6ZAQRrCfnRu.jpg\",\"objects\\/August2019\\/D5rfvWgExb8kJvpFtpsy.jpg\",\"objects\\/August2019\\/65PeCgmq0T6Yi0brP8sW.jpg\",\"objects\\/August2019\\/OaYYu5Mb9XqdJD0JBRSy.jpg\"]', '4-комнатная квартира премиального класса около вдхр. Дрозды с дизайнерским ремонтом.', 320000, 160000, 1470.5, 620.5, '[\"buy\",\"rent\"]', 'Квартира', 'Панельный', 4, 50, 10, 18, 5, 1986, 1, 1, '4-комнатная квартира премиального класса около вдхр. Дрозды с дизайнерским ремонтом.', 'Квартира находится в обыкновенном жилом состоянии, что позволяет при желании сразу заехать и жить или, не переплачивая деньги за чужой ремонт, сделать все по своему вкусу...', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestiae tenetur odio sequi quo quidem labore, reiciendis quod eos atque repellat illo sint quia hic doloribus commodi corporis animi, pariatur nesciunt! Nihil quaerat repellendus veritatis eligendi! Dolorum exercitationem odit consectetur excepturi!  Lorem ipsum dolor sit amet consectetur adipisicing elit. Consequatur ab iure soluta atque iste pariatur sapiente blanditiis eligendi dolorem ut aut animi quis maxime qui, quia quasi. Nihil, dicta. Sequi voluptate architecto numquam quia distinctio iusto obcaecati exercitationem recusandae veritatis cumque, ratione, incidunt necessitatibus illum modi? Corrupti, commodi voluptates quidem repellat nobis perferendis quod laborum ducimus qui voluptatum ipsum pariatur doloremque deserunt quaerat quos obcaecati, labore dolorum! Ullam, enim! Vitae unde ex amet magni laudantium minima est veniam labore explicabo voluptas non dolore accusantium, alias necessitatibus sit a nihil suscipit perspiciatis dolorum provident, natus deserunt earum dicta eligendi. Quos possimus totam consectetur molestiae delectus expedita maiores nemo aspernatur non nam sequi cumque quae deleniti soluta, exercitationem harum! Voluptatum exercitationem excepturi quo eaque in soluta placeat, magnam officia non unde! Atque vero ad temporibus. Possimus blanditiis dolores fuga, ea alias vero, quasi nostrum ducimus nam numquam non autem sint. Error corporis molestias ipsam! Tenetur cum pariatur fugiat est soluta qui ullam repellendus, eum quaerat vitae, illo sequi error adipisci aspernatur! Facilis, inventore ipsam nesciunt labore dicta id debitis blanditiis ex in dolorem ducimus repellat impedit et recusandae sunt voluptatem repudiandae doloremque fugiat consectetur reiciendis! Tempora, excepturi voluptate. Iusto labore quasi atque? Minima iste officia, dolores earum corporis atque qui aliquam dolorem! Blanditiis, in. Incidunt facere officia debitis culpa error, explicabo neque? Culpa, cupiditate molestias. Tenetur id ducimus aspernatur pariatur necessitatibus atque, facere incidunt aperiam aut, optio dolorum ab modi fugiat quas cumque eligendi obcaecati, expedita veritatis voluptatibus. Suscipit blanditiis aliquam quaerat quod natus minus enim nobis unde iste fugit cupiditate quisquam praesentium quasi obcaecati eaque quas, minima saepe perferendis, ullam nemo numquam ab aut dicta id? Iusto esse voluptatibus, qui quidem suscipit adipisci voluptas ut ab fuga, libero, quos saepe placeat! Esse exercitationem eveniet rerum dicta doloremque commodi, placeat animi voluptatum architecto praesentium labore? Aperiam maxime, impedit animi, harum error ipsam, hic expedita architecto tenetur porro earum at et! Consequuntur ipsam amet quos numquam, velit exercitationem eum in ipsa assumenda ab natus dolorum sapiente deserunt soluta corrupti distinctio? Consequuntur ipsam explicabo aspernatur eaque praesentium animi fugit eum sit nisi commodi iure amet magni doloremque iste saepe, deserunt maxime accusantium recusandae. Esse error aliquam quos laboriosam sint unde dolore libero. Fugit nesciunt magni doloribus, beatae, sapiente praesentium rerum, doloremque ea ducimus fugiat repudiandae alias. Voluptatem nisi dolorem ratione expedita inventore pariatur quis blanditiis optio, ullam reiciendis aut tempora necessitatibus similique quod nam accusamus perspiciatis, quos nulla ad minima eius recusandae, autem maxime id. Quas eveniet repudiandae sapiente aliquid nihil? Quis porro obcaecati nemo eligendi vitae nulla illum consequuntur. At consectetur accusamus quibusdam unde veritatis nulla eligendi doloremque eius corrupti consequuntur? Vitae consequuntur dolores animi adipisci, sed autem eveniet magnam praesentium, ab a hic laborum quasi nobis aperiam. Non ducimus doloribus consequuntur facere est adipisci veniam, ipsam iste itaque optio, qui hic. Illo consequatur cum esse fuga! Obcaecati mollitia ex consectetur, veniam, facere amet dicta explicabo blanditiis et asperiores totam quisquam dolorum rerum, provident magni. Quaerat tempora ducimus esse maxime. Nam, provident cupiditate!', 'проспект Победителей, 99к3', 0x000000000101000000e228738774903b4033984c469df24a40, 1, '2019-08-18 12:26:00', '2019-09-04 16:30:53', 'Центральный', NULL, 0),
(20, 'test-6', 'objects/August2019/VQbgngdDFb3CRKg0XGYp.jpg', '[\"objects\\/August2019\\/Q5YdALVgg6ZAQRrCfnRu.jpg\",\"objects\\/August2019\\/D5rfvWgExb8kJvpFtpsy.jpg\",\"objects\\/August2019\\/65PeCgmq0T6Yi0brP8sW.jpg\",\"objects\\/August2019\\/OaYYu5Mb9XqdJD0JBRSy.jpg\"]', '4-комнатная квартира премиального класса около вдхр. Дрозды с дизайнерским ремонтом.', 280000, 115000, 1470, 620, '[\"sell\"]', 'Квартира', 'Панельный', 4, 50, 10, 18, 5, 1988, 0, 1, '4-комнатная квартира премиального класса около вдхр. Дрозды с дизайнерским ремонтом.', 'Квартира находится в обыкновенном жилом состоянии, что позволяет при желании сразу заехать и жить или, не переплачивая деньги за чужой ремонт, сделать все по своему вкусу...', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestiae tenetur odio sequi quo quidem labore, reiciendis quod eos atque repellat illo sint quia hic doloribus commodi corporis animi, pariatur nesciunt! Nihil quaerat repellendus veritatis eligendi! Dolorum exercitationem odit consectetur excepturi!  Lorem ipsum dolor sit amet consectetur adipisicing elit. Consequatur ab iure soluta atque iste pariatur sapiente blanditiis eligendi dolorem ut aut animi quis maxime qui, quia quasi. Nihil, dicta. Sequi voluptate architecto numquam quia distinctio iusto obcaecati exercitationem recusandae veritatis cumque, ratione, incidunt necessitatibus illum modi? Corrupti, commodi voluptates quidem repellat nobis perferendis quod laborum ducimus qui voluptatum ipsum pariatur doloremque deserunt quaerat quos obcaecati, labore dolorum! Ullam, enim! Vitae unde ex amet magni laudantium minima est veniam labore explicabo voluptas non dolore accusantium, alias necessitatibus sit a nihil suscipit perspiciatis dolorum provident, natus deserunt earum dicta eligendi. Quos possimus totam consectetur molestiae delectus expedita maiores nemo aspernatur non nam sequi cumque quae deleniti soluta, exercitationem harum! Voluptatum exercitationem excepturi quo eaque in soluta placeat, magnam officia non unde! Atque vero ad temporibus. Possimus blanditiis dolores fuga, ea alias vero, quasi nostrum ducimus nam numquam non autem sint. Error corporis molestias ipsam! Tenetur cum pariatur fugiat est soluta qui ullam repellendus, eum quaerat vitae, illo sequi error adipisci aspernatur! Facilis, inventore ipsam nesciunt labore dicta id debitis blanditiis ex in dolorem ducimus repellat impedit et recusandae sunt voluptatem repudiandae doloremque fugiat consectetur reiciendis! Tempora, excepturi voluptate. Iusto labore quasi atque? Minima iste officia, dolores earum corporis atque qui aliquam dolorem! Blanditiis, in. Incidunt facere officia debitis culpa error, explicabo neque? Culpa, cupiditate molestias. Tenetur id ducimus aspernatur pariatur necessitatibus atque, facere incidunt aperiam aut, optio dolorum ab modi fugiat quas cumque eligendi obcaecati, expedita veritatis voluptatibus. Suscipit blanditiis aliquam quaerat quod natus minus enim nobis unde iste fugit cupiditate quisquam praesentium quasi obcaecati eaque quas, minima saepe perferendis, ullam nemo numquam ab aut dicta id? Iusto esse voluptatibus, qui quidem suscipit adipisci voluptas ut ab fuga, libero, quos saepe placeat! Esse exercitationem eveniet rerum dicta doloremque commodi, placeat animi voluptatum architecto praesentium labore? Aperiam maxime, impedit animi, harum error ipsam, hic expedita architecto tenetur porro earum at et! Consequuntur ipsam amet quos numquam, velit exercitationem eum in ipsa assumenda ab natus dolorum sapiente deserunt soluta corrupti distinctio? Consequuntur ipsam explicabo aspernatur eaque praesentium animi fugit eum sit nisi commodi iure amet magni doloremque iste saepe, deserunt maxime accusantium recusandae. Esse error aliquam quos laboriosam sint unde dolore libero. Fugit nesciunt magni doloribus, beatae, sapiente praesentium rerum, doloremque ea ducimus fugiat repudiandae alias. Voluptatem nisi dolorem ratione expedita inventore pariatur quis blanditiis optio, ullam reiciendis aut tempora necessitatibus similique quod nam accusamus perspiciatis, quos nulla ad minima eius recusandae, autem maxime id. Quas eveniet repudiandae sapiente aliquid nihil? Quis porro obcaecati nemo eligendi vitae nulla illum consequuntur. At consectetur accusamus quibusdam unde veritatis nulla eligendi doloremque eius corrupti consequuntur? Vitae consequuntur dolores animi adipisci, sed autem eveniet magnam praesentium, ab a hic laborum quasi nobis aperiam. Non ducimus doloribus consequuntur facere est adipisci veniam, ipsam iste itaque optio, qui hic. Illo consequatur cum esse fuga! Obcaecati mollitia ex consectetur, veniam, facere amet dicta explicabo blanditiis et asperiores totam quisquam dolorum rerum, provident magni. Quaerat tempora ducimus esse maxime. Nam, provident cupiditate!', 'проспект Победителей, 99к3', 0x000000000101000000e228738744933b4030e2b4b539ed4a40, 1, '2019-08-18 12:26:00', '2019-09-04 16:31:44', 'Ленинский', NULL, 0),
(21, 'test-7', 'objects/August2019/VQbgngdDFb3CRKg0XGYp.jpg', '[\"objects\\/August2019\\/Q5YdALVgg6ZAQRrCfnRu.jpg\",\"objects\\/August2019\\/D5rfvWgExb8kJvpFtpsy.jpg\",\"objects\\/August2019\\/65PeCgmq0T6Yi0brP8sW.jpg\",\"objects\\/August2019\\/OaYYu5Mb9XqdJD0JBRSy.jpg\"]', '4-комнатная квартира премиального класса около вдхр. Дрозды с дизайнерским ремонтом.', 280000, 115000, 1470, 620, '[\"buy\",\"rent\"]', 'Квартира', 'Кирпичный', 2, 50, 10, 18, 5, 1968, 0, 0, '4-комнатная квартира премиального класса около вдхр. Дрозды с дизайнерским ремонтом.', 'Квартира находится в обыкновенном жилом состоянии, что позволяет при желании сразу заехать и жить или, не переплачивая деньги за чужой ремонт, сделать все по своему вкусу...', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestiae tenetur odio sequi quo quidem labore, reiciendis quod eos atque repellat illo sint quia hic doloribus commodi corporis animi, pariatur nesciunt! Nihil quaerat repellendus veritatis eligendi! Dolorum exercitationem odit consectetur excepturi!  Lorem ipsum dolor sit amet consectetur adipisicing elit. Consequatur ab iure soluta atque iste pariatur sapiente blanditiis eligendi dolorem ut aut animi quis maxime qui, quia quasi. Nihil, dicta. Sequi voluptate architecto numquam quia distinctio iusto obcaecati exercitationem recusandae veritatis cumque, ratione, incidunt necessitatibus illum modi? Corrupti, commodi voluptates quidem repellat nobis perferendis quod laborum ducimus qui voluptatum ipsum pariatur doloremque deserunt quaerat quos obcaecati, labore dolorum! Ullam, enim! Vitae unde ex amet magni laudantium minima est veniam labore explicabo voluptas non dolore accusantium, alias necessitatibus sit a nihil suscipit perspiciatis dolorum provident, natus deserunt earum dicta eligendi. Quos possimus totam consectetur molestiae delectus expedita maiores nemo aspernatur non nam sequi cumque quae deleniti soluta, exercitationem harum! Voluptatum exercitationem excepturi quo eaque in soluta placeat, magnam officia non unde! Atque vero ad temporibus. Possimus blanditiis dolores fuga, ea alias vero, quasi nostrum ducimus nam numquam non autem sint. Error corporis molestias ipsam! Tenetur cum pariatur fugiat est soluta qui ullam repellendus, eum quaerat vitae, illo sequi error adipisci aspernatur! Facilis, inventore ipsam nesciunt labore dicta id debitis blanditiis ex in dolorem ducimus repellat impedit et recusandae sunt voluptatem repudiandae doloremque fugiat consectetur reiciendis! Tempora, excepturi voluptate. Iusto labore quasi atque? Minima iste officia, dolores earum corporis atque qui aliquam dolorem! Blanditiis, in. Incidunt facere officia debitis culpa error, explicabo neque? Culpa, cupiditate molestias. Tenetur id ducimus aspernatur pariatur necessitatibus atque, facere incidunt aperiam aut, optio dolorum ab modi fugiat quas cumque eligendi obcaecati, expedita veritatis voluptatibus. Suscipit blanditiis aliquam quaerat quod natus minus enim nobis unde iste fugit cupiditate quisquam praesentium quasi obcaecati eaque quas, minima saepe perferendis, ullam nemo numquam ab aut dicta id? Iusto esse voluptatibus, qui quidem suscipit adipisci voluptas ut ab fuga, libero, quos saepe placeat! Esse exercitationem eveniet rerum dicta doloremque commodi, placeat animi voluptatum architecto praesentium labore? Aperiam maxime, impedit animi, harum error ipsam, hic expedita architecto tenetur porro earum at et! Consequuntur ipsam amet quos numquam, velit exercitationem eum in ipsa assumenda ab natus dolorum sapiente deserunt soluta corrupti distinctio? Consequuntur ipsam explicabo aspernatur eaque praesentium animi fugit eum sit nisi commodi iure amet magni doloremque iste saepe, deserunt maxime accusantium recusandae. Esse error aliquam quos laboriosam sint unde dolore libero. Fugit nesciunt magni doloribus, beatae, sapiente praesentium rerum, doloremque ea ducimus fugiat repudiandae alias. Voluptatem nisi dolorem ratione expedita inventore pariatur quis blanditiis optio, ullam reiciendis aut tempora necessitatibus similique quod nam accusamus perspiciatis, quos nulla ad minima eius recusandae, autem maxime id. Quas eveniet repudiandae sapiente aliquid nihil? Quis porro obcaecati nemo eligendi vitae nulla illum consequuntur. At consectetur accusamus quibusdam unde veritatis nulla eligendi doloremque eius corrupti consequuntur? Vitae consequuntur dolores animi adipisci, sed autem eveniet magnam praesentium, ab a hic laborum quasi nobis aperiam. Non ducimus doloribus consequuntur facere est adipisci veniam, ipsam iste itaque optio, qui hic. Illo consequatur cum esse fuga! Obcaecati mollitia ex consectetur, veniam, facere amet dicta explicabo blanditiis et asperiores totam quisquam dolorum rerum, provident magni. Quaerat tempora ducimus esse maxime. Nam, provident cupiditate!', 'проспект Победителей, 99к3', 0x000000000101000000e2287387c47c3b40c0c8e0ff9fee4a40, 1, '2019-08-18 12:26:00', '2019-09-04 16:33:03', 'Московский', NULL, 0),
(22, 'test-8', 'objects/August2019/VQbgngdDFb3CRKg0XGYp.jpg', '[\"objects\\/August2019\\/Q5YdALVgg6ZAQRrCfnRu.jpg\",\"objects\\/August2019\\/D5rfvWgExb8kJvpFtpsy.jpg\",\"objects\\/August2019\\/65PeCgmq0T6Yi0brP8sW.jpg\",\"objects\\/August2019\\/OaYYu5Mb9XqdJD0JBRSy.jpg\"]', '4-комнатная квартира премиального класса около вдхр. Дрозды с дизайнерским ремонтом.', 280000, 115000, 1470, 620, '[\"sell\",\"rent\"]', 'Квартира', 'Панельный', 5, 120, 89, 18.5, 18, 2001, 1, 1, '4-комнатная квартира премиального класса около вдхр. Дрозды с дизайнерским ремонтом.', 'Квартира находится в обыкновенном жилом состоянии, что позволяет при желании сразу заехать и жить или, не переплачивая деньги за чужой ремонт, сделать все по своему вкусу...', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestiae tenetur odio sequi quo quidem labore, reiciendis quod eos atque repellat illo sint quia hic doloribus commodi corporis animi, pariatur nesciunt! Nihil quaerat repellendus veritatis eligendi! Dolorum exercitationem odit consectetur excepturi!  Lorem ipsum dolor sit amet consectetur adipisicing elit. Consequatur ab iure soluta atque iste pariatur sapiente blanditiis eligendi dolorem ut aut animi quis maxime qui, quia quasi. Nihil, dicta. Sequi voluptate architecto numquam quia distinctio iusto obcaecati exercitationem recusandae veritatis cumque, ratione, incidunt necessitatibus illum modi? Corrupti, commodi voluptates quidem repellat nobis perferendis quod laborum ducimus qui voluptatum ipsum pariatur doloremque deserunt quaerat quos obcaecati, labore dolorum! Ullam, enim! Vitae unde ex amet magni laudantium minima est veniam labore explicabo voluptas non dolore accusantium, alias necessitatibus sit a nihil suscipit perspiciatis dolorum provident, natus deserunt earum dicta eligendi. Quos possimus totam consectetur molestiae delectus expedita maiores nemo aspernatur non nam sequi cumque quae deleniti soluta, exercitationem harum! Voluptatum exercitationem excepturi quo eaque in soluta placeat, magnam officia non unde! Atque vero ad temporibus. Possimus blanditiis dolores fuga, ea alias vero, quasi nostrum ducimus nam numquam non autem sint. Error corporis molestias ipsam! Tenetur cum pariatur fugiat est soluta qui ullam repellendus, eum quaerat vitae, illo sequi error adipisci aspernatur! Facilis, inventore ipsam nesciunt labore dicta id debitis blanditiis ex in dolorem ducimus repellat impedit et recusandae sunt voluptatem repudiandae doloremque fugiat consectetur reiciendis! Tempora, excepturi voluptate. Iusto labore quasi atque? Minima iste officia, dolores earum corporis atque qui aliquam dolorem! Blanditiis, in. Incidunt facere officia debitis culpa error, explicabo neque? Culpa, cupiditate molestias. Tenetur id ducimus aspernatur pariatur necessitatibus atque, facere incidunt aperiam aut, optio dolorum ab modi fugiat quas cumque eligendi obcaecati, expedita veritatis voluptatibus. Suscipit blanditiis aliquam quaerat quod natus minus enim nobis unde iste fugit cupiditate quisquam praesentium quasi obcaecati eaque quas, minima saepe perferendis, ullam nemo numquam ab aut dicta id? Iusto esse voluptatibus, qui quidem suscipit adipisci voluptas ut ab fuga, libero, quos saepe placeat! Esse exercitationem eveniet rerum dicta doloremque commodi, placeat animi voluptatum architecto praesentium labore? Aperiam maxime, impedit animi, harum error ipsam, hic expedita architecto tenetur porro earum at et! Consequuntur ipsam amet quos numquam, velit exercitationem eum in ipsa assumenda ab natus dolorum sapiente deserunt soluta corrupti distinctio? Consequuntur ipsam explicabo aspernatur eaque praesentium animi fugit eum sit nisi commodi iure amet magni doloremque iste saepe, deserunt maxime accusantium recusandae. Esse error aliquam quos laboriosam sint unde dolore libero. Fugit nesciunt magni doloribus, beatae, sapiente praesentium rerum, doloremque ea ducimus fugiat repudiandae alias. Voluptatem nisi dolorem ratione expedita inventore pariatur quis blanditiis optio, ullam reiciendis aut tempora necessitatibus similique quod nam accusamus perspiciatis, quos nulla ad minima eius recusandae, autem maxime id. Quas eveniet repudiandae sapiente aliquid nihil? Quis porro obcaecati nemo eligendi vitae nulla illum consequuntur. At consectetur accusamus quibusdam unde veritatis nulla eligendi doloremque eius corrupti consequuntur? Vitae consequuntur dolores animi adipisci, sed autem eveniet magnam praesentium, ab a hic laborum quasi nobis aperiam. Non ducimus doloribus consequuntur facere est adipisci veniam, ipsam iste itaque optio, qui hic. Illo consequatur cum esse fuga! Obcaecati mollitia ex consectetur, veniam, facere amet dicta explicabo blanditiis et asperiores totam quisquam dolorum rerum, provident magni. Quaerat tempora ducimus esse maxime. Nam, provident cupiditate!', 'проспект Победителей, 99к3', 0x000000000101000000f328738783ae3b401b86b5295cf84a40, 1, '2019-08-18 12:26:00', '2019-09-04 16:34:21', 'Центральный', NULL, 0),
(23, 'test-9', 'objects/August2019/VQbgngdDFb3CRKg0XGYp.jpg', '[\"objects\\/August2019\\/Q5YdALVgg6ZAQRrCfnRu.jpg\",\"objects\\/August2019\\/D5rfvWgExb8kJvpFtpsy.jpg\",\"objects\\/August2019\\/65PeCgmq0T6Yi0brP8sW.jpg\",\"objects\\/August2019\\/OaYYu5Mb9XqdJD0JBRSy.jpg\"]', '4-комнатная квартира премиального класса около вдхр. Дрозды с дизайнерским ремонтом.', 280000, 115000, 1470, 620, '[\"buy\",\"rent\"]', 'Квартира', 'Панельный', 3, 50, 10, 18, 8, 1998, 0, 0, '4-комнатная квартира премиального класса около вдхр. Дрозды с дизайнерским ремонтом.', 'Квартира находится в обыкновенном жилом состоянии, что позволяет при желании сразу заехать и жить или, не переплачивая деньги за чужой ремонт, сделать все по своему вкусу...', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestiae tenetur odio sequi quo quidem labore, reiciendis quod eos atque repellat illo sint quia hic doloribus commodi corporis animi, pariatur nesciunt! Nihil quaerat repellendus veritatis eligendi! Dolorum exercitationem odit consectetur excepturi!  Lorem ipsum dolor sit amet consectetur adipisicing elit. Consequatur ab iure soluta atque iste pariatur sapiente blanditiis eligendi dolorem ut aut animi quis maxime qui, quia quasi. Nihil, dicta. Sequi voluptate architecto numquam quia distinctio iusto obcaecati exercitationem recusandae veritatis cumque, ratione, incidunt necessitatibus illum modi? Corrupti, commodi voluptates quidem repellat nobis perferendis quod laborum ducimus qui voluptatum ipsum pariatur doloremque deserunt quaerat quos obcaecati, labore dolorum! Ullam, enim! Vitae unde ex amet magni laudantium minima est veniam labore explicabo voluptas non dolore accusantium, alias necessitatibus sit a nihil suscipit perspiciatis dolorum provident, natus deserunt earum dicta eligendi. Quos possimus totam consectetur molestiae delectus expedita maiores nemo aspernatur non nam sequi cumque quae deleniti soluta, exercitationem harum! Voluptatum exercitationem excepturi quo eaque in soluta placeat, magnam officia non unde! Atque vero ad temporibus. Possimus blanditiis dolores fuga, ea alias vero, quasi nostrum ducimus nam numquam non autem sint. Error corporis molestias ipsam! Tenetur cum pariatur fugiat est soluta qui ullam repellendus, eum quaerat vitae, illo sequi error adipisci aspernatur! Facilis, inventore ipsam nesciunt labore dicta id debitis blanditiis ex in dolorem ducimus repellat impedit et recusandae sunt voluptatem repudiandae doloremque fugiat consectetur reiciendis! Tempora, excepturi voluptate. Iusto labore quasi atque? Minima iste officia, dolores earum corporis atque qui aliquam dolorem! Blanditiis, in. Incidunt facere officia debitis culpa error, explicabo neque? Culpa, cupiditate molestias. Tenetur id ducimus aspernatur pariatur necessitatibus atque, facere incidunt aperiam aut, optio dolorum ab modi fugiat quas cumque eligendi obcaecati, expedita veritatis voluptatibus. Suscipit blanditiis aliquam quaerat quod natus minus enim nobis unde iste fugit cupiditate quisquam praesentium quasi obcaecati eaque quas, minima saepe perferendis, ullam nemo numquam ab aut dicta id? Iusto esse voluptatibus, qui quidem suscipit adipisci voluptas ut ab fuga, libero, quos saepe placeat! Esse exercitationem eveniet rerum dicta doloremque commodi, placeat animi voluptatum architecto praesentium labore? Aperiam maxime, impedit animi, harum error ipsam, hic expedita architecto tenetur porro earum at et! Consequuntur ipsam amet quos numquam, velit exercitationem eum in ipsa assumenda ab natus dolorum sapiente deserunt soluta corrupti distinctio? Consequuntur ipsam explicabo aspernatur eaque praesentium animi fugit eum sit nisi commodi iure amet magni doloremque iste saepe, deserunt maxime accusantium recusandae. Esse error aliquam quos laboriosam sint unde dolore libero. Fugit nesciunt magni doloribus, beatae, sapiente praesentium rerum, doloremque ea ducimus fugiat repudiandae alias. Voluptatem nisi dolorem ratione expedita inventore pariatur quis blanditiis optio, ullam reiciendis aut tempora necessitatibus similique quod nam accusamus perspiciatis, quos nulla ad minima eius recusandae, autem maxime id. Quas eveniet repudiandae sapiente aliquid nihil? Quis porro obcaecati nemo eligendi vitae nulla illum consequuntur. At consectetur accusamus quibusdam unde veritatis nulla eligendi doloremque eius corrupti consequuntur? Vitae consequuntur dolores animi adipisci, sed autem eveniet magnam praesentium, ab a hic laborum quasi nobis aperiam. Non ducimus doloribus consequuntur facere est adipisci veniam, ipsam iste itaque optio, qui hic. Illo consequatur cum esse fuga! Obcaecati mollitia ex consectetur, veniam, facere amet dicta explicabo blanditiis et asperiores totam quisquam dolorum rerum, provident magni. Quaerat tempora ducimus esse maxime. Nam, provident cupiditate!', 'проспект Победителей, 99к3', 0x0000000001010000004b297387beaf3b40398247ac83eb4a40, 1, '2019-08-18 12:26:00', '2019-09-04 16:35:35', 'Заводской', NULL, 0),
(24, 'test-10', 'objects/August2019/VQbgngdDFb3CRKg0XGYp.jpg', '[\"objects\\/August2019\\/Q5YdALVgg6ZAQRrCfnRu.jpg\",\"objects\\/August2019\\/D5rfvWgExb8kJvpFtpsy.jpg\",\"objects\\/August2019\\/65PeCgmq0T6Yi0brP8sW.jpg\",\"objects\\/August2019\\/OaYYu5Mb9XqdJD0JBRSy.jpg\"]', '4-комнатная квартира премиального класса около вдхр. Дрозды с дизайнерским ремонтом.', 280000, 115000, 1470, 620, '[\"buy\"]', 'Квартира', 'Панельный', 2, 50, 10, 18, 5, 1986, 1, 1, '4-комнатная квартира премиального класса около вдхр. Дрозды с дизайнерским ремонтом.', 'Квартира находится в обыкновенном жилом состоянии, что позволяет при желании сразу заехать и жить или, не переплачивая деньги за чужой ремонт, сделать все по своему вкусу...', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestiae tenetur odio sequi quo quidem labore, reiciendis quod eos atque repellat illo sint quia hic doloribus commodi corporis animi, pariatur nesciunt! Nihil quaerat repellendus veritatis eligendi! Dolorum exercitationem odit consectetur excepturi!  Lorem ipsum dolor sit amet consectetur adipisicing elit. Consequatur ab iure soluta atque iste pariatur sapiente blanditiis eligendi dolorem ut aut animi quis maxime qui, quia quasi. Nihil, dicta. Sequi voluptate architecto numquam quia distinctio iusto obcaecati exercitationem recusandae veritatis cumque, ratione, incidunt necessitatibus illum modi? Corrupti, commodi voluptates quidem repellat nobis perferendis quod laborum ducimus qui voluptatum ipsum pariatur doloremque deserunt quaerat quos obcaecati, labore dolorum! Ullam, enim! Vitae unde ex amet magni laudantium minima est veniam labore explicabo voluptas non dolore accusantium, alias necessitatibus sit a nihil suscipit perspiciatis dolorum provident, natus deserunt earum dicta eligendi. Quos possimus totam consectetur molestiae delectus expedita maiores nemo aspernatur non nam sequi cumque quae deleniti soluta, exercitationem harum! Voluptatum exercitationem excepturi quo eaque in soluta placeat, magnam officia non unde! Atque vero ad temporibus. Possimus blanditiis dolores fuga, ea alias vero, quasi nostrum ducimus nam numquam non autem sint. Error corporis molestias ipsam! Tenetur cum pariatur fugiat est soluta qui ullam repellendus, eum quaerat vitae, illo sequi error adipisci aspernatur! Facilis, inventore ipsam nesciunt labore dicta id debitis blanditiis ex in dolorem ducimus repellat impedit et recusandae sunt voluptatem repudiandae doloremque fugiat consectetur reiciendis! Tempora, excepturi voluptate. Iusto labore quasi atque? Minima iste officia, dolores earum corporis atque qui aliquam dolorem! Blanditiis, in. Incidunt facere officia debitis culpa error, explicabo neque? Culpa, cupiditate molestias. Tenetur id ducimus aspernatur pariatur necessitatibus atque, facere incidunt aperiam aut, optio dolorum ab modi fugiat quas cumque eligendi obcaecati, expedita veritatis voluptatibus. Suscipit blanditiis aliquam quaerat quod natus minus enim nobis unde iste fugit cupiditate quisquam praesentium quasi obcaecati eaque quas, minima saepe perferendis, ullam nemo numquam ab aut dicta id? Iusto esse voluptatibus, qui quidem suscipit adipisci voluptas ut ab fuga, libero, quos saepe placeat! Esse exercitationem eveniet rerum dicta doloremque commodi, placeat animi voluptatum architecto praesentium labore? Aperiam maxime, impedit animi, harum error ipsam, hic expedita architecto tenetur porro earum at et! Consequuntur ipsam amet quos numquam, velit exercitationem eum in ipsa assumenda ab natus dolorum sapiente deserunt soluta corrupti distinctio? Consequuntur ipsam explicabo aspernatur eaque praesentium animi fugit eum sit nisi commodi iure amet magni doloremque iste saepe, deserunt maxime accusantium recusandae. Esse error aliquam quos laboriosam sint unde dolore libero. Fugit nesciunt magni doloribus, beatae, sapiente praesentium rerum, doloremque ea ducimus fugiat repudiandae alias. Voluptatem nisi dolorem ratione expedita inventore pariatur quis blanditiis optio, ullam reiciendis aut tempora necessitatibus similique quod nam accusamus perspiciatis, quos nulla ad minima eius recusandae, autem maxime id. Quas eveniet repudiandae sapiente aliquid nihil? Quis porro obcaecati nemo eligendi vitae nulla illum consequuntur. At consectetur accusamus quibusdam unde veritatis nulla eligendi doloremque eius corrupti consequuntur? Vitae consequuntur dolores animi adipisci, sed autem eveniet magnam praesentium, ab a hic laborum quasi nobis aperiam. Non ducimus doloribus consequuntur facere est adipisci veniam, ipsam iste itaque optio, qui hic. Illo consequatur cum esse fuga! Obcaecati mollitia ex consectetur, veniam, facere amet dicta explicabo blanditiis et asperiores totam quisquam dolorum rerum, provident magni. Quaerat tempora ducimus esse maxime. Nam, provident cupiditate!', 'проспект Победителей, 99к3', 0x000000000101000000d8297387d6973b409de2901b46fa4a40, 1, '2019-08-18 12:26:00', '2019-09-04 16:36:18', 'Советский', NULL, 0),
(25, 'test-11', 'objects/August2019/VQbgngdDFb3CRKg0XGYp.jpg', '[\"objects\\/August2019\\/Q5YdALVgg6ZAQRrCfnRu.jpg\",\"objects\\/August2019\\/D5rfvWgExb8kJvpFtpsy.jpg\",\"objects\\/August2019\\/65PeCgmq0T6Yi0brP8sW.jpg\",\"objects\\/August2019\\/OaYYu5Mb9XqdJD0JBRSy.jpg\"]', '4-комнатная квартира премиального класса около вдхр. Дрозды с дизайнерским ремонтом.', 280000, 115000, 1470, 620, '[\"sell\"]', 'Квартира', 'Панельный', 1, 49.75, 9.93, 18, 2, 1987, 0, 0, '4-комнатная квартира премиального класса около вдхр. Дрозды с дизайнерским ремонтом.', 'Квартира находится в обыкновенном жилом состоянии, что позволяет при желании сразу заехать и жить или, не переплачивая деньги за чужой ремонт, сделать все по своему вкусу...', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestiae tenetur odio sequi quo quidem labore, reiciendis quod eos atque repellat illo sint quia hic doloribus commodi corporis animi, pariatur nesciunt! Nihil quaerat repellendus veritatis eligendi! Dolorum exercitationem odit consectetur excepturi!  Lorem ipsum dolor sit amet consectetur adipisicing elit. Consequatur ab iure soluta atque iste pariatur sapiente blanditiis eligendi dolorem ut aut animi quis maxime qui, quia quasi. Nihil, dicta. Sequi voluptate architecto numquam quia distinctio iusto obcaecati exercitationem recusandae veritatis cumque, ratione, incidunt necessitatibus illum modi? Corrupti, commodi voluptates quidem repellat nobis perferendis quod laborum ducimus qui voluptatum ipsum pariatur doloremque deserunt quaerat quos obcaecati, labore dolorum! Ullam, enim! Vitae unde ex amet magni laudantium minima est veniam labore explicabo voluptas non dolore accusantium, alias necessitatibus sit a nihil suscipit perspiciatis dolorum provident, natus deserunt earum dicta eligendi. Quos possimus totam consectetur molestiae delectus expedita maiores nemo aspernatur non nam sequi cumque quae deleniti soluta, exercitationem harum! Voluptatum exercitationem excepturi quo eaque in soluta placeat, magnam officia non unde! Atque vero ad temporibus. Possimus blanditiis dolores fuga, ea alias vero, quasi nostrum ducimus nam numquam non autem sint. Error corporis molestias ipsam! Tenetur cum pariatur fugiat est soluta qui ullam repellendus, eum quaerat vitae, illo sequi error adipisci aspernatur! Facilis, inventore ipsam nesciunt labore dicta id debitis blanditiis ex in dolorem ducimus repellat impedit et recusandae sunt voluptatem repudiandae doloremque fugiat consectetur reiciendis! Tempora, excepturi voluptate. Iusto labore quasi atque? Minima iste officia, dolores earum corporis atque qui aliquam dolorem! Blanditiis, in. Incidunt facere officia debitis culpa error, explicabo neque? Culpa, cupiditate molestias. Tenetur id ducimus aspernatur pariatur necessitatibus atque, facere incidunt aperiam aut, optio dolorum ab modi fugiat quas cumque eligendi obcaecati, expedita veritatis voluptatibus. Suscipit blanditiis aliquam quaerat quod natus minus enim nobis unde iste fugit cupiditate quisquam praesentium quasi obcaecati eaque quas, minima saepe perferendis, ullam nemo numquam ab aut dicta id? Iusto esse voluptatibus, qui quidem suscipit adipisci voluptas ut ab fuga, libero, quos saepe placeat! Esse exercitationem eveniet rerum dicta doloremque commodi, placeat animi voluptatum architecto praesentium labore? Aperiam maxime, impedit animi, harum error ipsam, hic expedita architecto tenetur porro earum at et! Consequuntur ipsam amet quos numquam, velit exercitationem eum in ipsa assumenda ab natus dolorum sapiente deserunt soluta corrupti distinctio? Consequuntur ipsam explicabo aspernatur eaque praesentium animi fugit eum sit nisi commodi iure amet magni doloremque iste saepe, deserunt maxime accusantium recusandae. Esse error aliquam quos laboriosam sint unde dolore libero. Fugit nesciunt magni doloribus, beatae, sapiente praesentium rerum, doloremque ea ducimus fugiat repudiandae alias. Voluptatem nisi dolorem ratione expedita inventore pariatur quis blanditiis optio, ullam reiciendis aut tempora necessitatibus similique quod nam accusamus perspiciatis, quos nulla ad minima eius recusandae, autem maxime id. Quas eveniet repudiandae sapiente aliquid nihil? Quis porro obcaecati nemo eligendi vitae nulla illum consequuntur. At consectetur accusamus quibusdam unde veritatis nulla eligendi doloremque eius corrupti consequuntur? Vitae consequuntur dolores animi adipisci, sed autem eveniet magnam praesentium, ab a hic laborum quasi nobis aperiam. Non ducimus doloribus consequuntur facere est adipisci veniam, ipsam iste itaque optio, qui hic. Illo consequatur cum esse fuga! Obcaecati mollitia ex consectetur, veniam, facere amet dicta explicabo blanditiis et asperiores totam quisquam dolorum rerum, provident magni. Quaerat tempora ducimus esse maxime. Nam, provident cupiditate!', 'проспект Победителей, 99к3', 0x0000000001010000001629738731883b40243f4a8719f54a40, 1, '2019-08-18 12:26:00', '2019-09-04 16:37:21', 'Центральный', NULL, 0),
(26, 'test-12', 'objects/August2019/VQbgngdDFb3CRKg0XGYp.jpg', '[\"objects\\/August2019\\/Q5YdALVgg6ZAQRrCfnRu.jpg\",\"objects\\/August2019\\/D5rfvWgExb8kJvpFtpsy.jpg\",\"objects\\/August2019\\/65PeCgmq0T6Yi0brP8sW.jpg\",\"objects\\/August2019\\/OaYYu5Mb9XqdJD0JBRSy.jpg\"]', '4-комнатная квартира премиального класса около вдхр. Дрозды с дизайнерским ремонтом.', 480000, 225000, 1370, 620, '[\"buy\",\"rent\"]', 'Квартира', 'Панельный', 4, 80, 50, 18, 9, 1999, 1, 1, '4-комнатная квартира премиального класса около вдхр. Дрозды с дизайнерским ремонтом.', 'Квартира находится в обыкновенном жилом состоянии, что позволяет при желании сразу заехать и жить или, не переплачивая деньги за чужой ремонт, сделать все по своему вкусу...', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestiae tenetur odio sequi quo quidem labore, reiciendis quod eos atque repellat illo sint quia hic doloribus commodi corporis animi, pariatur nesciunt! Nihil quaerat repellendus veritatis eligendi! Dolorum exercitationem odit consectetur excepturi!  Lorem ipsum dolor sit amet consectetur adipisicing elit. Consequatur ab iure soluta atque iste pariatur sapiente blanditiis eligendi dolorem ut aut animi quis maxime qui, quia quasi. Nihil, dicta. Sequi voluptate architecto numquam quia distinctio iusto obcaecati exercitationem recusandae veritatis cumque, ratione, incidunt necessitatibus illum modi? Corrupti, commodi voluptates quidem repellat nobis perferendis quod laborum ducimus qui voluptatum ipsum pariatur doloremque deserunt quaerat quos obcaecati, labore dolorum! Ullam, enim! Vitae unde ex amet magni laudantium minima est veniam labore explicabo voluptas non dolore accusantium, alias necessitatibus sit a nihil suscipit perspiciatis dolorum provident, natus deserunt earum dicta eligendi. Quos possimus totam consectetur molestiae delectus expedita maiores nemo aspernatur non nam sequi cumque quae deleniti soluta, exercitationem harum! Voluptatum exercitationem excepturi quo eaque in soluta placeat, magnam officia non unde! Atque vero ad temporibus. Possimus blanditiis dolores fuga, ea alias vero, quasi nostrum ducimus nam numquam non autem sint. Error corporis molestias ipsam! Tenetur cum pariatur fugiat est soluta qui ullam repellendus, eum quaerat vitae, illo sequi error adipisci aspernatur! Facilis, inventore ipsam nesciunt labore dicta id debitis blanditiis ex in dolorem ducimus repellat impedit et recusandae sunt voluptatem repudiandae doloremque fugiat consectetur reiciendis! Tempora, excepturi voluptate. Iusto labore quasi atque? Minima iste officia, dolores earum corporis atque qui aliquam dolorem! Blanditiis, in. Incidunt facere officia debitis culpa error, explicabo neque? Culpa, cupiditate molestias. Tenetur id ducimus aspernatur pariatur necessitatibus atque, facere incidunt aperiam aut, optio dolorum ab modi fugiat quas cumque eligendi obcaecati, expedita veritatis voluptatibus. Suscipit blanditiis aliquam quaerat quod natus minus enim nobis unde iste fugit cupiditate quisquam praesentium quasi obcaecati eaque quas, minima saepe perferendis, ullam nemo numquam ab aut dicta id? Iusto esse voluptatibus, qui quidem suscipit adipisci voluptas ut ab fuga, libero, quos saepe placeat! Esse exercitationem eveniet rerum dicta doloremque commodi, placeat animi voluptatum architecto praesentium labore? Aperiam maxime, impedit animi, harum error ipsam, hic expedita architecto tenetur porro earum at et! Consequuntur ipsam amet quos numquam, velit exercitationem eum in ipsa assumenda ab natus dolorum sapiente deserunt soluta corrupti distinctio? Consequuntur ipsam explicabo aspernatur eaque praesentium animi fugit eum sit nisi commodi iure amet magni doloremque iste saepe, deserunt maxime accusantium recusandae. Esse error aliquam quos laboriosam sint unde dolore libero. Fugit nesciunt magni doloribus, beatae, sapiente praesentium rerum, doloremque ea ducimus fugiat repudiandae alias. Voluptatem nisi dolorem ratione expedita inventore pariatur quis blanditiis optio, ullam reiciendis aut tempora necessitatibus similique quod nam accusamus perspiciatis, quos nulla ad minima eius recusandae, autem maxime id. Quas eveniet repudiandae sapiente aliquid nihil? Quis porro obcaecati nemo eligendi vitae nulla illum consequuntur. At consectetur accusamus quibusdam unde veritatis nulla eligendi doloremque eius corrupti consequuntur? Vitae consequuntur dolores animi adipisci, sed autem eveniet magnam praesentium, ab a hic laborum quasi nobis aperiam. Non ducimus doloribus consequuntur facere est adipisci veniam, ipsam iste itaque optio, qui hic. Illo consequatur cum esse fuga! Obcaecati mollitia ex consectetur, veniam, facere amet dicta explicabo blanditiis et asperiores totam quisquam dolorum rerum, provident magni. Quaerat tempora ducimus esse maxime. Nam, provident cupiditate!', 'проспект Победителей, 99к3', 0x00000000010100000005297387a2803b408b4d948a81ef4a40, 1, '2019-08-18 12:26:00', '2019-09-04 16:38:43', 'Московский', NULL, 0),
(27, 'test-13', 'objects/August2019/VQbgngdDFb3CRKg0XGYp.jpg', '[\"objects\\/August2019\\/Q5YdALVgg6ZAQRrCfnRu.jpg\",\"objects\\/August2019\\/D5rfvWgExb8kJvpFtpsy.jpg\",\"objects\\/August2019\\/65PeCgmq0T6Yi0brP8sW.jpg\",\"objects\\/August2019\\/OaYYu5Mb9XqdJD0JBRSy.jpg\"]', '4-комнатная квартира премиального класса около вдхр. Дрозды с дизайнерским ремонтом.', 280000, 115000, 1470, 620, '[\"sell\",\"rent\"]', 'Квартира', 'Панельный', 4, 50, 10, 18, 5, 1986, 1, 1, '4-комнатная квартира премиального класса около вдхр. Дрозды с дизайнерским ремонтом.', 'Квартира находится в обыкновенном жилом состоянии, что позволяет при желании сразу заехать и жить или, не переплачивая деньги за чужой ремонт, сделать все по своему вкусу...', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestiae tenetur odio sequi quo quidem labore, reiciendis quod eos atque repellat illo sint quia hic doloribus commodi corporis animi, pariatur nesciunt! Nihil quaerat repellendus veritatis eligendi! Dolorum exercitationem odit consectetur excepturi!  Lorem ipsum dolor sit amet consectetur adipisicing elit. Consequatur ab iure soluta atque iste pariatur sapiente blanditiis eligendi dolorem ut aut animi quis maxime qui, quia quasi. Nihil, dicta. Sequi voluptate architecto numquam quia distinctio iusto obcaecati exercitationem recusandae veritatis cumque, ratione, incidunt necessitatibus illum modi? Corrupti, commodi voluptates quidem repellat nobis perferendis quod laborum ducimus qui voluptatum ipsum pariatur doloremque deserunt quaerat quos obcaecati, labore dolorum! Ullam, enim! Vitae unde ex amet magni laudantium minima est veniam labore explicabo voluptas non dolore accusantium, alias necessitatibus sit a nihil suscipit perspiciatis dolorum provident, natus deserunt earum dicta eligendi. Quos possimus totam consectetur molestiae delectus expedita maiores nemo aspernatur non nam sequi cumque quae deleniti soluta, exercitationem harum! Voluptatum exercitationem excepturi quo eaque in soluta placeat, magnam officia non unde! Atque vero ad temporibus. Possimus blanditiis dolores fuga, ea alias vero, quasi nostrum ducimus nam numquam non autem sint. Error corporis molestias ipsam! Tenetur cum pariatur fugiat est soluta qui ullam repellendus, eum quaerat vitae, illo sequi error adipisci aspernatur! Facilis, inventore ipsam nesciunt labore dicta id debitis blanditiis ex in dolorem ducimus repellat impedit et recusandae sunt voluptatem repudiandae doloremque fugiat consectetur reiciendis! Tempora, excepturi voluptate. Iusto labore quasi atque? Minima iste officia, dolores earum corporis atque qui aliquam dolorem! Blanditiis, in. Incidunt facere officia debitis culpa error, explicabo neque? Culpa, cupiditate molestias. Tenetur id ducimus aspernatur pariatur necessitatibus atque, facere incidunt aperiam aut, optio dolorum ab modi fugiat quas cumque eligendi obcaecati, expedita veritatis voluptatibus. Suscipit blanditiis aliquam quaerat quod natus minus enim nobis unde iste fugit cupiditate quisquam praesentium quasi obcaecati eaque quas, minima saepe perferendis, ullam nemo numquam ab aut dicta id? Iusto esse voluptatibus, qui quidem suscipit adipisci voluptas ut ab fuga, libero, quos saepe placeat! Esse exercitationem eveniet rerum dicta doloremque commodi, placeat animi voluptatum architecto praesentium labore? Aperiam maxime, impedit animi, harum error ipsam, hic expedita architecto tenetur porro earum at et! Consequuntur ipsam amet quos numquam, velit exercitationem eum in ipsa assumenda ab natus dolorum sapiente deserunt soluta corrupti distinctio? Consequuntur ipsam explicabo aspernatur eaque praesentium animi fugit eum sit nisi commodi iure amet magni doloremque iste saepe, deserunt maxime accusantium recusandae. Esse error aliquam quos laboriosam sint unde dolore libero. Fugit nesciunt magni doloribus, beatae, sapiente praesentium rerum, doloremque ea ducimus fugiat repudiandae alias. Voluptatem nisi dolorem ratione expedita inventore pariatur quis blanditiis optio, ullam reiciendis aut tempora necessitatibus similique quod nam accusamus perspiciatis, quos nulla ad minima eius recusandae, autem maxime id. Quas eveniet repudiandae sapiente aliquid nihil? Quis porro obcaecati nemo eligendi vitae nulla illum consequuntur. At consectetur accusamus quibusdam unde veritatis nulla eligendi doloremque eius corrupti consequuntur? Vitae consequuntur dolores animi adipisci, sed autem eveniet magnam praesentium, ab a hic laborum quasi nobis aperiam. Non ducimus doloribus consequuntur facere est adipisci veniam, ipsam iste itaque optio, qui hic. Illo consequatur cum esse fuga! Obcaecati mollitia ex consectetur, veniam, facere amet dicta explicabo blanditiis et asperiores totam quisquam dolorum rerum, provident magni. Quaerat tempora ducimus esse maxime. Nam, provident cupiditate!', 'проспект Победителей, 99к3', 0x0000000001010000001629738731883b4022e772cf22f94a40, 1, '2019-08-18 12:26:00', '2019-09-04 16:39:18', 'Центральный', NULL, 0);
INSERT INTO `objects` (`id`, `slug`, `main_image`, `images`, `name`, `price`, `price_usd`, `price_m2`, `price_m2_usd`, `type`, `property_type`, `house_type`, `rooms`, `square`, `living_square`, `kitchen_square`, `floor`, `year`, `repair`, `furniture`, `title`, `description`, `content`, `address`, `coordinates`, `published`, `created_at`, `updated_at`, `districts`, `bitrix_id`, `popular`) VALUES
(28, 'test-14', 'objects/August2019/VQbgngdDFb3CRKg0XGYp.jpg', '[\"objects\\/August2019\\/Q5YdALVgg6ZAQRrCfnRu.jpg\",\"objects\\/August2019\\/D5rfvWgExb8kJvpFtpsy.jpg\",\"objects\\/August2019\\/65PeCgmq0T6Yi0brP8sW.jpg\",\"objects\\/August2019\\/OaYYu5Mb9XqdJD0JBRSy.jpg\"]', '4-комнатная квартира премиального класса около вдхр. Дрозды с дизайнерским ремонтом.', 280000, 115000, 1470, 520, '[\"buy\",\"rent\"]', 'Квартира', 'Панельный', 8, 500, 100, 180, 1, 1986, 1, 1, '4-комнатная квартира премиального класса около вдхр. Дрозды с дизайнерским ремонтом.', 'Квартира находится в обыкновенном жилом состоянии, что позволяет при желании сразу заехать и жить или, не переплачивая деньги за чужой ремонт, сделать все по своему вкусу...', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestiae tenetur odio sequi quo quidem labore, reiciendis quod eos atque repellat illo sint quia hic doloribus commodi corporis animi, pariatur nesciunt! Nihil quaerat repellendus veritatis eligendi! Dolorum exercitationem odit consectetur excepturi!  Lorem ipsum dolor sit amet consectetur adipisicing elit. Consequatur ab iure soluta atque iste pariatur sapiente blanditiis eligendi dolorem ut aut animi quis maxime qui, quia quasi. Nihil, dicta. Sequi voluptate architecto numquam quia distinctio iusto obcaecati exercitationem recusandae veritatis cumque, ratione, incidunt necessitatibus illum modi? Corrupti, commodi voluptates quidem repellat nobis perferendis quod laborum ducimus qui voluptatum ipsum pariatur doloremque deserunt quaerat quos obcaecati, labore dolorum! Ullam, enim! Vitae unde ex amet magni laudantium minima est veniam labore explicabo voluptas non dolore accusantium, alias necessitatibus sit a nihil suscipit perspiciatis dolorum provident, natus deserunt earum dicta eligendi. Quos possimus totam consectetur molestiae delectus expedita maiores nemo aspernatur non nam sequi cumque quae deleniti soluta, exercitationem harum! Voluptatum exercitationem excepturi quo eaque in soluta placeat, magnam officia non unde! Atque vero ad temporibus. Possimus blanditiis dolores fuga, ea alias vero, quasi nostrum ducimus nam numquam non autem sint. Error corporis molestias ipsam! Tenetur cum pariatur fugiat est soluta qui ullam repellendus, eum quaerat vitae, illo sequi error adipisci aspernatur! Facilis, inventore ipsam nesciunt labore dicta id debitis blanditiis ex in dolorem ducimus repellat impedit et recusandae sunt voluptatem repudiandae doloremque fugiat consectetur reiciendis! Tempora, excepturi voluptate. Iusto labore quasi atque? Minima iste officia, dolores earum corporis atque qui aliquam dolorem! Blanditiis, in. Incidunt facere officia debitis culpa error, explicabo neque? Culpa, cupiditate molestias. Tenetur id ducimus aspernatur pariatur necessitatibus atque, facere incidunt aperiam aut, optio dolorum ab modi fugiat quas cumque eligendi obcaecati, expedita veritatis voluptatibus. Suscipit blanditiis aliquam quaerat quod natus minus enim nobis unde iste fugit cupiditate quisquam praesentium quasi obcaecati eaque quas, minima saepe perferendis, ullam nemo numquam ab aut dicta id? Iusto esse voluptatibus, qui quidem suscipit adipisci voluptas ut ab fuga, libero, quos saepe placeat! Esse exercitationem eveniet rerum dicta doloremque commodi, placeat animi voluptatum architecto praesentium labore? Aperiam maxime, impedit animi, harum error ipsam, hic expedita architecto tenetur porro earum at et! Consequuntur ipsam amet quos numquam, velit exercitationem eum in ipsa assumenda ab natus dolorum sapiente deserunt soluta corrupti distinctio? Consequuntur ipsam explicabo aspernatur eaque praesentium animi fugit eum sit nisi commodi iure amet magni doloremque iste saepe, deserunt maxime accusantium recusandae. Esse error aliquam quos laboriosam sint unde dolore libero. Fugit nesciunt magni doloribus, beatae, sapiente praesentium rerum, doloremque ea ducimus fugiat repudiandae alias. Voluptatem nisi dolorem ratione expedita inventore pariatur quis blanditiis optio, ullam reiciendis aut tempora necessitatibus similique quod nam accusamus perspiciatis, quos nulla ad minima eius recusandae, autem maxime id. Quas eveniet repudiandae sapiente aliquid nihil? Quis porro obcaecati nemo eligendi vitae nulla illum consequuntur. At consectetur accusamus quibusdam unde veritatis nulla eligendi doloremque eius corrupti consequuntur? Vitae consequuntur dolores animi adipisci, sed autem eveniet magnam praesentium, ab a hic laborum quasi nobis aperiam. Non ducimus doloribus consequuntur facere est adipisci veniam, ipsam iste itaque optio, qui hic. Illo consequatur cum esse fuga! Obcaecati mollitia ex consectetur, veniam, facere amet dicta explicabo blanditiis et asperiores totam quisquam dolorum rerum, provident magni. Quaerat tempora ducimus esse maxime. Nam, provident cupiditate!', 'проспект Победителей, 99к3', 0x0000000001010000004b2973876e963b4049f5d1f019f84a40, 1, '2019-08-18 12:26:00', '2019-09-04 16:40:17', 'Советский', NULL, 0),
(29, 'test-15', 'objects/August2019/VQbgngdDFb3CRKg0XGYp.jpg', '[\"objects\\/August2019\\/Q5YdALVgg6ZAQRrCfnRu.jpg\",\"objects\\/August2019\\/D5rfvWgExb8kJvpFtpsy.jpg\",\"objects\\/August2019\\/65PeCgmq0T6Yi0brP8sW.jpg\",\"objects\\/August2019\\/OaYYu5Mb9XqdJD0JBRSy.jpg\"]', '4-комнатная квартира премиального класса около вдхр. Дрозды с дизайнерским ремонтом.', 180000, 110000, 1110, 436.5, '[\"buy\"]', 'Квартира', 'Панельный', 3, 50, 10, 18, 5, 1975, 0, 0, '4-комнатная квартира премиального класса около вдхр. Дрозды с дизайнерским ремонтом.', 'Квартира находится в обыкновенном жилом состоянии, что позволяет при желании сразу заехать и жить или, не переплачивая деньги за чужой ремонт, сделать все по своему вкусу...', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestiae tenetur odio sequi quo quidem labore, reiciendis quod eos atque repellat illo sint quia hic doloribus commodi corporis animi, pariatur nesciunt! Nihil quaerat repellendus veritatis eligendi! Dolorum exercitationem odit consectetur excepturi!  Lorem ipsum dolor sit amet consectetur adipisicing elit. Consequatur ab iure soluta atque iste pariatur sapiente blanditiis eligendi dolorem ut aut animi quis maxime qui, quia quasi. Nihil, dicta. Sequi voluptate architecto numquam quia distinctio iusto obcaecati exercitationem recusandae veritatis cumque, ratione, incidunt necessitatibus illum modi? Corrupti, commodi voluptates quidem repellat nobis perferendis quod laborum ducimus qui voluptatum ipsum pariatur doloremque deserunt quaerat quos obcaecati, labore dolorum! Ullam, enim! Vitae unde ex amet magni laudantium minima est veniam labore explicabo voluptas non dolore accusantium, alias necessitatibus sit a nihil suscipit perspiciatis dolorum provident, natus deserunt earum dicta eligendi. Quos possimus totam consectetur molestiae delectus expedita maiores nemo aspernatur non nam sequi cumque quae deleniti soluta, exercitationem harum! Voluptatum exercitationem excepturi quo eaque in soluta placeat, magnam officia non unde! Atque vero ad temporibus. Possimus blanditiis dolores fuga, ea alias vero, quasi nostrum ducimus nam numquam non autem sint. Error corporis molestias ipsam! Tenetur cum pariatur fugiat est soluta qui ullam repellendus, eum quaerat vitae, illo sequi error adipisci aspernatur! Facilis, inventore ipsam nesciunt labore dicta id debitis blanditiis ex in dolorem ducimus repellat impedit et recusandae sunt voluptatem repudiandae doloremque fugiat consectetur reiciendis! Tempora, excepturi voluptate. Iusto labore quasi atque? Minima iste officia, dolores earum corporis atque qui aliquam dolorem! Blanditiis, in. Incidunt facere officia debitis culpa error, explicabo neque? Culpa, cupiditate molestias. Tenetur id ducimus aspernatur pariatur necessitatibus atque, facere incidunt aperiam aut, optio dolorum ab modi fugiat quas cumque eligendi obcaecati, expedita veritatis voluptatibus. Suscipit blanditiis aliquam quaerat quod natus minus enim nobis unde iste fugit cupiditate quisquam praesentium quasi obcaecati eaque quas, minima saepe perferendis, ullam nemo numquam ab aut dicta id? Iusto esse voluptatibus, qui quidem suscipit adipisci voluptas ut ab fuga, libero, quos saepe placeat! Esse exercitationem eveniet rerum dicta doloremque commodi, placeat animi voluptatum architecto praesentium labore? Aperiam maxime, impedit animi, harum error ipsam, hic expedita architecto tenetur porro earum at et! Consequuntur ipsam amet quos numquam, velit exercitationem eum in ipsa assumenda ab natus dolorum sapiente deserunt soluta corrupti distinctio? Consequuntur ipsam explicabo aspernatur eaque praesentium animi fugit eum sit nisi commodi iure amet magni doloremque iste saepe, deserunt maxime accusantium recusandae. Esse error aliquam quos laboriosam sint unde dolore libero. Fugit nesciunt magni doloribus, beatae, sapiente praesentium rerum, doloremque ea ducimus fugiat repudiandae alias. Voluptatem nisi dolorem ratione expedita inventore pariatur quis blanditiis optio, ullam reiciendis aut tempora necessitatibus similique quod nam accusamus perspiciatis, quos nulla ad minima eius recusandae, autem maxime id. Quas eveniet repudiandae sapiente aliquid nihil? Quis porro obcaecati nemo eligendi vitae nulla illum consequuntur. At consectetur accusamus quibusdam unde veritatis nulla eligendi doloremque eius corrupti consequuntur? Vitae consequuntur dolores animi adipisci, sed autem eveniet magnam praesentium, ab a hic laborum quasi nobis aperiam. Non ducimus doloribus consequuntur facere est adipisci veniam, ipsam iste itaque optio, qui hic. Illo consequatur cum esse fuga! Obcaecati mollitia ex consectetur, veniam, facere amet dicta explicabo blanditiis et asperiores totam quisquam dolorum rerum, provident magni. Quaerat tempora ducimus esse maxime. Nam, provident cupiditate!', 'проспект Победителей, 99к3', 0x000000000101000000f328738723873b4046ae6f35f2fa4a40, 1, '2019-08-18 12:26:00', '2019-09-04 16:41:13', 'Центральный', NULL, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `pages`
--

CREATE TABLE `pages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '1',
  `template` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `pages`
--

INSERT INTO `pages` (`id`, `title`, `name`, `description`, `content`, `slug`, `published`, `template`, `created_at`, `updated_at`, `image`) VALUES
(1, 'главная', 'главная', 'главная', NULL, '/', 1, 'index', '2019-08-11 17:09:00', '2019-08-18 05:53:18', 'pages/August2019/j9kx9aT7BvAuyMASoVYW.jpg'),
(2, 'Работа в нашей компании', 'Работа в нашей компании', 'Работа в нашей компании описание', '<div class=\"offset-xl-1 col-md-6 col-xl-5\">\r\n<p data-aos=\"fade\" data-aos-delay=\"200\">&laquo;Уласны дах&raquo; &ndash; современное агентство недвижимости. Мы используем передовые риэлторские технологии. Наша ниша &ndash; продажа, обмен и покупка жилой недвижимости в Минске и пригороде. Агентство &laquo;Уласны дах&raquo; &ndash; это команда профессионалов и единомышленников, которые любят свое дело и с радостью помогают людям продавать и покупать недвижимость.</p>\r\n</div>\r\n<div class=\"col-xl-5 col-md-6\">\r\n<p data-aos=\"fade\" data-aos-delay=\"300\">Мы практикуем передовые риэлторские технологии, постоянно внедряем новые фишки и техники, совершенствуем свои навыки. Работаем много и получаем соответствующую отдачу. Наша ниша &ndash; купля-продажа жилья в Минске и пригороде. Наша цель &ndash; довольные клиенты, получившие риэлторские услуги первоклассного качества.</p>\r\n</div>', 'work-with-us', 1, 'work-with-us', '2019-09-09 02:50:00', '2019-09-22 12:59:11', NULL),
(3, 'О компании', 'О компании', 'О компании описание', NULL, 'about-company', 1, 'about', '2019-09-09 02:58:45', '2019-09-09 02:58:45', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `permissions`
--

INSERT INTO `permissions` (`id`, `key`, `table_name`, `created_at`, `updated_at`) VALUES
(1, 'browse_admin', NULL, '2019-08-03 08:40:04', '2019-08-03 08:40:04'),
(2, 'browse_bread', NULL, '2019-08-03 08:40:04', '2019-08-03 08:40:04'),
(3, 'browse_database', NULL, '2019-08-03 08:40:04', '2019-08-03 08:40:04'),
(4, 'browse_media', NULL, '2019-08-03 08:40:04', '2019-08-03 08:40:04'),
(5, 'browse_compass', NULL, '2019-08-03 08:40:04', '2019-08-03 08:40:04'),
(6, 'browse_menus', 'menus', '2019-08-03 08:40:04', '2019-08-03 08:40:04'),
(7, 'read_menus', 'menus', '2019-08-03 08:40:04', '2019-08-03 08:40:04'),
(8, 'edit_menus', 'menus', '2019-08-03 08:40:04', '2019-08-03 08:40:04'),
(9, 'add_menus', 'menus', '2019-08-03 08:40:04', '2019-08-03 08:40:04'),
(10, 'delete_menus', 'menus', '2019-08-03 08:40:04', '2019-08-03 08:40:04'),
(11, 'browse_roles', 'roles', '2019-08-03 08:40:04', '2019-08-03 08:40:04'),
(12, 'read_roles', 'roles', '2019-08-03 08:40:04', '2019-08-03 08:40:04'),
(13, 'edit_roles', 'roles', '2019-08-03 08:40:04', '2019-08-03 08:40:04'),
(14, 'add_roles', 'roles', '2019-08-03 08:40:04', '2019-08-03 08:40:04'),
(15, 'delete_roles', 'roles', '2019-08-03 08:40:04', '2019-08-03 08:40:04'),
(16, 'browse_users', 'users', '2019-08-03 08:40:05', '2019-08-03 08:40:05'),
(17, 'read_users', 'users', '2019-08-03 08:40:05', '2019-08-03 08:40:05'),
(18, 'edit_users', 'users', '2019-08-03 08:40:05', '2019-08-03 08:40:05'),
(19, 'add_users', 'users', '2019-08-03 08:40:05', '2019-08-03 08:40:05'),
(20, 'delete_users', 'users', '2019-08-03 08:40:05', '2019-08-03 08:40:05'),
(21, 'browse_settings', 'settings', '2019-08-03 08:40:05', '2019-08-03 08:40:05'),
(22, 'read_settings', 'settings', '2019-08-03 08:40:05', '2019-08-03 08:40:05'),
(23, 'edit_settings', 'settings', '2019-08-03 08:40:05', '2019-08-03 08:40:05'),
(24, 'add_settings', 'settings', '2019-08-03 08:40:05', '2019-08-03 08:40:05'),
(25, 'delete_settings', 'settings', '2019-08-03 08:40:05', '2019-08-03 08:40:05'),
(26, 'browse_hooks', NULL, '2019-08-03 08:40:07', '2019-08-03 08:40:07'),
(27, 'browse_blocks', 'blocks', '2019-08-11 16:41:14', '2019-08-11 16:41:14'),
(28, 'read_blocks', 'blocks', '2019-08-11 16:41:14', '2019-08-11 16:41:14'),
(29, 'edit_blocks', 'blocks', '2019-08-11 16:41:14', '2019-08-11 16:41:14'),
(30, 'add_blocks', 'blocks', '2019-08-11 16:41:14', '2019-08-11 16:41:14'),
(31, 'delete_blocks', 'blocks', '2019-08-11 16:41:14', '2019-08-11 16:41:14'),
(32, 'browse_pages', 'pages', '2019-08-11 16:52:56', '2019-08-11 16:52:56'),
(33, 'read_pages', 'pages', '2019-08-11 16:52:56', '2019-08-11 16:52:56'),
(34, 'edit_pages', 'pages', '2019-08-11 16:52:56', '2019-08-11 16:52:56'),
(35, 'add_pages', 'pages', '2019-08-11 16:52:56', '2019-08-11 16:52:56'),
(36, 'delete_pages', 'pages', '2019-08-11 16:52:56', '2019-08-11 16:52:56'),
(37, 'browse_blocks_pages', 'blocks_pages', '2019-08-11 17:15:01', '2019-08-11 17:15:01'),
(38, 'read_blocks_pages', 'blocks_pages', '2019-08-11 17:15:01', '2019-08-11 17:15:01'),
(39, 'edit_blocks_pages', 'blocks_pages', '2019-08-11 17:15:01', '2019-08-11 17:15:01'),
(40, 'add_blocks_pages', 'blocks_pages', '2019-08-11 17:15:01', '2019-08-11 17:15:01'),
(41, 'delete_blocks_pages', 'blocks_pages', '2019-08-11 17:15:01', '2019-08-11 17:15:01'),
(42, 'browse_employers', 'employers', '2019-08-18 06:45:19', '2019-08-18 06:45:19'),
(43, 'read_employers', 'employers', '2019-08-18 06:45:19', '2019-08-18 06:45:19'),
(44, 'edit_employers', 'employers', '2019-08-18 06:45:19', '2019-08-18 06:45:19'),
(45, 'add_employers', 'employers', '2019-08-18 06:45:19', '2019-08-18 06:45:19'),
(46, 'delete_employers', 'employers', '2019-08-18 06:45:19', '2019-08-18 06:45:19'),
(47, 'browse_objects', 'objects', '2019-08-18 07:25:31', '2019-08-18 07:25:31'),
(48, 'read_objects', 'objects', '2019-08-18 07:25:31', '2019-08-18 07:25:31'),
(49, 'edit_objects', 'objects', '2019-08-18 07:25:31', '2019-08-18 07:25:31'),
(50, 'add_objects', 'objects', '2019-08-18 07:25:31', '2019-08-18 07:25:31'),
(51, 'delete_objects', 'objects', '2019-08-18 07:25:31', '2019-08-18 07:25:31'),
(52, 'browse_reviews', 'reviews', '2019-08-23 10:36:35', '2019-08-23 10:36:35'),
(53, 'read_reviews', 'reviews', '2019-08-23 10:36:35', '2019-08-23 10:36:35'),
(54, 'edit_reviews', 'reviews', '2019-08-23 10:36:35', '2019-08-23 10:36:35'),
(55, 'add_reviews', 'reviews', '2019-08-23 10:36:35', '2019-08-23 10:36:35'),
(56, 'delete_reviews', 'reviews', '2019-08-23 10:36:35', '2019-08-23 10:36:35'),
(57, 'browse_blocks_objects', 'blocks_objects', '2019-08-23 16:33:45', '2019-08-23 16:33:45'),
(58, 'read_blocks_objects', 'blocks_objects', '2019-08-23 16:33:45', '2019-08-23 16:33:45'),
(59, 'edit_blocks_objects', 'blocks_objects', '2019-08-23 16:33:45', '2019-08-23 16:33:45'),
(60, 'add_blocks_objects', 'blocks_objects', '2019-08-23 16:33:45', '2019-08-23 16:33:45'),
(61, 'delete_blocks_objects', 'blocks_objects', '2019-08-23 16:33:45', '2019-08-23 16:33:45');

-- --------------------------------------------------------

--
-- Структура таблицы `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(26, 1),
(27, 1),
(28, 1),
(29, 1),
(30, 1),
(31, 1),
(32, 1),
(33, 1),
(34, 1),
(35, 1),
(36, 1),
(37, 1),
(38, 1),
(39, 1),
(40, 1),
(41, 1),
(42, 1),
(43, 1),
(44, 1),
(45, 1),
(46, 1),
(47, 1),
(48, 1),
(49, 1),
(50, 1),
(51, 1),
(52, 1),
(53, 1),
(54, 1),
(55, 1),
(56, 1),
(57, 1),
(58, 1),
(59, 1),
(60, 1),
(61, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `reviews`
--

CREATE TABLE `reviews` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `item_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `item_id` bigint(20) UNSIGNED NOT NULL,
  `text` text COLLATE utf8mb4_unicode_ci,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `video` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `reviews`
--

INSERT INTO `reviews` (`id`, `user_id`, `item_type`, `item_id`, `text`, `photo`, `video`, `created_at`, `updated_at`) VALUES
(1, 2, 'App\\Page', 1, '<p>Выражаю огромную благодарность Екатерине Малец за помощь в продаже квартиры. Самостоятельная двухмесячная продажа не принесла успеха, после чего доверился агентству недвижимости \"А\", и менее чем за две недели квартира была продана. Екатерина показала себя настоящим профессионалом своего дела. <br /><br />СПАСИБО!!!</p>', NULL, NULL, '2019-08-23 10:43:00', '2019-08-27 08:12:37'),
(2, 1, 'App\\Page', 1, '<p>Выражаю огромную благодарность Екатерине Малец за помощь в продаже квартиры. Самостоятельная двухмесячная продажа не принесла успеха, после чего доверился агентству недвижимости \"А\", и менее чем за две недели квартира была продана. Екатерина показала себя настоящим профессионалом своего дела. <br /><br />СПАСИБО!!!</p>', NULL, '1La4QzGeaaQ', '2019-08-23 15:49:55', '2019-08-23 15:49:55'),
(3, 2, 'App\\Page', 1, '<p>Выражаю огромную благодарность Екатерине Малец за помощь в продаже квартиры. Самостоятельная двухмесячная продажа не принесла успеха, после чего доверился агентству недвижимости \"А\", и менее чем за две недели квартира была продана. Екатерина показала себя настоящим профессионалом своего дела. <br /><br />СПАСИБО!!!</p>', NULL, '1La4QzGeaaQ', '2019-08-23 15:50:00', '2019-08-23 16:25:40');

-- --------------------------------------------------------

--
-- Структура таблицы `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Administrator', '2019-08-03 08:40:03', '2019-08-03 08:40:03'),
(2, 'user', 'Normal User', '2019-08-03 08:40:04', '2019-08-03 08:40:04');

-- --------------------------------------------------------

--
-- Структура таблицы `saved_forms`
--

CREATE TABLE `saved_forms` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `form_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `page_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `details` text COLLATE utf8mb4_unicode_ci,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `group` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `settings`
--

INSERT INTO `settings` (`id`, `key`, `display_name`, `value`, `details`, `type`, `order`, `group`) VALUES
(1, 'site.title', 'Site Title', 'Site Title', '', 'text', 1, 'Site'),
(2, 'site.description', 'Site Description', 'Site Description', '', 'text', 2, 'Site'),
(3, 'site.logo', 'Site Logo', 'settings/August2019/8rNve6KDZImEUkRBpPso.png', '', 'image', 3, 'Site'),
(4, 'site.google_analytics_tracking_id', 'Google Analytics Tracking ID', NULL, '', 'text', 4, 'Site'),
(5, 'admin.bg_image', 'Admin Background Image', '', '', 'image', 5, 'Admin'),
(6, 'admin.title', 'Admin Title', 'Voyager', '', 'text', 1, 'Admin'),
(7, 'admin.description', 'Admin Description', 'Welcome to Voyager. The Missing Admin for Laravel', '', 'text', 2, 'Admin'),
(8, 'admin.loader', 'Admin Loader', '', '', 'image', 3, 'Admin'),
(9, 'admin.icon_image', 'Admin Icon Image', '', '', 'image', 4, 'Admin'),
(10, 'admin.google_analytics_client_id', 'Google Analytics Client ID (used for admin dashboard)', NULL, '', 'text', 1, 'Admin'),
(11, 'site.phone', 'Телефон', '+372 29 777 77 77', NULL, 'text', 6, 'Site'),
(12, 'site.phone2', 'Телефон2', '+372 29 777 77 77', NULL, 'text', 7, 'Site'),
(13, 'site.worktime', 'Время работы', 'Ежедневно с 9-00 до 18-00', NULL, 'text', 8, 'Site'),
(14, 'site.logo_footer', 'Логотип footer', 'settings/August2019/V9AzOC10Vyro0Ph2yM2J.png', NULL, 'image', 9, 'Site'),
(15, 'site.address', 'Адрес', 'г.Минск, ул. М.Танка, 30', NULL, 'text', 10, 'Site'),
(16, 'site.vk', 'Vk', 'https://vk.com', NULL, 'text', 11, 'Site'),
(17, 'site.fb', 'Facebook', 'https://fb.com', NULL, 'text', 12, 'Site'),
(18, 'site.tw', 'Twitter', 'https://twitter.com', NULL, 'text', 13, 'Site'),
(19, 'site.license', 'Лицензия', 'ООО «Уласны Дах» Лицензия на риэлтерскую деятельность № 02240/344,\r\n            выдана 10.07.2017, Министерством юстиции Республики Беларусь', NULL, 'text_area', 14, 'Site');

-- --------------------------------------------------------

--
-- Структура таблицы `translations`
--

CREATE TABLE `translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) UNSIGNED NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'users/default.png',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `settings` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `role_id`, `name`, `email`, `avatar`, `email_verified_at`, `password`, `remember_token`, `settings`, `created_at`, `updated_at`) VALUES
(1, 1, 'admin', 'als_1984@mail.ru', 'users/default.png', NULL, '$2y$10$Gw5Nan8izD0m3B6qfclFROxTSkOPAhgxJdANZuIiFl.stCup/xIhC', 'SVvzvP6EK4pgXdB23wJKPXjjaa3Fc2ddHOTFbO1J6JSR7LzDN9XrVgDYmeM9', '{\"locale\":\"ru\"}', '2019-08-03 08:40:31', '2019-08-10 07:51:52'),
(2, 1, 'Pavel', 'volohovich@thakairus.com', 'users/default.png', NULL, '$2y$10$Hmo3/G7FM9zrexRGenBXce7PetuvmKVqAKPNvj.5xTT5/FtFJouFi', 'GWxQzub2itFbZxU3xR25UzDUYYCoq3n5X4RzAfYXhdA8dAOVdTOnXMtdBjnE', '{\"locale\":\"ru\"}', '2019-08-13 04:26:11', '2019-08-13 04:26:11');

-- --------------------------------------------------------

--
-- Структура таблицы `user_roles`
--

CREATE TABLE `user_roles` (
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `user_roles`
--

INSERT INTO `user_roles` (`user_id`, `role_id`) VALUES
(2, 1);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `blocks`
--
ALTER TABLE `blocks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `blocks_name_index` (`name`);

--
-- Индексы таблицы `blocks_objects`
--
ALTER TABLE `blocks_objects`
  ADD PRIMARY KEY (`id`),
  ADD KEY `blocks_objects_object_id_foreign` (`object_id`),
  ADD KEY `blocks_objects_block_id_foreign` (`block_id`);

--
-- Индексы таблицы `blocks_pages`
--
ALTER TABLE `blocks_pages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `blocks_pages_page_id_foreign` (`page_id`),
  ADD KEY `blocks_pages_block_id_foreign` (`block_id`);

--
-- Индексы таблицы `data_rows`
--
ALTER TABLE `data_rows`
  ADD PRIMARY KEY (`id`),
  ADD KEY `data_rows_data_type_id_foreign` (`data_type_id`);

--
-- Индексы таблицы `data_types`
--
ALTER TABLE `data_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `data_types_name_unique` (`name`),
  ADD UNIQUE KEY `data_types_slug_unique` (`slug`);

--
-- Индексы таблицы `employers`
--
ALTER TABLE `employers`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `employer_objects`
--
ALTER TABLE `employer_objects`
  ADD PRIMARY KEY (`id`),
  ADD KEY `employer_objects_facility_id_foreign` (`facility_id`),
  ADD KEY `employer_objects_employer_id_foreign` (`employer_id`);

--
-- Индексы таблицы `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `menus_name_unique` (`name`);

--
-- Индексы таблицы `menu_items`
--
ALTER TABLE `menu_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_items_menu_id_foreign` (`menu_id`);

--
-- Индексы таблицы `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `objects`
--
ALTER TABLE `objects`
  ADD PRIMARY KEY (`id`),
  ADD KEY `objects_slug_index` (`slug`),
  ADD KEY `objects_name_index` (`name`),
  ADD KEY `objects_price_usd_index` (`price_usd`),
  ADD KEY `objects_price_m2_usd_index` (`price_m2_usd`),
  ADD KEY `objects_type_index` (`type`),
  ADD KEY `objects_property_type_index` (`property_type`),
  ADD KEY `objects_house_type_index` (`house_type`),
  ADD KEY `objects_rooms_index` (`rooms`),
  ADD KEY `objects_square_index` (`square`),
  ADD KEY `objects_living_square_index` (`living_square`),
  ADD KEY `objects_kitchen_square_index` (`kitchen_square`),
  ADD KEY `objects_floor_index` (`floor`),
  ADD KEY `objects_year_index` (`year`),
  ADD KEY `objects_repair_index` (`repair`),
  ADD KEY `objects_furniture_index` (`furniture`),
  ADD KEY `objects_published_index` (`published`);

--
-- Индексы таблицы `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pages_slug_index` (`slug`);

--
-- Индексы таблицы `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Индексы таблицы `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permissions_key_index` (`key`);

--
-- Индексы таблицы `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_permission_id_index` (`permission_id`),
  ADD KEY `permission_role_role_id_index` (`role_id`);

--
-- Индексы таблицы `reviews`
--
ALTER TABLE `reviews`
  ADD PRIMARY KEY (`id`),
  ADD KEY `reviews_user_id_foreign` (`user_id`);

--
-- Индексы таблицы `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Индексы таблицы `saved_forms`
--
ALTER TABLE `saved_forms`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `settings_key_unique` (`key`);

--
-- Индексы таблицы `translations`
--
ALTER TABLE `translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- Индексы таблицы `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `user_roles_user_id_index` (`user_id`),
  ADD KEY `user_roles_role_id_index` (`role_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `blocks`
--
ALTER TABLE `blocks`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT для таблицы `blocks_objects`
--
ALTER TABLE `blocks_objects`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT для таблицы `blocks_pages`
--
ALTER TABLE `blocks_pages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT для таблицы `data_rows`
--
ALTER TABLE `data_rows`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=114;

--
-- AUTO_INCREMENT для таблицы `data_types`
--
ALTER TABLE `data_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT для таблицы `employers`
--
ALTER TABLE `employers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `employer_objects`
--
ALTER TABLE `employer_objects`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `menu_items`
--
ALTER TABLE `menu_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT для таблицы `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT для таблицы `objects`
--
ALTER TABLE `objects`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT для таблицы `pages`
--
ALTER TABLE `pages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;

--
-- AUTO_INCREMENT для таблицы `reviews`
--
ALTER TABLE `reviews`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `saved_forms`
--
ALTER TABLE `saved_forms`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT для таблицы `translations`
--
ALTER TABLE `translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `blocks_objects`
--
ALTER TABLE `blocks_objects`
  ADD CONSTRAINT `blocks_objects_block_id_foreign` FOREIGN KEY (`block_id`) REFERENCES `blocks` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `blocks_objects_object_id_foreign` FOREIGN KEY (`object_id`) REFERENCES `objects` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `blocks_pages`
--
ALTER TABLE `blocks_pages`
  ADD CONSTRAINT `blocks_pages_block_id_foreign` FOREIGN KEY (`block_id`) REFERENCES `blocks` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `blocks_pages_page_id_foreign` FOREIGN KEY (`page_id`) REFERENCES `pages` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `data_rows`
--
ALTER TABLE `data_rows`
  ADD CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `employer_objects`
--
ALTER TABLE `employer_objects`
  ADD CONSTRAINT `employer_objects_employer_id_foreign` FOREIGN KEY (`employer_id`) REFERENCES `employers` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `employer_objects_facility_id_foreign` FOREIGN KEY (`facility_id`) REFERENCES `objects` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `menu_items`
--
ALTER TABLE `menu_items`
  ADD CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `reviews`
--
ALTER TABLE `reviews`
  ADD CONSTRAINT `reviews_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Ограничения внешнего ключа таблицы `user_roles`
--
ALTER TABLE `user_roles`
  ADD CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
