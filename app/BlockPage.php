<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlockPage extends Model
{
    public $table='blocks_pages';
    public $timestamps=false;

    public $fillable=['image','title','text','file'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function block()
    {
        return $this->belongsTo(Block::class,'block_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function page()
    {
        return $this->belongsTo(Page::class,'page_id');
    }
}
