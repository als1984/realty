<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employer extends Model
{
    public $fillable=['first_name','last_name','phones','email','department','position','description'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function objects()
    {
        return $this->belongsToMany(Facility::class,'employer_objects','employer_id','facility_id');
    }

    public function reviews()
    {
        return $this->morphMany(Review::class,'item');
    }
}
