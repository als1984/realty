<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Block extends Model
{
    public $fillable=['name','description'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function pages()
    {
        return $this->belongsToMany(Page::class,'blocks_pages','block_id','page_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function objects()
    {
        return $this->belongsToMany(Object::class,'blocks_objects','block_id','object_id');
    }
}
