<?php
/**
 * Created by PhpStorm.
 * User: sanek
 * Date: 06.10.19
 * Time: 20:53
 */

namespace App\Traits;


use App\BitrixToken;
use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\Log;

trait BitrixAuthTrait
{
    public function getTokenDb()
    {
        $token=BitrixToken::latest()->first();
        if(!$token){
            return false;
        }
        if($token->expired<=Carbon::now()){
            return false;
        }

        return $token;
    }

    public function refreshRequest()
    {
        $client=new Client(['base_uri'=>'https://oauth.bitrix.info']);
        $params=['query' => [
            'grant_type'=>'refresh_token',
            'client_id' => env('BITRIX_APP_ID','local.5d8789d5a444b6.43518446'),
            'client_secret' => env('BITRIX_APP_SECRET','AA3DhoH1kLDhU9lZvu2wgc7FxaXpXscOCGOIDPp2hhOp7nZjJ1'),
            "refresh_token" => BitrixToken::latest()->first()->refresh_token,
            //"redirect_uri" => route('token'),
            //"SCOPE" => array('crm')
            //{"access_token":"6381a35d0040723c001fb0b80000021b0000033e703ccfb22f4d3d98765e4d6a49b140","expires":1570996579,"expires_in":3600,"scope":"app","domain":"portal.ulasnydax.by","server_endpoint":"https:\/\/oauth.bitrix.info\/rest\/","status":"L","client_endpoint":"https:\/\/portal.ulasnydax.by\/rest\/","member_id":"a72b4e33d6f3af0ce2fdc6b31b33908f","user_id":539,"refresh_token":"5300cb5d0040723c001fb0b80000021b000003c415a73c5124d29651033658d6e5462b"}
        ]];
        $res=$client->request('GET','/oauth/token/',$params);
        if($res && $res->getStatusCode()==200){
            $resp=json_decode($res->getBody(),1);
            $token=BitrixToken::latest()->first();
            $token->token=$resp['access_token'];
            $token->refresh_token=$resp['refresh_token'];
            $token->expired=Carbon::createFromTimestamp($resp['expires']);
            $token->save();
        }else{
            return false;
        }
        Log::info($res->getStatusCode());
// "200"
        Log::info($res->getHeader('content-type')[0]);
// 'application/json; charset=utf8'
        Log::info( $res->getBody());
        return $token;
    }
}
