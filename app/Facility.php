<?php

namespace App;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Support\Str;
use TCG\Voyager\Traits\Spatial;
use Illuminate\Database\Eloquent\Model;
use YaGeo;

/**
 * @SWG\Definition(
 *  definition="Object",
 *  @SWG\Property(
 *      property="id",
 *      type="integer"
 *  ),
 *  @SWG\Property(
 *      property="coordinates",
 *      type="array",
 *      description="Координаты в массиве ['lat','lng']",
 *      @SWG\Items(
 *             type="number",
 *         ),
 *  ),
 *  @SWG\Property(
 *      property="slug",
 *      type="string",
 *      description="URL страницы (http:/sitename.ru/objects/url)"
 *  ),
 *  @SWG\Property(
 *      property="main_image",
 *      type="string",
 *      description="Основная картинка впереди нужно добавить /storage/ "
 *  ),
 *  @SWG\Property(
 *      property="images",
 *      type="array",
 *      description="Картинки (массив) впереди нужно добавить /storage/",
 *     @SWG\Items(
 *             type="string",
 *         )
 *  ),
 *  @SWG\Property(
 *      property="name",
 *      type="string",
 *      description="Название"
 *  ),
 *  @SWG\Property(
 *      property="address",
 *      type="string",
 *      description="Адрес "
 *  ),
 *  @SWG\Property(
 *      property="content",
 *      type="string",
 *      description="Описание"
 *  ),
 *     @SWG\Property(
 *      property="price",
 *      type="number",
 *      description="Цена BYN"
 *  ),
 *     @SWG\Property(
 *      property="price_usd",
 *      type="number",
 *      description="Цена USD"
 *  ),
 *     @SWG\Property(
 *      property="price_m2",
 *      type="number",
 *      description="Цена за м2 BYN"
 *  ),
 *     @SWG\Property(
 *      property="price_m2_usd",
 *      type="number",
 *      description="Цена за м2 USD"
 *  ),
 *     @SWG\Property(
 *      property="type",
 *      type="array",
 *      description="Тип (продажа, аренда ...)",
 *     @SWG\Items(
 *             type="string",
 *         )
 *  ),
 *     @SWG\Property(
 *      property="property_type",
 *      type="string",
 *      description="Тип собственности"
 *  ),
 *     @SWG\Property(
 *      property="house_type",
 *      type="string",
 *      description="Тип здания"
 *  ),
 *        @SWG\Property(
 *      property="rooms",
 *      type="integer",
 *      description="Кол-во комнат"
 *  ),
 *       @SWG\Property(
 *      property="square",
 *      type="number",
 *      description="Общая площадь"
 *  ),
 *    @SWG\Property(
 *      property="living_square",
 *      type="number",
 *      description="Жилая площадь"
 *  ),
 *        @SWG\Property(
 *      property="kitchen_square",
 *      type="number",
 *      description="Площадь кухни"
 *  ),
 *        @SWG\Property(
 *      property="floor",
 *      type="integer",
 *      description="Этаж"
 *  ),
 *          @SWG\Property(
 *      property="year",
 *      type="integer",
 *      description="Год постройки 4 цифры"
 *  ),
 *          @SWG\Property(
 *      property="repair",
 *      type="boolean",
 *      description="Есть ремонт?"
 *  ),
 *          @SWG\Property(
 *      property="furniture",
 *      type="boolean",
 *      description="Есть мебель?"
 *  ),
 *          @SWG\Property(
 *      property="title",
 *      type="string",
 *      description="meta title"
 *  ),
 *          @SWG\Property(
 *      property="description",
 *      type="string",
 *      description="meta description"
 *  ),        @SWG\Property(
 *      property="districts",
 *      type="string",
 *      description="Район"
 *  ),        @SWG\Property(
 *      property="popular",
 *      type="boolean",
 *      description="Популярный? (выводится на главной)"
 *  ),
 *     @SWG\Property(
 *      property="published",
 *      type="boolean",
 *      description="Опубликован?"
 *  ),
 *       @SWG\Property(
 *      property="created_at",
 *      type="string",
 *      description="дата создания"
 *  ),
 *    @SWG\Property(
 *      property="updated_at",
 *      type="string",
 *      description="дата изменения"
 *  ),
 *      @SWG\Property(
 *      property="bitrix_id",
 *      type="string",
 *      description="ID из битрикс (служебное поле)"
 *  ),
 *     @SWG\Property(
 *      property="neighbors",
 *      type="array",
 *      description="соседи - массив id не всегда передается",
 *      @SWG\Items(
 *             type="number",
 *         ),
 *  ),
 *      @SWG\Property(
 *      property="small_tile",
 *      type="string",
 *      description="Плашка"),
 *  )
 * )
 */

/**
 * @SWG\Definition(
 *  definition="Filters",
 *  @SWG\Property(
 *      property="min_price",
 *      type="number",
 *     description="Минимальная цена BYN"
 *  ),
 *  @SWG\Property(
 *      property="min_price_usd",
 *      type="number",
 *     description="Минимальная цена USD"
 *  ),
 * @SWG\Property(
 *      property="max_price",
 *      type="number",
 *     description="Максимальная цена"
 *  ),
 *     @SWG\Property(
 *      property="max_price_usd",
 *      type="number",
 *     description="Максимальная цена USD"
 *  ),
 *     @SWG\Property(
 *      property="min_price_m2",
 *      type="number",
 *     description="Минимальная цена m2 BYN"
 *  ),
 *     @SWG\Property(
 *      property="min_price_m2_usd",
 *      type="number",
 *     description="Минимальная цена m2 USD"
 *  ),
 *     @SWG\Property(
 *      property="max_price_m2",
 *      type="number",
 *     description="Максимальная цена m2"
 *  ),
 *     @SWG\Property(
 *      property="max_price_m2_usd",
 *      type="number",
 *     description="Максимальная цена m2 USD"
 *  ),
 *     @SWG\Property(
 *      property="min_square",
 *      type="number",
 *     description="Минимальная площадь"
 *  ),
 *     @SWG\Property(
 *      property="min_living_square",
 *      type="number",
 *     description="Минимальная жилая площадь"
 *  ),
 *       @SWG\Property(
 *      property="min_kitchen_square",
 *      type="number",
 *     description="Минимальная площадь кухни"
 *  ),
 *         @SWG\Property(
 *      property="max_square",
 *      type="number",
 *     description="Максимальная площадь"
 *  ),
 *     @SWG\Property(
 *      property="max_living_square",
 *      type="number",
 *     description="Максимальная жилая площадь"
 *  ),
 *       @SWG\Property(
 *      property="max_kitchen_square",
 *      type="number",
 *     description="Максимальная площадь кухни"
 *  ),
 *        @SWG\Property(
 *      property="min_year",
 *      type="number",
 *     description="Минимальная год постройки"
 *  ),
 *        @SWG\Property(
 *      property="max_year",
 *      type="number",
 *     description="Максимальный год постройки"
 *  ),
 *        @SWG\Property(
 *      property="max_floor",
 *      type="number",
 *     description="Максимальный этаж"
 *  ),
 *        @SWG\Property(
 *      property="min_floor",
 *      type="number",
 *     description="Минимальный этаж"
 *  ),
 *     @SWG\Property(
 *      property="types",
 *      type="array",
 *      description="Тип (продажа, аренда ...)",
 *     @SWG\Items(
 *             type="string",
 *         )
 *  ),
 *     @SWG\Property(
 *      property="property_types",
 *      type="array",
 *      description="Тип собственности",
 *     @SWG\Items(
 *             type="string",
 *         )
 *  ),
 *     @SWG\Property(
 *      property="house_types",
 *      type="array",
 *      description="Тип помещения",
 *     @SWG\Items(
 *             type="string",
 *         )
 *  ),
 *
 * )
 */

class Facility extends Model
{

    use Sluggable,Spatial;

    protected $table='objects';

    protected $spatial = ['coordinates'];

    public $fillable = [
        "slug",
        "main_image",
        "images",
        "name",
        "price",
        "price_usd",
        "price_m2",
        "price_m2_usd",
        "type",
        "property_type",
        "house_type",
        "rooms",
        "square",
        "living_square",
        "kitchen_square",
        "floor",
        "year",
        "repair",
        "furniture",
        "title",
        "description",
        "content",
        "address",
        "coordinates",
        "published",
        "created_at",
        "updated_at",
        "districts",
        "bitrix_id",
        "popular"];

    /**
     * @return array
     */
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function employers()
    {
        return $this->belongsToMany(Employer::class,'employer_objects','facility_id','employer_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function reviews()
    {
        return $this->morphMany(Review::class,'item');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function blocks()
    {
        return $this->hasMany(BlockObject::class,'object_id','id');
    }

    public function save(array $options = [])
    {
        //dd($this);
        try {
            if ($this->address) {
                $data = YaGeo::setQuery($this->address)->load();
                //dd($data);
                $lat = $data->getResponse()->getLatitude();
                $lng = $data->getResponse()->getLongitude();
                $this->coordinates = \DB::raw("GeomFromText('POINT($lng $lat)')");
            }
        }catch (\Exception $e){
            Log::error('Yandex geo error');
        }finally{
            return parent::save($options);
        }

        //return parent::save($options); // TODO: Change the autogenerated stub
    }

    public function titleTrim()
    {
        $str=$this->name;
        if(mb_strlen($this->name)>120){
            return Str::limit($this->name,117);
        }elseif (mb_strlen($this->name)<120){
            $k=mb_strlen($this->name);
            for ($i=$k;$i<120;$i++){
                $str.=' ';
            }
        }
        return $str;
    }

}
