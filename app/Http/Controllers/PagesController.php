<?php

namespace App\Http\Controllers;

use App\Employer;
use App\Facility;
use App\Page;
use Illuminate\Http\Request;

class PagesController extends Controller
{
    /**
     * main page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $page=Page::find(1);
        $tpl=$page->template;
        $blocks=$page->blocks()->withPivot('title', 'text','image','file','svg')->get();
        $employers=Employer::inRandomOrder()->limit(4)->get();
        $objects=Facility::where('popular',true)->inRandomOrder()->limit(4)->get();
        foreach ($objects as $object) {
            $images = [$object->main_image];
            $od_images = json_decode($object->images, 1);
            if ($od_images){
                $images += $od_images;
            }
            $object->images=json_encode($images);
        }
        $reviews=$page->reviews()->get();
        return view('pages.'.$tpl,compact('page','blocks','employers','objects','reviews'));
    }

    public function pages(string $slug)
    {
        $page=Page::where('slug',$slug)->firstOrFail();
        $tpl=$page->template;
        $reviews=$page->reviews()->get();
        $blocks=$page->blocks()->withPivot('title', 'text','image','file','svg','order')->orderBy('pivot_order','asc')->get();
        return view('pages.'.$tpl,compact('page','blocks','reviews'));
    }
}
