<?php

namespace App\Http\Controllers;

use App\BitrixToken;
use App\Traits\BitrixAuthTrait;
use Bitrix24\CRM\Lead;
use Bitrix24\CRM\Status;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Bitrix24\Bitrix24;
use Bitrix24\CRM\Contact;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class FormContactsController extends Controller
{
    use BitrixAuthTrait;

    public function addContact(Request $request)
    {
        $data=$request->validate([
            'name'=>'required|string',
            'phone'=>'required|string',
            'type'=>'nullable|string',
            'price'=>'nullable|string',
            'employer'=>'nullable|string',
            'place' =>'nullable|string',
            'lead_id'=>'nullable|integer',
            'person_id'=>'nullable|integer',
            'lead_type'=>'nullable|integer',
            'obj_address'=>'nullable|string',
            'obj_id'=>'nullable|string',
        ]);
        if(!isset($data['type']) || !$data['type']){
            $data['type']='Не выбран';
        }

        $data['phone']=preg_replace("/[^0-9]/", '', $data['phone']);
        $token=$this->getTokenDb();
        if(!$token){

            $token=$this->refreshRequest();
        }
        if(!$token){
            Log::error('cannot get token');
            return response()->json(['error'=>'Произошла ошибка при отправке заявки, свяжитесь с нами по телефону '.setting('site.phone'), 'heading'=>'Ошибка'], 400);
        }
        $log = new Logger('bitrix24');
        $log->pushHandler(new StreamHandler(storage_path('logs/laravel.log'), Logger::DEBUG));
        $obB24App = new Bitrix24(false, $log);
        $obB24App->setApplicationScope(['crm']);
        $obB24App->setApplicationId(env('BITRIX_APP_ID','local.5d8789d5a444b6.43518446'));
        $obB24App->setApplicationSecret(env('BITRIX_APP_SECRET','AA3DhoH1kLDhU9lZvu2wgc7FxaXpXscOCGOIDPp2hhOp7nZjJ1'));
        $obB24App->setDomain(env('BITRIX_DOMAIN','portal.ulasnydax.by'));
        $obB24App->setAccessToken($token->token);
        $obB24App->setRefreshToken($token->refresh_token);
        $params=[];
        $params['TITLE']='Заявка с сайта, тип - '.$data['type'];
        $params['STATUS_ID']='NEW';
        $params['ASSIGNED_BY_ID']=504;
        $params['NAME']=$data['name'];
        $params['PHONE'][0]=['VALUE'=>$data['phone'],'VALUE_TYPE'=>'WORK'];
        $params['SOURCE_ID']=9;
        $params['COMMENTS']='';
        if(isset($data['price']) && $data['price']){
            $params['COMMENTS']='Указанная cтоимость - '.$data['price'];
        }
        if(isset($data['employer']) && $data['employer']){
            $params['COMMENTS']='Сотрудник - '.$data['employer'];
        }
        if(isset($data['place']) && $data['place']){
            $params['COMMENTS'].=' Форма отправлена из '.($data['place']=='footer')?'футера':'хэдера';
        }
        $params=$this->getLead($data,$params);
        $lead = new Lead($obB24App);

        $lead->add($params);
        return response()->json(['text'=>'Ваша заявка отправлена, мы свяжемся с вами в ближайшее время', 'heading'=>'Заявка отправлена']);
    }

    public function addInvoice(Request $request)
    {
        $data = $request->validate([
            'name' => 'required|string',
            'phone' => 'required|string',
            'last_name' => 'nullable|string',
            'email' => 'nullable|email',
            'invoice_type' => 'nullable|integer',
            'person_id'=>'nullable|integer'
        ]);

        if (($request->hasFile('file')) && $request->file('file')->isValid()) {
            $extension = $request->file('file')->extension();
            $data['file'] = $request->file('file')->storeAs('uploads', 'anketa.' . $extension, 'local');
        }
        $token = $this->getTokenDb();
        if (!$token) {

            $token = $this->refreshRequest();
        }
        if (!$token) {
            Log::error('cannot get token');
            return response()->json(['error' => 'Произошла ошибка при отправке заявки, свяжитесь с нами по телефону ' . setting('site.phone'), 'heading' => 'Ошибка'], 400);
        }
        $log = new Logger('bitrix24');
        $log->pushHandler(new StreamHandler(storage_path('logs/laravel.log'), Logger::DEBUG));
        $obB24App = new Bitrix24(false, $log);
        $obB24App->setApplicationScope(['crm']);
        $obB24App->setApplicationId(env('BITRIX_APP_ID', 'local.5d8789d5a444b6.43518446'));
        $obB24App->setApplicationSecret(env('BITRIX_APP_SECRET', 'AA3DhoH1kLDhU9lZvu2wgc7FxaXpXscOCGOIDPp2hhOp7nZjJ1'));
        $obB24App->setDomain(env('BITRIX_DOMAIN', 'portal.ulasnydax.by'));
        $obB24App->setAccessToken($token->token);
        $obB24App->setRefreshToken($token->refresh_token);
        $contact = new Contact($obB24App);
        $status = new Status($obB24App);
        $type = $status->get(1);
        //dd($status->getList(["SORT"=> "ASC"],['ENTITY_ID'=>'CONTACT_TYPE']));
        $params = [];
        $params['NAME'] = $data['name'];
        $params['LAST_NAME'] = isset($data['last_name']) ? $data['last_name'] : '';
        $params['SECOND_NAME'] = 'Соискатель';
        $params['TYPE_ID'] = '7';
        $params['PHONE'][0] = ['VALUE' => $data['phone'], 'VALUE_TYPE' => 'WORK'];
        if(isset($data['email'])) {
            $params['EMAIL'][0] = ['VALUE' => $data['email'], 'VALUE_TYPE' => 'WORK'];
        }
        $params['COMMENTS'] = 'Соискатель с сайта';
        $params['ASSIGNED_BY_ID']=572;
        if (isset($data['file']) && $data['file']) {
            $file = Storage::disk('local')->get($data['file']);
            $params['UF_CRM_1576232064'] = ['fileData' => [basename($data['file']), base64_encode($file)]];
        }
        if (isset($data['invoice_type']) && $data['invoice_type']){
            $params['UF_CRM_1579267449295'] = $data['invoice_type'];
        }
        if(isset($data['person_id']) && $data['person_id']){
            $params['ASSIGNED_BY_ID']=$data['person_id'];
        }
        $contact->add($params);
        //$fields=$contact->fields();
        return response()->json(['text'=>'Ваша заявка отправлена, мы свяжемся с вами в ближайшее время', 'heading'=>'Заявка отправлена']);
    }

    public function getLead($data,$params)
    {
        if(isset($data['lead_id'])){
            $lead=\App\Lead::find($data['lead_id']);
            if(!$lead) return $params;
            $params['ASSIGNED_BY_ID']=$lead->person_id;
            if(isset($data['person_id'])){
                $params['ASSIGNED_BY_ID']=$data['person_id'];
            }
            //$params['ASSIGNED_BY_ID']=$lead->person_id;
            $params['UF_CRM_1579264780056']=$lead->site_point_id;
            $params['UF_CRM_1579264846748']=$lead->type_online_id;
            $comment=$lead->comment;
            if(isset($data['lead_type'])){
                if($data['lead_type']==264){
                    $comment=str_replace('lead_type','Продавец',$comment);
                }elseif ($data['lead_type']==265){
                    $comment=str_replace('lead_type','Покупатель',$comment);
                }
                $params['UF_CRM_1558595658858']=$data['lead_type'];
            }else{
                $comment=str_replace('lead_type','не указано',$comment);
            }
            if(isset($data['obj_address'])){
                $comment=str_replace('obj_address',$data['obj_address'],$comment);
            }else{
                $comment=str_replace('obj_address','',$comment);
            }
            if(isset($data['price'])){
                $comment=str_replace('price',$data['price'],$comment);
            }else{
                $comment=str_replace('price','',$comment);
            }
            if(isset($data['obj_id'])){
                $params['UF_CRM_1580303746306']=$data['obj_id'];
            }
            $params['COMMENTS']=$comment;
        }
        return $params;
    }
}
