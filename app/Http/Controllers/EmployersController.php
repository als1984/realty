<?php

namespace App\Http\Controllers;

use App\Employer;
use App\Page;
use Illuminate\Http\Request;

class EmployersController extends Controller
{
    public function index()
    {
        $employers=Employer::all();
        $page=Page::where('slug','about/employers')->firstOrFail();
        $tpl=$page->template;
        //$reviews=$page->reviews()->get();
       // $blocks=$page->blocks()->withPivot('title', 'text','image','file','svg','order')->orderBy('pivot_order','asc')->get();
        return view('pages.'.$tpl,compact('page','employers'));
    }

    public function employer(string $id)
    {
        $employer=Employer::findOrFail($id);
        $sell_objects=$employer->objects()->where('selled',1)->get();
        $objects=$employer->objects()->where('selled',0)->get();
        $reviews=$employer->reviews()->where('published',1)->get();
        foreach ($objects as $object)
        {
            $images=[$object->main_image];
            $od_images=json_decode($object->images,1);
            if ($od_images){
                $images += $od_images;
            }
            $object->images=json_encode($images);
        }
        foreach ($sell_objects as $object)
        {
            $images=[$object->main_image];
            $od_images = json_decode($object->images, 1);
            if ($od_images){
                $images += $od_images;
            }
            $object->images=json_encode($images);
        }
        return view('employer',compact('employer','objects','sell_objects','reviews'));
    }
}
