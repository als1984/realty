<?php

namespace App\Http\Controllers\Api;

use App\Traits\BitrixAuthTrait;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Bitrix24\CRM\Lead;
use Bitrix24\Bitrix24;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class FormsController extends Controller
{


    use BitrixAuthTrait;

    public function index(Request $request)
    {
        $data = $request->validate(['name' => 'required:string', 'tel' => 'required:string', 'types' => 'nullable:string']);
        if (!isset($data['types']) || !$data['types']) {
            $data['types'] = 'Не выбран';
        }
        $data['tel'] = preg_replace("/[^0-9]/", '', $data['tel']);
        $params = [];
        switch ($data['types']) {
            case 'buy':
                $data['types'] = 'Покупка';
                $params['UF_CRM_1579264780056']=1091;
                break;
            case 'sell':
                $data['types'] = 'Продажа';
                $params['UF_CRM_1579264780056']=1090;
                break;
            case 'rent':
                $data['types'] = 'Обмен';
                $params['UF_CRM_1579264780056']=1092;
                break;
            case 'change':
                $data['types'] = 'Обмен';
                $params['UF_CRM_1579264780056']=1092;
                break;
        }
        //return response()->json(['success' => $data]);
        $token = $this->getTokenDb();
        if (!$token) {

            $token = $this->refreshRequest();
        }
        if (!$token) {
            Log::error('cannot get token');
            return redirect()->back();
        }
        $log = new Logger('bitrix24');
        $log->pushHandler(new StreamHandler(storage_path('logs/laravel.log'), Logger::DEBUG));
        $obB24App = new Bitrix24(false, $log);
        $obB24App->setApplicationScope(['crm']);
        $obB24App->setApplicationId(env('BITRIX_APP_ID', 'local.5d8789d5a444b6.43518446'));
        $obB24App->setApplicationSecret(env('BITRIX_APP_SECRET', 'AA3DhoH1kLDhU9lZvu2wgc7FxaXpXscOCGOIDPp2hhOp7nZjJ1'));
        $obB24App->setDomain(env('BITRIX_DOMAIN', 'portal.ulasnydax.by'));
        $obB24App->setAccessToken($token->token);
        $obB24App->setRefreshToken($token->refresh_token);

        $params['TITLE'] = 'Заявка с сайта, тип - ' . $data['types'];
        $params['STATUS_ID'] = 'NEW';
        $params['ASSIGNED_BY_ID'] = 521;
        $params['NAME'] = $data['name'];
        $params['PHONE'][0] = ['VALUE' => $data['tel'], 'VALUE_TYPE' => 'WORK'];
        $params['SOURCE_ID'] = 9;
        $params['UF_CRM_1579264780056']=1095;
        $params['COMMENTS']='Запрос с сайта, '.$data['types'].', необходимо перезвонить';
        $lead = new Lead($obB24App);
        $lead->add($params);
        return response()->json(['success' => true]);
    }
}
