<?php

namespace App\Http\Controllers\Api;

use App\Facility;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Validator;

class FilterController extends Controller
{

    /**
     * @SWG\Get(
     *     path="/api/objects/{type}",
     *     summary="Получаем полный список объектов и начальные данные фильтров",
     *     tags={"Начальные данные"},
     *      @SWG\Parameter(
     *         name="type",
     *         in="path",
     *         description="тип предложения buy,sell,rent (только эти)",
     *         required=false,
     *         type="string",
     *         default="",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *         @SWG\Schema(
     *             type="array",
     *     @SWG\Items(
     *             @SWG\Property(
     *               property="objects",
     *              description="Объекты",
     *              type="array",
     *              @SWG\Items(
     *                  ref="#/definitions/Object"
     *              ),
     *              ),
     *              @SWG\Property(
     *              property="filters",
     *              description="Фильтры",
     *              type="array",
     *              @SWG\Items(
     *                  ref="#/definitions/Filters"
     *              ),
     *              ),
     *     ),
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="неверный тип предложения",
     *     ),
     * )
     */
    /**
     * @param string $type
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(string $type = '')
    {
        if (!$type) {
            $objects = Facility::where('published', true)->get();
        } else {
            if (!in_array($type, ['buy', 'sell', 'rent','change'])) {
                return response()->json(['error' => 'invalid type'], 400);
            }
            $objects = Facility::where([['published', true], ['type', 'like', '%' . $type . '%']])->get();
        }
        $filters = [];
        $types = '';
        $property_types = [];
        $house_types = [];
        $obj_neighbors=DB::table('objects')->selectRaw("coordinates, GROUP_CONCAT(`id` SEPARATOR ',') as neighbors")->where('published', true)
            ->groupBy('coordinates')
            ->get();
        //$obj_neighbors=explode(',',$obj_neighbors);
        //var_dump($obj_neighbors->toArray());die();

        foreach ($objects as $object) {

            //$object->type=json_decode($object->type);
            $types .= ','.implode(',',json_decode($object->type));
            $property_types[] = $object->property_type;
            $house_types[] = $object->house_type;
            $object->neighbors=$object->id.',';
            foreach ($obj_neighbors as $neighbor){
                if($neighbor->coordinates==$object->coordinates){
                    $object->neighbors.=$neighbor->neighbors.',';
                }
            }
            $neighbors_arr=array_unique(explode(',',rtrim($object->neighbors,',')));
            $n_arr=[];
            foreach ($neighbors_arr as $neighbor){
                $n_arr[]=(int)$neighbor;
            }
            $object->neighbors=$n_arr;
            $object->coordinates = $object->getCoordinates();
            $object->small_title=$object->small_title ? $object->small_title : $object->rooms.'-комнатная квартира';
            $images=[$object->main_image];
            $obj_img=json_decode($object->images,1);
            if(empty($images)) {
                $images = $images + $obj_img[0];
                //$images = $images;
            }
            $object->images=json_encode($images);
            //$object->neighbors=array_unique(explode(',',rtrim($object->neighbors,',')));
        }
        $types=trim($types,',');
        $types=explode(',',$types);
        $filters['min_price'] = $objects->min('price');
        $filters['min_price_usd'] = $objects->min('price_usd');
        $filters['max_price'] = $objects->max('price');
        $filters['max_price_usd'] = $objects->max('price_usd');
        $filters['min_price_m2'] = $objects->min('price_m2');
        $filters['min_price_m2_usd'] = $objects->min('price_m2_usd');
        $filters['max_price_m2'] = $objects->max('price_m2');
        $filters['max_price_m2_usd'] = $objects->max('price_m2_usd');
        $filters['min_square'] = $objects->min('square');
        $filters['min_living_square'] = $objects->min('living_square');
        $filters['min_kitchen_square'] = $objects->min('kitchen_square');
        $filters['max_square'] = $objects->max('square');
        $filters['max_living_square'] = $objects->max('living_square');
        $filters['max_kitchen_square'] = $objects->max('kitchen_square');
        $filters['property_types'] = array_unique($property_types);
        $filters['types'] = array_values(array_unique($types));
        $filters['house_types'] = array_unique($house_types);
        $filters['min_year'] = $objects->min('year');
        $filters['max_year'] = $objects->max('year');
        $filters['min_floor'] = $objects->min('floor');
        $filters['max_floor'] = $objects->max('floor');
        return response()->json(['success' => ['fliters' => $filters, 'objects' => $objects]]);
    }

    /**
     * @SWG\Get(
     *     path="/api/objects/filter",
     *     summary="Получаем  список объектов после фильтрации",
     *     tags={"Фильтрация"},
     *   @SWG\Parameter(
     *      in="query",
     *     required=false,
     *      name="min_price",
     *      type="number",
     *     description="Минимальная цена BYN"
     *  ),
     *  @SWG\Parameter(
     *     in="query",
     *     required=false,
     *      name="min_price_usd",
     *      type="number",
     *     description="Минимальная цена USD"
     *  ),
     * @SWG\Parameter(
     *     in="query",
     *     required=false,
     *      name="max_price",
     *      type="number",
     *     description="Максимальная цена"
     *  ),
     *     @SWG\Parameter(
     *     in="query",
     *     required=false,
     *      name="max_price_usd",
     *      type="number",
     *     description="Максимальная цена USD"
     *  ),
     *     @SWG\Parameter(
     *     in="query",
     *     required=false,
     *      name="min_price_m2",
     *      type="number",
     *     description="Минимальная цена m2 BYN"
     *  ),
     *     @SWG\Parameter(
     *     in="query",
     *     required=false,
     *      name="min_price_m2_usd",
     *      type="number",
     *     description="Минимальная цена m2 USD"
     *  ),
     *     @SWG\Parameter(
     *     in="query",
     *     required=false,
     *      name="max_price_m2",
     *      type="number",
     *     description="Максимальная цена m2"
     *  ),
     *     @SWG\Parameter(
     *     in="query",
     *     required=false,
     *      name="max_price_m2_usd",
     *      type="number",
     *     description="Максимальная цена m2 USD"
     *  ),
     *     @SWG\Parameter(
     *     in="query",
     *     required=false,
     *      name="min_square",
     *      type="number",
     *     description="Минимальная площадь"
     *  ),
     *     @SWG\Parameter(
     *     in="query",
     *     required=false,
     *      name="min_living_square",
     *      type="number",
     *     description="Минимальная жилая площадь"
     *  ),
     *       @SWG\Parameter(
     *     in="query",
     *     required=false,
     *      name="min_kitchen_square",
     *      type="number",
     *     description="Минимальная площадь кухни"
     *  ),
     *         @SWG\Parameter(
     *     in="query",
     *     required=false,
     *      name="max_square",
     *      type="number",
     *     description="Максимальная площадь"
     *  ),
     *     @SWG\Parameter(
     *     in="query",
     *     required=false,
     *      name="max_living_square",
     *      type="number",
     *     description="Максимальная жилая площадь"
     *  ),
     *       @SWG\Parameter(
     *     in="query",
     *     required=false,
     *      name="max_kitchen_square",
     *      type="number",
     *     description="Максимальная площадь кухни"
     *  ),
     *        @SWG\Parameter(
     *     in="query",
     *     required=false,
     *      name="min_year",
     *      type="number",
     *     description="Минимальная год постройки"
     *  ),
     *        @SWG\Parameter(
     *     in="query",
     *     required=false,
     *      name="max_year",
     *      type="number",
     *     description="Максимальный год постройки"
     *  ),
     *     @SWG\Parameter(
     *     in="query",
     *     required=false,
     *      name="types",
     *      type="string",
     *      description="Тип (продажа, аренда ...)",
     *
     *  ),
     *     @SWG\Parameter(
     *     in="query",
     *     required=false,
     *      name="property_types",
     *      type="string",
     *      description="Тип собственности",
     *
     *  ),
     *     @SWG\Parameter(
     *     in="query",
     *     required=false,
     *      name="rooms",
     *      type="integer",
     *      description="Кол-во комнат",
     *      default=0
     *  ),
     *       @SWG\Parameter(
     *     in="query",
     *     required=false,
     *      name="house_types",
     *      type="string",
     *      description="Тип помещения",
     *
     *  ),
     *       @SWG\Parameter(
     *     in="query",
     *     required=false,
     *      name="min_floor",
     *      type="integer",
     *      description="Минимальный этаж",
     *      default=0
     *  ),
     *      @SWG\Parameter(
     *     in="query",
     *     required=false,
     *      name="max_floor",
     *      type="integer",
     *      description="Максимальный этаж",
     *      default=0
     *  ),
     *       @SWG\Parameter(
     *     in="query",
     *     required=false,
     *      name="repair",
     *      type="boolean",
     *      description="Наличие ремонта",
     *      default=false
     *  ),
     *      @SWG\Parameter(
     *     in="query",
     *     required=false,
     *      name="furniture",
     *      type="boolean",
     *      description="Наличие мебели",
     *      default=false
     *  ),
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *         @SWG\Schema(
     *             type="array",
     *     @SWG\Items(
     *             @SWG\Property(
     *               property="objects",
     *              description="Объекты",
     *              type="array",
     *              @SWG\Items(
     *                  ref="#/definitions/Object"
     *              ),
     *              ),
     *     ),
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="Ошибка валидации",
     *     ),
     * )
     */
    public function filter(Request $request)
    {
        $validator=Validator::make($request->all(),[
            'min_price'=>'nullable|numeric',
            'max_price'=>'nullable|numeric',
            'min_price_usd'=>'nullable|numeric',
            'max_price_usd'=>'nullable|numeric',
            'min_price_m2'=>'nullable|numeric',
            'max_price_m2'=>'nullable|numeric',
            'min_price_m2_usd'=>'nullable|numeric',
            'max_price_m2_usd'=>'nullable|numeric',
            'max_square'=>'nullable|numeric',
            'min_square'=>'nullable|numeric',
            'max_living_square'=>'nullable|numeric',
            'min_living_square'=>'nullable|numeric',
            'max_kitchen_square'=>'nullable|numeric',
            'min_kitchen_square'=>'nullable|numeric',
            'min_year'=>'nullable|numeric',
            'max_year'=>'nullable|numeric',
            'types'=>'nullable|string',
            'property_types'=>'nullable|string',
            'house_types'=>'nullable|string',
            'room1'=>'nullable|string',
            'room2'=>'nullable|string',
            'room3'=>'nullable|string',
            'room4'=>'nullable|string',
            'min_floor'=>'nullable|integer',
            'max_floor'=>'nullable|integer',
            'repair'=>'nullable|string',
            'furniture'=>'nullable|string',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }
        //Log::info(print_r($request->all(),1));
        $data=$request->all();
        $data['rooms']=$this->roomsToArray($data);
        $data=$this->setDefault($data);
        //Log::info(print_r($data,1));
       // DB::enableQueryLog();
        $query=Facility::where('published',true)
            ->whereBetween('price',[$data['min_price'],$data['max_price']])
            ->whereBetween('price_usd',[$data['min_price_usd'],$data['max_price_usd']])
            ->whereBetween('price_m2',[$data['min_price_m2'],$data['max_price_m2']])
            ->whereBetween('price_m2_usd',[$data['min_price_m2_usd'],$data['max_price_m2_usd']])
            ->whereBetween('square',[$data['min_square'],$data['max_square']])
            ->whereBetween('living_square',[$data['min_living_square'],$data['max_living_square']])
            ->whereBetween('kitchen_square',[$data['min_kitchen_square'],$data['max_kitchen_square']])
            ->whereBetween('floor',[$data['min_floor'],$data['max_floor']])
            ->whereBetween('year',[$data['min_year'],$data['max_year']]);
        if(isset($data['types']) && $data['types']){
            $query->where('type','like','%'.$data['types'].'%');
        }
        if(isset($data['property_types']) && $data['property_types']){
            $query->where('property_type','like',$data['property_types']);
        }
        if(isset($data['house_types']) && $data['house_types']){
            $query->where('house_type','like',$data['house_types']);
        }
        if(isset($data['type']) && $data['type']){
            $query->where('type','=',$data['type']);
        }
        if(isset($data['repair']) && $data['repair']=="true"){
            $query->where('repair',true);
        }
        if(isset($data['furniture']) && $data['furniture']=="true"){
            $query->where('furniture',true);
        }
        if(isset($data['rooms']) && $data['rooms']){
            if(in_array(4,$data['rooms'])){
                $query->where(function ($n_query) use ($data){
                    $n_query->whereIn('rooms',$data['rooms'])->orWhere('rooms','>',4);
                });
            }else{
                $query->whereIn('rooms',$data['rooms']);
            }
        }
        $objects=$query->get();
        $obj_neighbors=DB::table('objects')->selectRaw("coordinates, GROUP_CONCAT(`id` SEPARATOR ',') as neighbors")->where('published', true)
            ->groupBy('coordinates')
            ->get();
        foreach ($objects as $object) {

            $object->neighbors=$object->id.',';
            foreach ($obj_neighbors as $neighbor){
                if($neighbor->coordinates==$object->coordinates){
                    $object->neighbors.=$neighbor->neighbors.',';
                }
            }
            $neighbors_arr=array_unique(explode(',',rtrim($object->neighbors,',')));
            $n_arr=[];
            foreach ($neighbors_arr as $neighbor){
                $n_arr[]=(int)$neighbor;
            }
            $object->neighbors=$n_arr;
            $object->coordinates = $object->getCoordinates();
            $object->small_title=$object->small_title ? $object->small_title : $object->rooms.'-комнатная квартира';
            $images=[$object->main_image];
            $obj_img=json_decode($object->images,1);
//            if($obj_img) {
//                $images = $images + $obj_img;
//            }
            if(empty($images)) {
                $images = $images + $obj_img[0];
                //$images = $images;
            }
            $object->images=json_encode($images);
            //$object->neighbors=array_unique(explode(',',rtrim($object->neighbors,',')));
        }
        return response()->json(['success' => ['objects' => $objects]]);
    }

    public function setDefault(array $data)
    {

        foreach ($data as $key=>$val){
            if(($val==null) || ($val===0)){
                unset($data[$key]);
            }
        }
        //Log::info(print_r($data,1));

        $filters['min_price'] = 0;
        $filters['min_price_usd'] = 0;
        $filters['max_price'] = PHP_INT_MAX;
        $filters['max_price_usd'] = PHP_INT_MAX;
        $filters['min_price_m2'] = 0;
        $filters['min_price_m2_usd'] = 0;
        $filters['max_price_m2'] = PHP_INT_MAX;
        $filters['max_price_m2_usd'] = PHP_INT_MAX;
        $filters['min_square'] = 0;
        $filters['min_living_square'] = 0;
        $filters['min_kitchen_square'] = 0;
        $filters['max_square'] = PHP_INT_MAX;
        $filters['max_living_square'] = PHP_INT_MAX;
        $filters['max_kitchen_square'] = PHP_INT_MAX;
        $filters['min_year'] = 0;
        $filters['max_year'] = 3000;
        $filters['min_floor'] = 0;
        $filters['max_floor'] = 100;

        return array_merge($filters,$data);
    }

    public function roomsToArray(array $data)
    {
        $result=[];
        for ($i=1;$i<=4;$i++){
            if(isset($data['room'.$i]) && $data['room'.$i]=='true'){
                $result[]=$i;
            }
        }
        //ничего не выбрали или все выбрали вернем null
        if(empty($result) || $result==[1,2,3,4]){
            return null;
        }
        return $result;
    }
}
