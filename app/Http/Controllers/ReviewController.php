<?php

namespace App\Http\Controllers;

use App\Employer;
use App\Review;
use App\User;
use Illuminate\Http\Request;

class ReviewController extends Controller
{
    public function index()
    {
        $reviews=Review::where('item_type','<>','App\Employer')->get();
        $employers=Employer::all();
        return view('reviews',compact('reviews','employers'));
    }

    public function view()
    {
        //TODO на всякий случай если будет страница отзыва
    }

    public function add(Request $request)
    {
        $data=$request->validate([
            'name'=>'required|string',
            'employer'=>'nullable|integer',
            'text'=>'required|string',
            'avatar'=>'nullable|image'
        ]);
        if(isset($data['employer']) && $data['employer']){
            $employer=Employer::find($data['employer']);
            if(!$employer){
                return response()->json(['error'=>'Не верный сотрудник', 'heading'=>'Ошибка'],422);
            }
        }else{
            $employer=false;
        }
        $site_domain=str_replace(['http://','https://'],'',env('APP_URL','http://ulasnydax.by'));
        //$u_id=$request->session()->get('u_id',0);
        $user=User::firstOrNew(['email'=>'visitor@ulasnydax.by']);

        if($request->file('avatar')){
            $extension=$request->file('avatar')->extension();
            $path=$request->file('avatar')->storeAs('public/users',$user->id.'_photo.'.$extension);
            $data['avatar']=str_replace('public/','',$path);
        }else{
            $data['avatar']=null;
        }

        $review=new Review(['user_id'=>$user->id,'text'=>$data['text'],'name'=>$data['name'],'avatar'=>$data['avatar']]);
        if($employer){
            $review->item_type=Employer::class;
            $review->item_id=$employer->id;
        }else{
            $review->item_type=Page::class;
            $review->item_id=1;
        }
        $review->save();
        return response()->json(['text'=>'Ваш отзыв отправлен, он будет добавлен после модерации', 'heading'=>'Отзыв отправлен']);

    }

}
