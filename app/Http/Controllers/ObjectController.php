<?php

namespace App\Http\Controllers;

use App\Employer;
use App\Facility;
use Illuminate\Http\Request;

class ObjectController extends Controller
{
    public function index(string $slug)
    {
        $object=Facility::where('slug','=',$slug)->firstOrFail();
        $blocks=$object->blocks()->where('block_id','5')->limit(3)->get();
        $all_blocks=$object->blocks()->get();
        $employer=$object->employers()->first();
        if(!$employer){
            $employer=Employer::inRandomOrder()->first();
        }
        $phone=['Не указан'];
        if($employer->phones) {
            $phone = explode(',', $employer->phones);
        }
        $employer->phone=$phone[0];
        $objects=Facility::where('id','<>',$object->id)->where('rooms',$object->rooms)->inRandomOrder()->limit(4)->get();
        foreach ($objects as $one_object)
        {
            $images=[$one_object->main_image];
            $od_images = json_decode($one_object->images, 1);
            if ($od_images){
                $images += $od_images;
            }
            $one_object->images=json_encode($images);
        }
        return view('object',compact('object','objects','employer','blocks','all_blocks'));
    }

    public function objects(Request $request)
    {
        $path=$request->path();
        $page_path=$path;
        $path='api/'.$path;
        return view('objects',compact('path','page_path'));
    }
}
