<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployerObject extends Model
{
    public $timestamps=false;

    protected $table='employer_objects';

    public $fillable=['chat_script'];

    public function object()
    {
        return $this->belongsTo(Object::class,'object_id');
    }

    public function employer()
    {
        return $this->belongsTo(Employer::class,'employer_id');
    }
}
