<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SavedForm extends Model
{
    public $fillable=["form_name","page_name","name","phone","email","message","created_at"];
}
