<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlockObject extends Model
{
    protected $table='blocks_objects';
    public $timestamps=false;

    public $fillable=['image','title','text','file'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function block()
    {
        return $this->belongsTo(Block::class,'block_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function object()
    {
        return $this->belongsTo(Facility::class,'object_id');
    }
}
