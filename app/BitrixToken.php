<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BitrixToken extends Model
{
    protected $fillable=['expired','token','refresh_token'];
}
