<!DOCTYPE html>
<html lang="ru">

<head>
    <meta charset="utf-8" />
    @yield('title')
    <link rel="shortcut icon" href="{{asset('favicon.ico')}}">
    <link rel="prerender" href="{{route('objects')}}">
    <link rel="preload" href="{{asset('/js/app.js')}}" as="script"/>
    <link rel="preload" href="{{asset('/js/chunk-vendors.js')}}" as="script"/>
    <link rel="preload" href="https://api-maps.yandex.ru/2.1/?apikey=0c615af4-d453-4a88-986b-7bf18ed04914&lang=ru_RU" as="script"/>
    {{--<link rel="preconnect" href="//api-maps.yandex.ru">--}}
    {{--<link rel="dns-prefetch" href="//api-maps.yandex.ru">--}}
    <link rel="preconnect" href="//portal.ulasnydax.by">
    <link rel="dns-prefetch" href="//portal.ulasnydax.by">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.5.0/css/swiper.min.css"
          integrity="sha256-XwfUNXGiAjWyUGBhyXKdkRedMrizx1Ejqo/NReYNdUE=" crossorigin="anonymous" />

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="format-detection" content="telephone=no" />
    @yield('pre_css')
    <link href="{{asset('css/style.min.css')}}" rel="stylesheet" media="screen" />
    <link href="{{asset('css/jquery.toast.min.css')}}" rel="stylesheet" media="screen" />

    <link rel="preload" href="{{asset('fonts/HelveticaNeueCyr-Light.woff2')}}" as="font" type="font/woff2" crossorigin />
    <link rel="preload" href="{{asset('fonts/HelveticaNeueCyr-Roman.woff2')}}" as="font" type="font/woff2" crossorigin />
    <link rel="preload" href="{{asset('fonts/HelveticaNeueCyr-Bold.woff2')}}" as="font" type="font/woff2" crossorigin />
    @yield('css')
    @yield('og')
</head>

<body>

<header class="header d-flex align-items-center" style="z-index: 999;">
    <!-- <svg class="ornament-top-line">
      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite-svg.svg#ornament1"></use>
    </svg> -->
    <div class="container h-100">
        <div class="row align-items-center h-100">
            <!-- Логотип -->
            <div class="col-3 col-md-2 col-xl-2">

                <a href="/" class="logo">
                    <img src="{{asset('img/logo.svg')}}" alt="Логотип" />
                </a>
            </div>

            <div class="col-9 col-md-10 col-xl-10 d-flex flex-column h-100">
                <!-- Контакты -->
                <div class="row d-none d-md-flex align-items-center mt-auto">
                    <div class="col-md-4">
                        <ul class="phone-list">
                            <li class="phone-list_item">
                                <a href="tel:{{str_replace(' ','', setting('site.phone'))}}" class="phone-list_link">
                                    <svg width="17" height="17">
                                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/img/sprite-svg.svg#phone"></use>
                                    </svg>{{setting('site.phone')}}
                                </a>
                            </li>
                            <li class="phone-list_item">
                                <a href="tel:{{str_replace(' ','', setting('site.phone2'))}}" class="phone-list_link">
                                    <svg width="17" height="17">
                                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/img/sprite-svg.svg#phone"></use>
                                    </svg>{{setting('site.phone2')}}
                                </a>
                            </li>

                        </ul>
                    </div>

                    <div class="col-md-4">
                        <button
                            class="button button--outline button--accent"
                            data-toggle="modal"
                            data-target="#requestCall"
                        >
                            Заказать звонок
                        </button>
                    </div>

                    <div class="col-md-4 d-flex justify-content-md-end">
                        <span class="header_work-time">{{setting('site.worktime')}}</span>
                    </div>
                </div>

                <!-- Меню -->
                <div class="row d-md-flex align-items-start mt-auto">
                    <div class="col-xs-0 col-sm-0 col-md-10">
                        <nav id="nav" class="nav">
                            <ul class="nav_list">
                                {{--<li class="nav_item">--}}
                                    {{--<a href="{{route('objects')}}" class="nav_link">Продавцу</a>--}}
                                {{--</li>--}}
                                {{--<li class="nav_item">--}}
                                    {{--<!-- class active -->--}}
                                    {{--<a href="{{route('objects')}}" class="nav_link">Покупателю</a>--}}
                                {{--</li>--}}
                                {{--<li class="nav_item">--}}
                                    {{--<a href="{{route('objects')}}" class="nav_link">Обмен</a>--}}
                                {{--</li>--}}
                                <li class="nav_item">
                                    <a href="{{route('objects')}}" class="nav_link">Объекты</a>
                                </li>
                                <li class="nav_item">
                                    <a href="{{route('pages',['slug'=>'work-with-us'])}}" class="nav_link">Работа у нас</a>
                                </li>
                                <li class="nav_item">
                                    <a href="{{route('pages',['slug'=>'about-company'])}}" class="nav_link">О компании</a>
                                </li>
                                <li class="nav_item">
                                    <a href="{{route('reviews')}}" class="nav_link">Отзывы</a>
                                </li>
                                <li class="nav_item">
                                    <a href="{{route('employers')}}" class="nav_link">Сотрудники</a>
                                </li>
                                <li class="nav_item nav_item-with-sublist d-none">
                                    <a href="javascript:void(0);" class="nav_link nav_link-with-icon">
                                        <svg class="d-none d-md-block" width="8" height="11">
                                            <use
                                                xmlns:xlink="http://www.w3.org/1999/xlink"
                                                xlink:href="/img/sprite-svg.svg#fill-arrow"
                                            ></use>
                                        </svg>
                                        <svg class="d-block d-md-none" width="16" height="16">
                                            <use
                                                xmlns:xlink="http://www.w3.org/1999/xlink"
                                                xlink:href="/img/sprite-svg.svg#small-arrow-menu"
                                            ></use>
                                        </svg>
                                        О нас
                                    </a>

                                    <ul class="nav_sublist d-none flex-column">
                                        <li class="nav_subitem">
                                            <a href="{{route('pages',['slug'=>'about-company'])}}" class="nav_sublink">О компании</a>
                                        </li>
                                        <li class="nav_subitem">
                                            <a href="{{route('reviews')}}" class="nav_sublink">Отзывы</a>
                                        </li>
                                        <li class="nav_subitem">
                                            <a href="{{route('employers')}}" class="nav_sublink">Сотрудники</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="d-block d-md-none nav_item">
                                    <a  class="nav_link disabled">Войти</a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                    <div class="d-none d-md-flex col-md-2 justify-content-md-end">
                        <a  class="header_log-in disabled">
                            <svg width="25" height="25">
                                <use
                                    xmlns:xlink="http://www.w3.org/1999/xlink"
                                    xlink:href="/img/sprite-svg.svg#enter-left"
                                ></use>
                            </svg>
                            <span>Войти</span>
                        </a>
                    </div>
                </div>

                <!-- Бургер -->
                <div class="row d-flex d-md-none h-100 header_mob-nav mob-nav">
                    <div class="col-12 d-flex justify-content-around align-items-center">
                        <svg class="mob-nav_ornament">
                            <use
                                xmlns:xlink="http://www.w3.org/1999/xlink"
                                xlink:href="/img/sprite-svg.svg#ornament8"
                            ></use>
                        </svg>

                        <button id="burger" class="burger" aria-label="Открыть меню">
                            <span class="burger_line burger_line-top"></span>
                            <span class="burger_line burger_line-middle"></span>
                            <span class="burger_line burger_line-bottom"></span>
                        </button>

                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
@yield('content')
<div class="request-call-modal modal fade" id="requestCall" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <button class="close" aria-label="Закрыть" data-dismiss="modal"></button>
            <svg class="request-call-modal_ornament" data-aos="fade" data-aos-delay="200">
                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/img/sprite-svg.svg#ornament3"></use>
            </svg>
            <svg class="request-call-modal_ornament" data-aos="fade" data-aos-delay="200">
                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/img/sprite-svg.svg#ornament3"></use>
            </svg>

            <form class="request-call-modal_form ajax-form" method="post" action="{{route('send-form')}}">
                {{ csrf_field() }}
                @isset($object)
                    <input type="hidden" name="object" value="{{$object->name}}">
                    <input type="hidden" name="object" value="{{$object->name}}">
                @endisset
                <input type="hidden" name="place" value="header">
                <input type="hidden" name="lead_id" value="4">
                <label class="visually-hidden" for="request-call-modal-field-name"></label>
                <input class="field failed--horizontal" type="text" name="name" required placeholder="Ваше Имя"
                       id="request-call-modal-field-name">

                <label class="visually-hidden" for="request-call-modal-phone"></label>
                <input class="field failed--horizontal" type="tel" name="phone" required placeholder="Телефон"
                       id="request-call-modal-name">

                {{--<label class="visually-hidden" for="request-call-modal-phone"></label>--}}
                {{--<input class="field failed--horizontal" type="text" name="price"  placeholder="Укажите цену"--}}
                       {{--id="request-call-modal-name">--}}

                <button type="submit" class="button button--form">Оставить
                    заявку</button>
            </form>
        </div>
    </div>
</div>
<div class="request-call-modal modal fade" id="requestCallFooter" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <button class="close" aria-label="Закрыть" data-dismiss="modal"></button>
            <svg class="request-call-modal_ornament" data-aos="fade" data-aos-delay="200">
                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/img/sprite-svg.svg#ornament3"></use>
            </svg>
            <svg class="request-call-modal_ornament" data-aos="fade" data-aos-delay="200">
                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/img/sprite-svg.svg#ornament3"></use>
            </svg>

            <form class="request-call-modal_form ajax-form" method="post" action="{{route('send-form')}}">
                {{ csrf_field() }}
                @isset($object)
                    <input type="hidden" name="object" value="{{$object->name}}">
                @endisset
                <input type="hidden" name="place" value="footer">
                <input type="hidden" name="lead_id" value="5">
                <label class="visually-hidden" for="request-call-modal-field-name"></label>
                <input class="field failed--horizontal" type="text" name="name" required placeholder="Ваше Имя"
                       id="request-call-modal-field-name">

                <label class="visually-hidden" for="request-call-modal-phone"></label>
                <input class="field failed--horizontal" type="tel" name="phone" required placeholder="Телефон"
                       id="request-call-modal-name">

                {{--<label class="visually-hidden" for="request-call-modal-phone"></label>--}}
                {{--<input class="field failed--horizontal" type="text" name="price"  placeholder="Укажите цену"--}}
                {{--id="request-call-modal-name">--}}

                <button type="submit" class="button button--form">Оставить
                    заявку</button>
            </form>
        </div>
    </div>
</div>
<footer role="contentinfo" class="footer" data-aos="fade" data-aos-delay="150">
    <svg class="footer_ornament">
        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/img/sprite-svg.svg#ornament3"></use>
    </svg>
    <svg class="footer_ornament">
        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/img/sprite-svg.svg#ornament3"></use>
    </svg>

    <div class="footer_container container">
        <div class="footer_top-row row">
            <div class="col-md-3 col-xl-2 offset-xl-1 d-flex d-md-block flex-column flex-md-row align-items-center">

                <a href="/" class="logo">
                    <img src="{{asset('img/logo_header-white.svg')}}" alt="Логотип" />
                </a>
                <div class="footer_location d-none d-md-block d-xl-none">
                    <svg width="17" height="17">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/img/sprite-svg.svg#pin"></use>
                    </svg>{{setting('site.address')}}
                </div>
            </div>

            <div class="col-md-6 d-flex justify-content-center justify-content-xl-between">
                <div class="footer_contacs">
                    <button class="footer_button-get-call button button--outline" data-toggle="modal"
                            data-target="#requestCallFooter">Заказать звонок</button>
                    <ul class="footer_phone-list phone-list">
                        <li class="phone-list_item">
                            <a href="tel:{{str_replace(' ','', setting('site.phone'))}}" class="phone-list_link">
                                <svg width="17" height="17">
                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/img/sprite-svg.svg#phone"></use>
                                </svg>{{setting('site.phone')}}</a>
                        </li>
                        <li class="phone-list_item">
                            <a href="tel:{{str_replace(' ','', setting('site.phone2'))}}" class="phone-list_link">
                                <svg width="17" height="17">
                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/img/sprite-svg.svg#phone"></use>
                                </svg>{{setting('site.phone2')}}</a>
                        </li>
                        <li class="phone-list_item">
                            <a href="mailto:{{str_replace(' ','', setting('site.email'))}}" class="phone-list_link">
                                <svg width="17" height="17">
                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/img/sprite-svg.svg#phone"></use>
                                </svg>{{setting('site.email')}}</a>
                        </li>
                        <li class="phone-list_item d-block d-lg-none d-xl-block">
                            <div class="phone-list_link">
                                <svg width="17" height="17">
                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/img/sprite-svg.svg#pin"></use>
                                </svg>{{setting('site.address')}}
                            </div>
                        </li>
                    </ul>
                </div>

                <ul class="footer_nav-list">
                    <li class="footer_nav-item">
                        <a href="{{route('reviews')}}" class="footer_nav-link">Отзывы</a>
                    </li>
                    <li class="footer_nav-item">
                        <a href="{{route('employers')}}" class="footer_nav-link">Сотрудники</a>
                    </li>
                    <li class="footer_nav-item">
                        <a href="{{route('pages',['slug'=>'about-company'])}}" class="footer_nav-link">О нас</a>
                    </li>
                    {{--<li class="footer_nav-item">--}}
                        {{--<a href="{{route('objects-bue')}}" class="footer_nav-link">Продавцу</a>--}}
                    {{--</li>--}}
                    {{--<li class="footer_nav-item">--}}
                        {{--<a href="{{route('objects-sel')}}" class="footer_nav-link">Покупателю</a>--}}
                    {{--</li>--}}
                    {{--<li class="footer_nav-item">--}}
                        {{--<a href="{{route('objects-change')}}" class="footer_nav-link">Обмен</a>--}}
                    {{--</li>--}}
                </ul>

                <ul class="footer_nav-list">
                    <li class="footer_nav-item">
                        <a href="{{route('objects')}}" class="footer_nav-link">Объекты</a>
                    </li>
                    <li class="footer_nav-item">
                        <a href="{{route('pages',['slug'=>'work-with-us'])}}" class="footer_nav-link">Работа у нас</a>
                    </li>

                </ul>
            </div>

            <div class="col-md-3 d-flex justify-content-center">
                <div class="footer_social">
                    <h3 class="footer_social-title">Мы в социальных сетях</h3>
                    <div class="footer_social-container">
                        <a href="{{setting('site.fb')}}" target="_blank" class="footer_social-link">
                            <svg width="28" height="28">
                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/img/sprite-svg.svg#fb"></use>
                            </svg>
                        </a>
                        <a href="{{setting('site.tw')}}" target="_blank" class="footer_social-link">
                            <svg width="35" height="35">
                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/img/sprite-svg.svg#tw"></use>
                            </svg>
                        </a>
                        <a href="{{setting('site.vk')}}" target="_blank" class="footer_social-link">
                            <svg width="37" height="37">
                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/img/sprite-svg.svg#vk"></use>
                            </svg>
                        </a>
                    </div>
                </div>
            </div>

            <div class="col-12 d-flex d-md-none justify-content-center">
                <a href="#" target="_blank" class="developer">
                    <img src="{{asset('img/dev-logo.svg')}}" alt="Логотип разработчика">
                    Разработка сайта
                </a>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row align-items-center">
            <div class="col-12 col-md-8 col-xl-4 offset-xl-1 mb-3 mb-md-3 mb-xl-0">
          <span class="company-license">
            {{setting('site.license')}}
          </span>
            </div>

            <div class="col-md-4 col-xl-3 d-none d-md-block">
                <a href="#" target="_blank" class="developer">
                    <img src="{{asset('img/dev-logo.svg')}}" alt="Логотип разработчика">
                    Разработка сайта
                </a>
            </div>

            <div class="w-0 w-md-100"></div>

            <div class="col-3 col-md-2 col-xl-1">
                <!—- re.kufar.by виджет -->
                <a href="https://re.kufar.by" target="_blank"><img src="https://content.kufar.by/img/kufar_re_2x.png" width="91" height="29" border="0" alt="Куфар Недвижимость"></a>
                <!—- re.kufar.by виджет -->

                {{--<a href="https://kufar.by" class="partner-link"><img src="{{asset('img/kufar.jpg')}}" height="62" width="130"  alt="kufar"></a>--}}
            </div>
            <div class="col-3 col-md-2 col-xl-1">
                <a href="https://onliner.by" class="partner-link"><img src="{{asset('img/onliner.jpg')}}" height="62" width="130"  alt="onliner"></a>
            </div>
            <div class="col-3 col-md-2 col-xl-1">
                <a href="https://realt.by" class="partner-link"><img src="{{asset('img/realt_by.jpg')}}" height="62" width="130" alt="realt"></a>
            </div>
            <div class="col-3 col-md-2 col-xl-1">
                <a href="https://domovita.by" class="partner-link"><img src="{{asset('img/logo_domovita.svg')}}" height="62" width="130"  alt="domovita"></a>
            </div>
        </div>
    </div>
</footer>

<script src="https://code.jquery.com/jquery-3.4.1.min.js"
        integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.5.0/js/swiper.min.js"
        integrity="sha256-uckMYBvIGtce2L5Vf/mwld5arpR5JuhAEeJyjPZSUKY=" crossorigin="anonymous"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>

<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>

<script src="{{asset('js/script.min.js')}}?1"></script>

<script src="{{asset('js/jquery.toast.min.js')}}?1"></script>

<script>
    $('.ajax-form').submit(function (e) {
        e.preventDefault();
        let form_data=$(this).serialize();
        let url=$(this).attr('action');
        $.ajax({
            url: url,
            method: 'post',
            data: form_data,
            success: function (date) {
                console.log(date);
                $.toast({
                    text : date.text,
                    heading: date.heading,
                    position: 'top-right',
                    bgColor: '#2ecc71',
                    textColor: '#fff',
                });
            },
            error: function (date) {
                $.toast({
                    text : date.error,
                    heading: date.heading ? date.heading  : 'Ошибка',
                    position: 'top-right',
                    bgColor: '#ff2a45',
                    textColor: '#fff',
                });
            }
        });
        $('#requestCall').modal('hide');
    });
</script>
<script>
    $(document).ready(function () {
        let url=window.location.href;
        $('a.nav_link').each(function (index,item) {
            console.log(item);
            if($(item).attr('href')==url){
                $(item).addClass('active');
            }
        });
    });

</script>

@yield('script')
<script>
    (function(w,d,u){
        var s=d.createElement('script');s.async=true;s.src=u+'?'+(Date.now()/60000|0);
        var h=d.getElementsByTagName('script')[0];h.parentNode.insertBefore(s,h);
    })(window,document,'https://portal.ulasnydax.by/upload/crm/site_button/loader_2_b6f6lr.js');
</script>
<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
    (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
        m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
    (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

    ym(56522929, "init", {
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        webvisor:true
    });
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/56522929" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- Yandex.Metrika counter -->
<!-- Facebook Pixel Code -->
<script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};
        if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
        n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t,s)}(window, document,'script',
        'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '514925075759492');
    fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
               src="https://www.facebook.com/tr?id=514925075759492&ev=PageView&noscript=1"
    /></noscript>
<!-- End Facebook Pixel Code -->
</body>

</html>
