@extends('layouts.main')

@section('title')
    <title>{{$page->title}}</title>
    <meta name="description" content="{{$page->description}}">
@endsection

@section('css')
    <style>
        .obg-card{
            height: 100%;
        }
    </style>
@endsection

@section('og')
    <meta property="og:title" content="{{$page->title}}">
    <meta property="og:image" content="{{asset('/storage/'.$page->image)}}">
    <meta property="og:type" content="article">
    <meta property="og:url" content="{{route('pages',['slug'=>$page->slug])}}">
    <meta property="og:description" content="{{$page->description}}">
@endsection

@section('content')
    <main class="main">
        <!-- Фоновая картинка меняется js-ом -->
        <img id="mainBg" class="main-slider_bg" src="/storage/{{$page->image}}" alt="Фон">

        <div class="main_container container">
            <div class="row">
                <div class="col-xl-8 offset-xl-2">
                    <div class="main-slider swiper-container">
                        <div class="swiper-wrapper">
                            @foreach($blocks->where('name','Слайдер')->all() as $part)

                                <div class="main-slider_slide swiper-slide">
                                    <h1 class="main-slider_title" data-aos="fade-up" data-aos-delay="100">{{$part->pivot->title}}</h1>
                                </div>
                            @endforeach


                        </div>

                        <div class="main-slider_footer">
                            <form class="main-slider_form ajax-form" method="post" action="{{route('send-form')}}">
                                {{ csrf_field() }}
                                <input type="hidden" name="type" value="Продать">
                                <input type="hidden" name="lead_id" value="1">
                                <label class="visually-hidden" for="main-slider-field-name"></label>
                                <input class="field failed--horizontal" type="text" name="name" required placeholder="Ваше Имя"
                                       id="main-slider-field-name">

                                <label class="visually-hidden" for="main-slider-field-phone"></label>
                                <input class="field failed--horizontal" type="tel" name="phone" required placeholder="Телефон"
                                       id="main-slider-field-name">

                                <button type="submit" class="button button--form">Оставить заявку</button>
                            </form>
                        </div>
                    </div>

                </div>
            </div>

            <button class="main-slider_button-prev">
                <svg width="37" height="37">
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite-svg.svg#arrow-left-big"></use>
                </svg>
            </button>
            <button class="main-slider_button-next">
                <svg width="37" height="37">
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite-svg.svg#arrow-right-big"></use>
                </svg>
            </button>

        </div>
        <div class="main-slider_pagination-container container">
            <div class="row">
                <div class="col-xl-8 offset-xl-2">
                    <!-- <div class="main-slider_pagination" data-aos="fade" data-aos-delay="1000"> -->
                    <div class="main-slider_pagination">
                        <!-- Кнопки для пагинации выводятся из js -->
                    </div>
                </div>
            </div>
        </div>
    </main>

    <section class="section-services">
        <h2 class="section-title" data-aos="fade" data-aos-delay="150">
      <span>
        <svg width="41" height="41">
          <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite-svg.svg#ornament4"></use>
        </svg>
        <svg width="41" height="41">
          <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite-svg.svg#ornament4"></use>
        </svg>
        Наши услуги
      </span>
        </h2>

        <div class="container">
            <div class="row">
                @foreach($blocks->where('name','Наши услуги')->all() as $part)
                    <div class="col-12 col-md-6 col-xl-5
                    @switch($loop->iteration)
                    @case(1) offset-lg-0 offset-xl-1 @break
                    @case(2) mb-70 @break
                    @case(3) offset-lg-0 offset-xl-1 @break
                    @case(4)  @break
                    @endswitch
                            ">
                        <div class="section-services_item" data-aos="fade-up" data-aos-delay="150">
                            <svg width="61" height="52" class="d-none d-md-block">
                                {!! $part->pivot->svg !!}
                            </svg>
                            <div class="section-services_item-desc">
                                <svg width="61" height="52" class="d-block d-md-none">
                                    {!! $part->pivot->svg !!}
                                </svg>
                                <h3>{{$part->pivot->title}}</h3>
                                {!! $part->pivot->text !!}
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>

    <section class="section-advantages">
        <h2 class="section-title" data-aos="fade" data-aos-delay="150">
      <span>
        Почему мы
      </span>
        </h2>

        <div class="container">
            <div class="row">
                @foreach($blocks->where('name','Почему мы')->all() as $part)
                    <div class="section-advantages-card-container col-md-6 col-xl-3">
                        <div class="section-advantages-card" data-aos="fade-up" data-aos-delay="150">
                            <div class="section-advantages-card_header">
                                {!! $part->pivot->svg !!}
                                <h3>{{$part->pivot->title}}</h3>
                            </div>
                            {!! $part->pivot->text !!}
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>

    <section class="section-reviews">
        <svg class="section-reviews_ornament" data-aos="fade" data-aos-delay="150">
            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite-svg.svg#ornament3"></use>
        </svg>
        <svg class="section-reviews_ornament" data-aos="fade" data-aos-delay="150">
            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite-svg.svg#ornament3"></use>
        </svg>

        <h2 class="section-title" data-aos="fade" data-aos-delay="150">
      <span>
        <!-- Орнамент для десктопа -->
        <svg class="d-none d-md-block">
          <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite-svg.svg#ornament1"></use>
        </svg>
        <svg class="d-none d-md-block">
          <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite-svg.svg#ornament1"></use>
        </svg>

          <!-- Орнамент для мобилы -->
        <span>
          <svg width="24" height="24" class="d-block d-md-none">
            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite-svg.svg#ornament4"></use>
          </svg>
          <svg width="24" height="24" class="d-block d-md-none">
            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite-svg.svg#ornament4"></use>
          </svg>
          Отзывы наших клиентов
        </span>
        </h2>

        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section-reviews_slider swiper-container">
                        <div class="section-reviews_slider-wrapper swiper-wrapper">
                        @foreach($reviews as $review)
                            @if(($loop->iteration)%2!=0 || $loop->first)
                                <!-- Слайд с отзывами -->
                                    <div class="section-reviews_slide swiper-slide">
                                        <div class="section-reviews_slide-inner">
                                        @endif
                                        @if($review->video)
                                            <!-- Видео отзыв -->
                                                <div class="section-reviews_slide-video">
                                                    <div class="review-card review-card-video">
                                                        <a class="review-card-video_link" href="https://youtu.be/{{$review->video}}">
                                                            <picture>
                                                                <source srcset="https://i.ytimg.com/vi_webp/{{$review->video}}/maxresdefault.webp" type="image/webp">
                                                                <img class="review-card-video_media"
                                                                     src="https://i.ytimg.com/vi/1La4QzGeaaQ/maxresdefault.jpg" alt="PERU">
                                                            </picture>
                                                        </a>
                                                        <button class="review-card-video_button" type="button" aria-label="Запустить видео">
                                                            <svg width="80" height="80">
                                                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite-svg.svg#play-button">
                                                                </use>
                                                            </svg>
                                                        </button>
                                                    </div>
                                                    <header class="review-card_header">
                                                        <img src="/storage/{{$review->avatar ?? $review->user->avatar}}" alt="{{$review->name ?? $review->user->name}}" class="review-card_avatar avatar">
                                                        <div class="review-card_reviewer-info">
                                                            <h3 class="review-card_reviewer-name">{{$review->name ?? $review->user->name}}</h3>
                                                            <time class="review-card_date" datetime="{{$review->created_at}}">{{$review->created_at->diffForHumans()}}</time>
                                                        </div>
                                                    </header>
                                                </div>
                                        @else
                                            <!-- Текстовый отзыв -->
                                                <div class="section-reviews_slide-text">
                                                    <article class="review-card review-card-text">
                                                        <header class="review-card_header">
                                                            <img src="/storage/{{$review->avatar ?? $review->user->avatar}}" alt="{{$review->name ?? $review->user->name}}" class="review-card_avatar avatar">
                                                            <div class="review-card_reviewer-info">
                                                                <h3 class="review-card_reviewer-name">{{$review->name ?? $review->user->name}}</h3>
                                                                <time class="review-card_date" datetime="{{$review->created_at}}">{{$review->created_at->diffForHumans()}}</time>
                                                            </div>
                                                        </header>

                                                        <section class="review-card_text">
                                                            {!! $review->text !!}
                                                        </section>
                                                    </article>
                                                </div>
                                            @endif
                                            @if(($loop->iteration)%2==0 || $loop->last)

                                        </div>
                                    </div>
                                @endif
                            @endforeach

                        </div>

                        <button class="section-reviews_arrow section-reviews_arrow-prev">
                            <svg width="37" height="37">
                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite-svg.svg#arrow-left-big-dark">
                                </use>
                            </svg>
                        </button>
                        <button class="section-reviews_arrow section-reviews_arrow-next">
                            <svg width="37" height="37">
                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite-svg.svg#arrow-right-big-dark">
                                </use>
                            </svg>
                        </button>
                    </div>
                </div>
            </div>

            <div class="row section-see-more">
                <div class="col-12 d-flex justify-content-center">
                    <a href="{{route('reviews')}}" class="button button button--main">Посмотреть все отзывы</a>
                </div>
            </div>
        </div>
    </section>

    <section class="section-partners">
        <h2 class="section-title" data-aos="fade" data-aos-delay="150">
      <span>
        <svg width="41" height="41" class="d-none d-md-block">
          <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite-svg.svg#ornament4"></use>
        </svg>
        <svg width="41" height="41" class="d-none d-md-block">
          <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite-svg.svg#ornament4"></use>
        </svg>
        <span>

          <svg width="25" height="5" class="d-block d-md-none">
            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite-svg.svg#dots"></use>
          </svg>
          <svg width="25" height="5" class="d-block d-md-none">
            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite-svg.svg#dots"></use>
          </svg>
          Мы работаем с
        </span>
      </span>
        </h2>

        <div class="container">
            <div class="row">
                @foreach($blocks->where('name','Мы работаем с')->all() as $part)
                    <div class="section-partners_col col-lg-3 col-md-6">
                        <div class="section-partners_item"
                             style="background-image: url('/storage/{{$part->pivot->image}}');">
                            <div class="section-partners_label">{{$part->pivot->title}}</div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>

    <section class="section-application">
        <svg class="section-application_ornament">
            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite-svg.svg#ornament3"></use>
        </svg>
        <svg class="section-application_ornament">
            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite-svg.svg#ornament3"></use>
        </svg>
        <h2 class="section-title" data-aos="fade" data-aos-delay="150">
      <span>
        Оставьте заявку и получите самое
        выгодное предложение от покупателя!
      </span>
        </h2>

        <div class="container">
            <div class="row">
                <div class="col-xl-8 offset-xl-2">
                    <form class="main-slider_form ajax-form" method="post" action="{{route('send-form')}}">
                        {{ csrf_field() }}
                        <input name="lead_id" value="4" type="hidden">
                        <label class="visually-hidden" for="main-slider-field-name4"></label>
                        <input class="field failed--horizontal" required type="text" name="name" placeholder="Ваше Имя"
                               id="main-slider-field-name4">

                        <label class="visually-hidden" for="main-slider-field-phone4"></label>
                        <input class="field failed--horizontal" required type="tel" name="phone" placeholder="Телефон"
                               id="main-slider-field-phone4">

                        <button type="submit" class="button button--form">Оставить заявку</button>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <section class="section-team">
        <h2 class="section-title" data-aos="fade" data-aos-delay="150">
      <span>
        <svg width="53" height="53">
          <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite-svg.svg#ornament5"></use>
        </svg>
        <svg width="53" height="53">
          <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite-svg.svg#ornament5"></use>
        </svg>
        Наша команда
      </span>
        </h2>

        <div class="container">
            <div class="row">
                @foreach($employers as $employer)
                    <div class="section-team_item-container col-12 col-md-6 col-lg-3 d-flex justify-content-center">
                        <div class="section-team_item" data-aos="fade-up" data-aos-delay="150">
                            <div class="section-team_image">
                                <img src="/storage/{{$employer->photo}}" alt="фото сотрудника {{$employer->id}}">
                            </div>
                            <a href="{{route('employer',['id'=>$employer->id])}}" class="section-team_name">{{$employer->first_name}} {{$employer->last_name}}</a>
                            <span class="section-team_bio">{{Str::limit($employer->description,100)}}</span>
                        </div>
                    </div>
                @endforeach
            </div>

            <a href="{{route('employers')}}" class="row section-team_see-more" data-aos="fade" data-aos-delay="150">
                Познакомиться со всеми сотрудниками
                <svg width="54" height="37">
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite-svg.svg#dobble-chevrone"></use>
                </svg>
            </a>
        </div>
    </section>

    <section class="section-hot-offer">
        <h2 class="section-title" data-aos="fade" data-aos-delay="150">
      <span>
        <!-- Орнамент для десктопа -->
        <svg class="d-none d-md-block">
          <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite-svg.svg#ornament1"></use>
        </svg>
        <svg class="d-none d-md-block">
          <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite-svg.svg#ornament1"></use>
        </svg>

          <!-- Орнамент для мобилы -->
        <span>
          <svg width="24" height="24" class="d-block d-md-none">
            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite-svg.svg#ornament4"></use>
          </svg>
          <svg width="24" height="24" class="d-block d-md-none">
            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite-svg.svg#ornament4"></use>
          </svg>
          Горящие объекты недвижимости
        </span>
        </h2>

        <div class="container">
            <div class="row">
                @foreach($objects as $object)
                    <div class="section-hot-offer_obg-card-col col-md-4 col-xl-3">
                        <!-- Карточка объекта -->
                        <a href="{{route('object',['slug'=>$object->slug])}}" class="obg-card obg-card--large" data-aos="fade-up" data-aos-delay="150">
                            <div class="obg-card_label">@if($object->small_title){{$object->small_title}}@else{{$object->rooms}}-х комнатная квартира@endif</div>
                            <!-- Слайдер -->
                            @if($object->images)
                                <div class="obg-card_preview swiper-container">
                                    <div class="obg-card_preview-wrapper swiper-wrapper">
                                    @foreach(json_decode($object->images,1) as $image)
                                        <!-- Слайды -->
                                            <div class="obg-card_preview-slide swiper-slide">
                                                <img src="/storage/{{$image}}" alt="{{$object->name}} {{$loop->iteration}}" style="width: 414px; height: 232px;">
                                            </div>
                                        @endforeach
                                    </div>

                                    <button class="obg-card_arrow obg-card_arrow-prev">
                                        <svg width="14" height="14">
                                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite-svg.svg#arrow-left"></use>
                                        </svg>
                                    </button>
                                    <button class="obg-card_arrow obg-card_arrow-next">
                                        <svg width="14" height="14">
                                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite-svg.svg#arrow-right"></use>
                                        </svg>
                                    </button>
                                </div>
                            @else
                                <div class="obg-card_preview swiper-container">
                                    <div class="obg-card_preview-wrapper swiper-wrapper">
                                        <!-- Слайды -->
                                        <div class="obg-card_preview-slide swiper-slide">
                                            <img src="{{$object->main_image}}" alt="{{$object->name}}" style="width: 414px; height: 232px;">
                                        </div>
                                    </div>


                                </div>
                        @endif
                        <!-- Описание -->
                            <div class="obg-card_desc">
                                <h3 class="obg-card_title">{{$object->name}}</h3>
                                <span class="obg-card_address">
                                <svg width="15" height="15">
                                  <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite-svg.svg#pin"></use>
                                </svg>
                                    {{$object->address}}
                          </span>
                            </div>
                        </a>
                    </div>
                @endforeach
            </div>

            <div class="row section-see-more">
                <div class="col-12 d-flex justify-content-center">
                    <a href="{{route('objects')}}" class="button button button--main" data-aos="fade" data-aos-delay="150">Посмотреть все
                        объекты</a>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('script')
    <script>
        // Скрипты для главного слайдера
        var slideBg = ["./img/slider-bg1.jpg", "./img/slider-bg2.jpg", "./img/slider-bg3.jpg"];
        var paginationText = ["Продать", "Купить", "Обменять"];
        var lead_id = ["1", "2", "3"];
        var mainSlider = new Swiper(".main-slider.swiper-container", {
            grabCursor: true,
            loop: true,
            spaceBetween: 20,
            pagination: {
                el: '.main-slider_pagination',
                clickable: true,
                renderBullet: function (index, className) {
                    return '<button class="button ' + className + '">' + paginationText[index] + '</button>'
                },
            },
            navigation: {
                nextEl: '.main-slider_button-next',
                prevEl: '.main-slider_button-prev',
            },
        });

        mainSlider.on('slideChange', function () {
            var index = mainSlider.realIndex;
            var bg = document.getElementById("mainBg");
            bg.src = slideBg[index];
            $('input[name="type"]').val(paginationText[index]);
            $('input[name="lead_id"]').val(lead_id[index]);
        });

        var reviewSlider = new Swiper(".section-reviews_slider.swiper-container", {
            grabCursor: true,
            loop: true,
            slidesPerView: 1,
            spaceBetween: 30,
            navigation: {
                nextEl: '.section-reviews_arrow-next',
                prevEl: '.section-reviews_arrow-prev',
            },
        })
    </script>
@endsection
