@extends('layouts.main')

@section('title')
    <title>{{$page->title}}</title>
    <meta name="description" content="{{$page->description}}">
@endsection

@section('og')
    <meta property="og:title" content="{{$page->title}}">
    <meta property="og:image" content="{{asset('img/logo_header.svg')}}">
    <meta property="og:type" content="article">
    <meta property="og:url" content="{{route('pages',['slug'=>$page->slug])}}">
    <meta property="og:description" content="{{$page->description}}">
@endsection

@section('content')
    <div class="page-employee">
        <div class="root-container container d-flex flex-column">
            <!-- Хлебные крошки -->
            <div class="breadcrumbs_row row">
                <div class="col-12">
                    <nav class="breadcrumbs">
                        <ul class="breadcrumbs_list">
                            <li class="breadcrumbs_item"><a href="/" class="breadcrumbs_link">Главная</a></li>
                            <li class="breadcrumbs_item"><a href="{{route('pages',['slug'=>'about-company'])}}" class="breadcrumbs_link">О нас</a></li>
                            <li class="breadcrumbs_item"><a href="#" class="breadcrumbs_link breadcrumbs_link--active">Сотрудники</a>
                        </ul>
                    </nav>
                </div>
            </div>

            <div class="base-title_row row">
                <div class="col-12">
                    <h1 class="base-title base-title--super-large" data-aos="fade" data-aos-delay="150">
                        <svg class="base-title-icon d-none d-md-block" width="40" height="40">
                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite-svg.svg#ornament2"></use>
                        </svg>
                        <svg class="base-title-icon d-none d-md-block" width="40" height="40">
                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite-svg.svg#ornament2"></use>
                        </svg>
                        Наша команда
                    </h1>
                </div>
            </div>

            <div class="page-employee_row row">
                <div class="col-12">
                    <h2 data-aos="fade-up" data-aos-delay="150"
                        class="page-employee_base-title-h2 base-title page-work-with_base-title-h2 base-title--super-large base-title--icon-long px-0">
                        Руководство
                    </h2>
                </div>
                @foreach($employers->where('department','Руководство')->all() as $boss)
                <div class="page-employee_col col-md-4 ">
                    <div class="page-employee-item" data-aos="fade-up" data-aos-delay="100">
                        <div class="page-employee-item_preview">
                            <img src="/storage/{{$boss->photo}}" alt="{{$boss->first_name}}">
                        </div>
                        <div class="page-employee-item_bio">
                            <span class="page-employee-item_name">{{$boss->first_name}} {{$boss->last_name}}</span>
                            <span class="page-employee-item_position">{{$boss->position}}</span>
                        </div>
                        <div class="page-employee-item_actions">
                            <a href="{{route('employer',['id'=>$boss->id])}}">подробнее...</a>
                            <a href="{{route('employer',['id'=>$boss->id])}}#revievs" class="page-employee-item_actions--gray">отзывы</a>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>

        <div class="container--light page-employee_container-arnament">
            <div class="container">
                <div class="page-employee_row row">
                    <div class="col-12">
                        <h2 data-aos="fade-up" data-aos-delay="150"
                            class="page-employee_base-title-h2 base-title page-work-with_base-title-h2 base-title--super-large base-title--icon-long px-0">
                            Риeлторы
                        </h2>
                    </div>
                    @foreach($employers->where('department','Риэлторы')->all() as $realtor)
                        <div class="page-employee_col col-md-6 col-lg-3" data-aos="fade-up" data-aos-delay="100">
                            <div class="page-employee-item">
                                <div class="page-employee-item_preview">
                                    <img src="/storage/{{$realtor->photo}}" alt="{{$realtor->first_name}}">
                                </div>
                                <div class="page-employee-item_bio">
                                    <span class="page-employee-item_name">{{$realtor->first_name}} {{$realtor->last_name}}</span>
                                    <span class="page-employee-item_position">{{$realtor->position}}</span>
                                </div>
                                <div class="page-employee-item_actions">
                                    <a href="{{route('employer',['id'=>$realtor->id])}}">подробнее...</a>
                                    <a href="{{route('employer',['id'=>$realtor->id])}}#revievs" class="page-employee-item_actions--gray">отзывы</a>
                                </div>
                            </div>
                        </div>
                    @endforeach

                </div>
            </div>
        </div>

        <div class="container pt-5">
            <div class="page-employee_row row">
                <div class="col-12">
                    <h2 data-aos="fade-up" data-aos-delay="150"
                        class="page-employee_base-title-h2 base-title page-work-with_base-title-h2 base-title--super-large base-title--icon-long px-0">
                        Агенты по операциям с недвижимостью
                    </h2>
                </div>

                @foreach($employers->where('department','Агенты')->all() as $agent)
                    <div class="page-employee_col col-md-6 col-lg-3" data-aos="fade-up" data-aos-delay="100">
                        <div class="page-employee-item">
                            <div class="page-employee-item_preview">
                                <img src="/storage/{{$agent->photo}}" alt="{{$agent->first_name}}">
                            </div>
                            <div class="page-employee-item_bio">
                                <span class="page-employee-item_name">{{$agent->first_name}} {{$agent->last_name}}</span>
                                <span class="page-employee-item_position">{{$agent->position}}</span>
                            </div>
                            <div class="page-employee-item_actions">
                                <a href="{{route('employer',['id'=>$agent->id])}}">подробнее...</a>
                                <a href="{{route('employer',['id'=>$agent->id])}}#revievs" class="page-employee-item_actions--gray">отзывы</a>
                            </div>
                        </div>
                    </div>
                    @if($loop->iteration%4==0 && !$loop->last)
                        </div>
                        <div class="page-employee_row row">
                    @endif
                @endforeach
            </div>
        </div>

        <div class="container--light page-employee_container-arnament">
            <div class="container">
                <div class="page-employee_row row">
                    <div class="col-12">
                        <h2 data-aos="fade-up" data-aos-delay="150"
                            class="page-employee_base-title-h2 base-title page-work-with_base-title-h2 base-title--super-large base-title--icon-long px-0">
                            Другие специалисты
                        </h2>
                    </div>
                    @foreach($employers->where('department','Другие')->all() as $other)
                        <div class="page-employee_col col-md-6 col-lg-4" data-aos="fade-up" data-aos-delay="100">
                            <div class="page-employee-item">
                                <div class="page-employee-item_preview">
                                    <img src="/storage/{{$other->photo}}" alt="{{$other->first_name}}">
                                </div>
                                <div class="page-employee-item_bio">
                                    <span class="page-employee-item_name">{{$other->first_name}} {{$other->last_name}}</span>
                                    <span class="page-employee-item_position">{{$other->position}}</span>
                                </div>
                                <div class="page-employee-item_actions">
                                    <a href="{{route('employer',['id'=>$other->id])}}">подробнее...</a>
                                    <a href="{{route('employer',['id'=>$other->id])}}#revievs" class="page-employee-item_actions--gray">отзывы</a>
                                </div>
                            </div>
                        </div>
                    @endforeach

                    <div class="col-12 d-flex justify-content-center d-none" data-aos="fade" data-aos-delay="150">
                        <button class="page-employee-load">
                            <svg width="64" height="80">
                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite-svg.svg#reload">
                                </use>
                            </svg>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
