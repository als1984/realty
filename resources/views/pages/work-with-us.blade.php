@extends('layouts.main')

@section('title')
    <title>{{$page->title}}</title>
    <meta name="description" content="{{$page->description}}">
@endsection

@section('og')
    <meta property="og:title" content="{{$page->title}}">
    <meta property="og:image" content="{{asset('img/logo_header.svg')}}">
    <meta property="og:type" content="article">
    <meta property="og:url" content="{{route('pages',['slug'=>$page->slug])}}">
    <meta property="og:description" content="{{$page->description}}">
@endsection

@section('content')
    <div class="page-work-with">
        <div class="root-container container d-flex flex-column">
            <!-- Хлебные крошки -->
            <div class="breadcrumbs_row row">
                <div class="col-12">
                    <nav class="breadcrumbs">
                        <ul class="breadcrumbs_list">
                            <li class="breadcrumbs_item"><a href="/" class="breadcrumbs_link">Главная</a></li>
                            <li class="breadcrumbs_item">
                                <a href="{{route('pages',['slug'=>'about-company'])}}" class="breadcrumbs_link">О
                                    нас</a></li>
                            <li class="breadcrumbs_item">
                                <a href="" class="breadcrumbs_link breadcrumbs_link--active">{{$page->title}}</a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>

            <div class="base-title_row row">
                <div class="col-12">
                    <h1 class="base-title base-title--super-large base-title--icon-accent" data-aos="fade" data-aos-delay="150">
                        <svg class="base-title-icon" width="40" height="40">
                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite-svg.svg#ornament2"></use>
                        </svg>
                        <svg class="base-title-icon" width="40" height="40">
                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite-svg.svg#ornament2"></use>
                        </svg>
                        {{$page->name}}
                    </h1>
                </div>
            </div>

            <div class="row">
                {!! $page->content !!}
            </div>

            <div class="page-work-with_link-row row">
                @foreach($blocks->where('name','Работа в нашей компании')->take(6) as $block)
                    @if(($loop->iteration) % 2 != 0)
                        <div class="page-work-with_link-col col-md-4">
                            <div class="page-work-with_link-item">
                                <a href="#block_{{$block->pivot->order}}" class="page-work-with-anchor" data-scroll-link="block_{{$block->pivot->order}}">{{$block->pivot->title}}</a>
                                @else
                                    <a href="#block_{{$block->pivot->order}}" class="page-work-with-anchor" data-scroll-link="block_{{$block->pivot->order}}">{{$block->pivot->title}}</a>
                            </div>
                        </div>
                    @endif
                    @if((($loop->iteration) % 2 != 0) && ($loop->last))
                        </div>
                    </div>
                @endif
                @endforeach

            </div>
            <div class="clearfix"></div>
            <div class="col-12 d-flex justify-content-center section-see-more">
                <a href="#" data-toggle="modal" data-target="#workCall" class="button button button--main">Хочу работать!</a>
            </div>
        </div>

        <div class="page-work-with_content">
            <!-- Ненавижу дизайнера за эти ебаные орнаменты -->
            <div class="page-work-with_ornament-row">
                <svg class="page-work-with_ornament">
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite-svg.svg#ornament3"></use>
                </svg>
                <svg class="page-work-with_ornament">
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite-svg.svg#ornament3"></use>
                </svg>
            </div>

            <div class="page-work-with_ornament-row">
                <svg class="page-work-with_ornament">
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite-svg.svg#ornament3"></use>
                </svg>
                <svg class="page-work-with_ornament">
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite-svg.svg#ornament3"></use>
                </svg>
            </div>

            <div class="container">
                @foreach($blocks->where('name','Работа в нашей компании')->take(6) as $block)
                    @if($block->pivot->image)
                        <div class="row">
                            <div class="col-12">
                                <h1 id="block_{{$block->pivot->order}}"
                                    class="base-title page-work-with_base-title-h2 base-title--super-large base-title--icon-long"
                                    data-aos="fade" data-aos-delay="150">
                                    <svg class="base-title-icon">
                                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite-svg.svg#ornament1"></use>
                                    </svg>
                                    <svg class="base-title-icon">
                                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite-svg.svg#ornament1"></use>
                                    </svg>
                                    {{$block->pivot->title}}
                                </h1>
                            </div>
                        </div>

                        <div class="row position-relative">
                            <div class="offset-md-2 offset-xl-3 col-md-8 col-xl-6">
                                <img src="{{asset('/storage/'.$block->pivot->image)}}" alt="alt">

                                <p class="mt-3 mt-xl-5" data-aos="fade" data-aos-delay="150">
                                    {!! $block->pivot->text !!}
                                </p>
                            </div>
                        </div>
                    @else
                        <div class="icon-devider-row row">
                            <div class="col-12 d-flex justify-content-center">
                                <svg class="icon-devider" data-aos="fade" data-aos-delay="50">
                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite-svg.svg#ornament1"></use>
                                </svg>
                            </div>
                        </div>

                        <div class="row">
                            <div class="offset-md-2 offset-xl-3 col-md-8 col-xl-6">
                                <h1 id="block_{{$block->pivot->order}}"
                                    class="base-title page-work-with_base-title-h2 base-title--super-large base-title--icon-long px-0"
                                    data-aos="fade" data-aos-delay="150">
                                    {{$block->pivot->title}}
                                </h1>

                                <p class="mt-3 mt-xl-5" data-aos="fade" data-aos-delay="150">
                                    {!! $block->pivot->text !!}
                                </p>
                            </div>
                        </div>
                    @endif
                @endforeach

            </div>
        </div>

        <div class="page-work-with_footer">
            <div class="container">
                <div class="row d-flex align-items-center">
                    <div class="col-sm-6 col-md-7">
                        @foreach($blocks->where('name','Работа в нашей компании форма')->take(6) as $block)
                            @if($block->pivot->title != 'Анкета')
                                <div class="page-work-with_footer-item">
                                    <h3>{{$block->pivot->title}}</h3>
                                    {!! $block->pivot->text !!}
                                </div>
                            @endif
                        @endforeach

                        <div class="page-work-with_footer-item">
                            <h3>Контакты</h3>
                            <p>
                                <a href="tel:{{setting('site.phone')}}">{{setting('site.phone')}}</a>
                                <a href="tel:{{setting('site.phone2')}}">{{setting('site.phone2')}}</a>
                            </p>
                        </div>
                    </div>

                    <div class="offset-md-1 col-sm-6 col-md-4 col-xl-3 mt-5 mt-md-0">
                        <form class="page-work-with_form" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="invoice_type" value="1101">
                            <input type="text" name="name" class="field" required placeholder="Имя">
                            <input type="text" name="last_name" class="field" required placeholder="Фамилия">
                            <input type="email" name="email" class="field" required placeholder="e-mail">
                            <input type="tel" name="phone" class="field" required placeholder="Телефон">
                            <div class="page-work-with_form-attach-wrapper">
                                @foreach($blocks->where('name','Работа в нашей компании форма')->take(6) as $block)
                                    @if($block->pivot->title == 'Анкета')
                                        <a href="{{asset('/storage/'.json_decode($block->pivot->file,1)[0]['download_link'])}}" download class="page-work-with_form-attach">
                                            <svg width="15" height="15">
                                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite-svg.svg#download"></use>
                                            </svg>
                                            Скачать анкету
                                        </a>
                                    @endif
                                @endforeach
                                <label class="page-work-with_form-attach">
                                    <svg width="15" height="15">
                                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite-svg.svg#attach"></use>
                                    </svg>
                                    Прикрепить анкету
                                    <input type="file" name="file">
                                </label>
                            </div>

                            <button type="submit" class="button button--form">Отправить</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
    <div class="request-call-modal modal fade" id="workCall" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">

                <button class="close" aria-label="Закрыть" data-dismiss="modal"></button>
                <svg class="request-call-modal_ornament" data-aos="fade" data-aos-delay="200">
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/img/sprite-svg.svg#ornament3"></use>
                </svg>
                <svg class="request-call-modal_ornament" data-aos="fade" data-aos-delay="200">
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/img/sprite-svg.svg#ornament3"></use>
                </svg>

                <form class="request-call-modal_form ajax-form" method="post" action="{{route('add-invoice')}}">
                    {{ csrf_field() }}
                    <input type="hidden" name="invoice_type" value="1100">
                    <label class="visually-hidden" for="request-call-modal-field-name"></label>
                    <input class="field failed--horizontal" type="text" name="name" required placeholder="Ваше Имя"
                           id="request-call-modal-field-name">

                    <label class="visually-hidden" for="request-call-modal-phone"></label>
                    <input class="field failed--horizontal" type="tel" name="phone" required placeholder="Телефон"
                           id="request-call-modal-name">

                    {{--<label class="visually-hidden" for="request-call-modal-phone"></label>--}}
                    {{--<input class="field failed--horizontal" type="text" name="price"  placeholder="Укажите цену"--}}
                    {{--id="request-call-modal-name">--}}

                    <button type="submit" class="button button--form">Оставить
                        заявку</button>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $('.page-work-with_form').submit(function (e) {
            e.preventDefault();
            let data=new FormData($('.page-work-with_form')[0]);
            $.ajax({
                url: '{{route('add-invoice')}}',
                method: 'post',
                data: data,
                processData: false,
                contentType: false,
                success: function (date) {
                    $.toast({
                        text : date.text,
                        heading: date.heading,
                        position: 'top-right',
                        bgColor: '#2ecc71',
                        textColor: '#fff',
                    });
                },
                error: function (date) {
                    $.toast({
                        text : date.error,
                        heading: date.heading ? date.heading  : 'Ошибка',
                        position: 'top-right',
                        bgColor: '#ff2a45',
                        textColor: '#fff',
                    });
                }
            })
        })
    </script>
@endsection
