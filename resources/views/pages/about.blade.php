@extends('layouts.main')

@section('title')
    <title>{{$page->title}}</title>
    <meta name="description" content="{{$page->description}}">
@endsection

@section('og')
    <meta property="og:title" content="{{$page->title}}">
    <meta property="og:image" content="{{asset('/img/about-comp-image.jpg')}}">
    <meta property="og:type" content="article">
    <meta property="og:url" content="{{route('pages',['slug'=>$page->slug])}}">
    <meta property="og:description" content="{{$page->description}}">
@endsection

@section('content')
    <div class="page-about">
        <div class="root-container container d-flex flex-column">
            <!-- Хлебные крошки -->
            <div class="breadcrumbs_row row">
                <div class="col-12">
                    <nav class="breadcrumbs">
                        <ul class="breadcrumbs_list">
                            <li class="breadcrumbs_item"><a href="/" class="breadcrumbs_link">Главная</a></li>
                            {{--<li class="breadcrumbs_item"><a href="./object.html" class="breadcrumbs_link">О нас</a></li>--}}
                            <li class="breadcrumbs_item"><a href="#" class="breadcrumbs_link breadcrumbs_link--active">{{$page->name ?? 'О компании'}}</a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>

            <div class="base-title_row row">
                <div class="col-12">
                    <h1 class="base-title base-title--super-large base-title--icon-accent" data-aos="fade" data-aos-delay="150">
                        <svg class="base-title-icon" width="40" height="40">
                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite-svg.svg#ornament2"></use>
                        </svg>
                        <svg class="base-title-icon" width="40" height="40">
                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite-svg.svg#ornament2"></use>
                        </svg>
                        {{$page->name ?? 'О компании'}}
                    </h1>
                </div>
            </div>

            {{--<div class="row">--}}
                {{--<div class="col-md-6">--}}
                    {{--<p data-aos="fade" data-aos-delay="200">--}}
                        {{--Уласны дах- агентство средних размеров. Обладая значительным опытом и компетенцией, мы сохраняем гибкость и--}}
                        {{--скорость работы.--}}
                        {{--Мы полны профессионального азарта, энергичны и нацелены на результат для наших клиентов.--}}
                        {{--В своих отзывах наши клиенты подчеркивают отточенность действий специалистов и грамотную организацию--}}
                        {{--процедуры--}}
                        {{--совершения сделки.--}}
                    {{--</p>--}}
                {{--</div>--}}
                {{--<div class="col-md-6">--}}
                    {{--<p data-aos="fade" data-aos-delay="300">--}}
                        {{--Уласны дах силен в продвижении объектов недвижимости. Персональные сайты для объекта, видео на YouTube,--}}
                        {{--Instagram и другие социальные сети, реклама более чем на 20 каналах продаж – и это не полный перечень того,--}}
                        {{--что мы используем чтобы привлекать больше покупателей, и в итоге продавать быстрее и дороже.--}}
                        {{--Наша цель – довольные клиенты, получившие риэлторские услуги первоклассного качества.--}}
                    {{--</p>--}}
                {{--</div>--}}
            {{--</div>--}}
            {!! $page->content !!}
        </div>

        <div class="position-relative overflow-hidden">
            <svg class="page-about-slider_ornament" data-aos="fade" data-aos-delay="150">
                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite-svg.svg#ornament3"></use>
            </svg>
            <svg class="page-about-slider_ornament" data-aos="fade" data-aos-delay="150">
                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite-svg.svg#ornament3"></use>
            </svg>
            <div class="container position-relative">
                <div class="row">
                    <div class="col-12">
                        <div class="page-about_slider swiper-container">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide swiper-slide--image" style="height: auto;">
                                    <img src="/storage/{{$page->image}}" alt="{{$page->name}}" style="height: auto;">
                                </div>
                                {{--<div class="swiper-slide swiper-slide--video">--}}
                                    {{--<div class="review-card review-card-video">--}}
                                        {{--<a class="review-card-video_link" href="https://youtu.be/1La4QzGeaaQ">--}}
                                            {{--<picture>--}}
                                                {{--<source srcset="https://i.ytimg.com/vi_webp/1La4QzGeaaQ/maxresdefault.webp" type="image/webp">--}}
                                                {{--<img class="review-card-video_media" src="https://i.ytimg.com/vi/1La4QzGeaaQ/maxresdefault.jpg"--}}
                                                     {{--alt="PERU">--}}
                                            {{--</picture>--}}
                                        {{--</a>--}}
                                        {{--<button class="review-card-video_button" type="button" aria-label="Запустить видео">--}}
                                            {{--<svg width="80" height="80">--}}
                                                {{--<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite-svg.svg#play-button">--}}
                                                {{--</use>--}}
                                            {{--</svg>--}}
                                        {{--</button>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            </div>

                        </div>
                        <!--button class="section-reviews_arrow section-reviews_arrow-prev">
                            <svg width="37" height="37">
                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite-svg.svg#arrow-left-big-dark">
                                </use>
                            </svg>
                        </button>
                        <button class="section-reviews_arrow section-reviews_arrow-next">
                            <svg width="37" height="37">
                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite-svg.svg#arrow-right-big-dark">
                                </use>
                            </svg>
                        </button-->
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h2
                            class="page-about_base-title-h2 base-title base-title--accent base-title--super-large base-title--icon-long"
                            data-aos="fade" data-aos-delay="150">
                        <svg class="base-title-icon">
                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite-svg.svg#ornament1"></use>
                        </svg>
                        <svg class="base-title-icon">
                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite-svg.svg#ornament1"></use>
                        </svg>
                        Наши преимущества
                    </h2>
                </div>
            </div>

            <div class="row">
                <div class="section-advantages-card-container col-md-4">
                    <div class="section-advantages-card" data-aos="fade-up" data-aos-delay="150">
                        <div class="section-advantages-card_header">
                            <svg width="61" height="61">
                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite-svg.svg#reliably"></use>
                            </svg>
                            <h3>Просто</h3>
                        </div>

                        <p class="section-advantages-card_text">С нами легко и просто, а все сложное делаем простым. Мы вас слышим,
                            понимаем, и делаем, что надо именно вам.</p>
                    </div>
                </div>

                <div class="section-advantages-card-container col-md-4">
                    <div class="section-advantages-card" data-aos="fade-up" data-aos-delay="350">
                        <div class="section-advantages-card_header">
                            <svg width="61" height="61">
                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite-svg.svg#safely"></use>
                            </svg>
                            <h3>Понятно</h3>
                        </div>

                        <p class="section-advantages-card_text">Рассказываем. Показываем. Объясняем. Убеждаем. Решение принимаете
                            вы.</p>
                    </div>
                </div>

                <div class="section-advantages-card-container col-md-4">
                    <div class="section-advantages-card" data-aos="fade-up" data-aos-delay="550">
                        <div class="section-advantages-card_header">
                            <svg width="61" height="61">
                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite-svg.svg#conveniently"></use>
                            </svg>
                            <h3>Прогнозируемо</h3>
                        </div>

                        <p class="section-advantages-card_text">Всякий раз есть «дорожная карта» решения любого вопроса. Всегда есть
                            «план Б». Вы заранее знаете, что будет.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
