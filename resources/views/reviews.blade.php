@extends('layouts.main')

@section('title')
    <title>Отзывы</title>
    <meta name="description" content="Отзывы наших клиентов">
@endsection

@section('og')
    <meta property="og:title" content="Отзывы">
    <meta property="og:image" content="{{asset('img/logo_header.svg')}}">
    <meta property="og:type" content="article">
    <meta property="og:url" content="{{route('reviews')}}">
    <meta property="og:description" content="Отзывы наших клиентов">
@endsection

@section('content')
    <div class="root-container container">

        <!-- Хлебные крошки -->
        <div class="breadcrumbs_row row">
            <div class="col-12">
                <nav class="breadcrumbs">
                    <ul class="breadcrumbs_list">
                        <li class="breadcrumbs_item">
                            <a href="/" class="breadcrumbs_link">Главная</a>
                        </li>
                        <li class="breadcrumbs_item">
                            <a href="{{route('pages',['slug'=>'about-company'])}}" class="breadcrumbs_link">О нас</a>
                        </li>
                        <li class="breadcrumbs_item">
                            <a href="#" class="breadcrumbs_link breadcrumbs_link--active">Отзывы</a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>

        <div class="page-reviews_top-row row">
            <div class="col-12">
                <h2 class="section-title">
                    <svg width="41" height="41">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/img/sprite-svg.svg#ornament2"></use>
                    </svg>
                    <svg width="41" height="41">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/img/sprite-svg.svg#ornament2"></use>
                    </svg>
                    Отзывы наших клиентов
                </h2>
            </div>

            <div class="offset-lg-2 offset-xl-3 col-lg-8 col-xl-6">
                <h3 class="page-reviews_form-title">Оставьте ваш отзыв о нас</h3>

                <form action="" class="page-reviews_form" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-xl-12 d-flex flex-column flex-md-row">
                            <input type="text" class="field" name="name" required placeholder="Ваше имя">
                            <select name="employer" class="field custom-select">
                                <option value="0">С кем вы работали</option>
                                @foreach($employers as $employer)
                                    <option value="{{$employer->id}}">{{$employer->first_name}} {{$employer->last_name}}</option>
                                @endforeach
                            </select>
                            <!-- <input type="text" class="field" placeholder="С кем вы работали"> -->
                        </div>

                        <div class="col-12 mt-3">
                            <textarea class="field" name="text" required placeholder="Ваш отзыв"></textarea>
                        </div>

                        <div class="col-12 mt-4 mt-md-5 d-flex flex-column flex-md-row">
                            <label class="page-work-with_form-attach">
                                <svg width="15" height="15">
                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/img/sprite-svg.svg#attach"></use>
                                </svg>
                                Прикрепить фото
                                <input type="file" name="avatar">
                            </label>
                            <button type="submit" class="button button--form">оставить отзыв</button>
                        </div>
                    </div>
                </form>

                <div class="page-reviews_top-tabbar controls">
                    <button type="button" class="page-reviews_top-tabbar-button " data-filter="all">Все</button>
                    <button type="button" class="page-reviews_top-tabbar-button" data-filter=".video-review">Видео</button>
                    <button type="button" class="page-reviews_top-tabbar-button" data-filter=".text-review">Текстовые</button>
                </div>
            </div>
        </div>

        <div class="page-reviews_content row">
            <div class="offset-xl-1 col-xl-9">
                <ul class="page-reviews_list containerMix">
                    @foreach($reviews as $review)
                        @if($review->published)
                    <li class="mix page-reviews_item @if($review->video) video-review @else text-review @endif">
                        <div class="d-flex flex-column flex-md-row align-items-start">
                            <div class="page-reviews_item-mobile-header d-flex align-items-center">
                                <div class="page-reviews_item-avatar" style="background-image: url('/storage/{{$review->avatar ?? $review->user->avatar}}');"></div>
                                <div class="d-flex d-md-none flex-column">
                                    <h3 class="page-reviews_item-comment-author">{{$review->name ?? $review->user->name}}</h3>
                                    <span class="page-reviews_item-comment-position">Клиент</span>
                                </div>
                            </div>
                            <div class="page-reviews_item-comment">
                                <h3 class="page-reviews_item-comment-author d-none d-md-block">{{$review->name ?? $review->user->name}}</h3>
                                <span class="page-reviews_item-comment-position d-none d-md-block">Клиент</span>
                                @if($review->video)
                                    <div class="review-card review-card-video">
                                        <a class="review-card-video_link" href="https://youtu.be/{{$review->video}}">
                                            <picture>
                                                {{--<source srcset="storage/{{$review->image}}" type="image/webp">--}}
                                                <img class="review-card-video_media" src="/storage/{{$review->image}}" alt="{{$review->name ?? $review->user->name}}">
                                            </picture>
                                        </a>
                                        <button class="review-card-video_button" type="button" aria-label="Запустить видео">
                                            <svg width="80" height="80">
                                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/img/sprite-svg.svg#play-button">
                                                </use>
                                            </svg>
                                        </button>
                                    </div>
                                @else
                                    <p>{{strip_tags($review->text)}}</p>
                                @endif
                            </div>
                        </div>
                        @if($review->ansvers()->count())
                        <ul class="page-reviews_list">
                            @foreach($review->ansvers()->get() as $ansver)
                            <li class="page-reviews_item">
                                <div class="d-flex flex-column flex-md-row align-items-start">
                                    <div class="page-reviews_item-mobile-header d-flex align-items-center">
                                        <div class="page-reviews_item-avatar" style="background-image: url('/storage/{{$ansver->avatar ?? $ansver->user->avatar}}');"></div>
                                        <div class="d-flex d-md-none flex-column">
                                            <h3 class="page-reviews_item-comment-author">{{$ansver->name ?? $ansver->user->name}}</h3>
                                            <span class="page-reviews_item-comment-position">Агент</span>
                                            <button class="page-reviews_item-request-call" data-toggle="modal"
                                                    data-target="#employerCall">заказать звонок
                                            </button>
                                        </div>
                                    </div>
                                    <div class="page-reviews_item-comment">
                                        <h3 class="page-reviews_item-comment-author d-none d-md-block">{{$ansver->name ?? $ansver->user->name}}</h3>
                                        <span class="page-reviews_item-comment-position d-none d-md-block">Агент</span>
                                        <button class="page-reviews_item-request-call d-none d-md-block" data-toggle="modal"
                                                data-target="#employerCall">заказать звонок
                                        </button>
                                        <div class="page-reviews_item-text">
                                            <p>{{strip_tags($ansver->text)}}</p>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            @endforeach
                        </ul>
                        @endif
                    </li>
                        @endif
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
    {{--</div>--}}

    <!-- Request Call Modal -->
    <div class="request-call-modal modal fade" id="employerCall" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">

                <button class="close" aria-label="Закрыть" data-dismiss="modal"></button>
                <svg class="request-call-modal_ornament" data-aos="fade" data-aos-delay="200">
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/img/sprite-svg.svg#ornament3"></use>
                </svg>
                <svg class="request-call-modal_ornament" data-aos="fade" data-aos-delay="200">
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/img/sprite-svg.svg#ornament3"></use>
                </svg>

                <form class="request-call-modal_form ajax-form" method="post" action="{{route('send-form')}}">
                    {{ csrf_field() }}
                    @isset($object)
                        <input type="hidden" name="object" value="{{$object->name}}">
                        <input type="hidden" name="employer" value="{{$employer->first_name}} {{$employer->last_name}}">
                    @endisset
                    <label class="visually-hidden" for="request-call-modal-field-name"></label>
                    <input class="field failed--horizontal" type="text" name="name" required placeholder="Ваше Имя"
                           id="request-call-modal-field-name">

                    <label class="visually-hidden" for="request-call-modal-phone"></label>
                    <input class="field failed--horizontal" type="tel" name="phone" required placeholder="Телефон"
                           id="request-call-modal-name">

                    <button type="submit" class="button button--form">Оставить
                        заявку
                    </button>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{asset('js/mixitup.min.js')}}"></script>
    <script>
        var mixer = mixitup('.containerMix');
    </script>
    <script>
        $('.page-reviews_form').submit(function (e) {
            e.preventDefault();
            let data = new FormData(this);
            $.ajax({
                url: '{{route('add-review')}}',
                method: 'post',
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                success: function (date) {
                    $.toast({
                        text : date.text,
                        heading: date.heading,
                        position: 'top-right',
                        bgColor: '#2ecc71',
                        textColor: '#fff',
                    });
                },
                error: function (date) {
                    $.toast({
                        text : date.error,
                        heading: date.heading ? date.heading  : 'Ошибка',
                        position: 'top-right',
                        bgColor: '#ff2a45',
                        textColor: '#fff',
                    });
                }
            });
        });
    </script>
@endsection
