@extends('layouts.main')

@section('title')
    <title>404</title>
    <meta name="description" content="404 error">
@endsection

@section('content')
    <div class="root-container container d-flex flex-column">
        <!-- Хлебные крошки -->
        <div class="breadcrumbs_row row">
            <div class="col-12">
                <nav class="breadcrumbs">
                    <ul class="breadcrumbs_list">
                        <li class="breadcrumbs_item"><a href="/" class="breadcrumbs_link">Главная</a></li>
                    </ul>
                </nav>
            </div>
        </div>

        <div class="page-error">
            <svg class="page-error_bg-image">
                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite-svg.svg#ornament3"></use>
            </svg>
            <span class="page-error_label">Ошибка</span>
            <span class="page-error_code">
        404
      </span>
            <!-- <span class="page-error_title">Страница не найдена</span> -->
            <h1 class="base-title page-work-with_base-title-h2 base-title--super-large base-title--icon-long" data-aos="fade"
                data-aos-delay="150">
                <svg class="base-title-icon">
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite-svg.svg#ornament1"></use>
                </svg>
                <svg class="base-title-icon">
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite-svg.svg#ornament1"></use>
                </svg>
                Страница не найдена
            </h1>
        </div>
    </div>

    <!-- Request Call Modal -->
    <div class="request-call-modal modal fade" id="requestCall" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">

                <button class="close" aria-label="Закрыть" data-dismiss="modal"></button>
                <svg class="request-call-modal_ornament" data-aos="fade" data-aos-delay="200">
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite-svg.svg#ornament3"></use>
                </svg>
                <svg class="request-call-modal_ornament" data-aos="fade" data-aos-delay="200">
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite-svg.svg#ornament3"></use>
                </svg>

                <form class="request-call-modal_form">
                    <label class="visually-hidden" for="request-call-modal-field-name"></label>
                    <input class="field failed--horizontal" type="text" name="name" placeholder="Ваше Имя"
                           id="request-call-modal-field-name">

                    <label class="visually-hidden" for="request-call-modal-phone"></label>
                    <input class="field failed--horizontal" type="tel" name="phone" placeholder="Телефон"
                           id="request-call-modal-name">

                    <button type="submit" class="button button--form">Оставить
                        заявку</button>
                </form>
            </div>
        </div>
    </div>
@endsection