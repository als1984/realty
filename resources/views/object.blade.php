@extends('layouts.main')

@section('title')
    <title>{{$object->title}}</title>
    <meta name="description" content="{{$object->description}}">
@endsection

@section('css')
    <script src="https://api-maps.yandex.ru/2.1/?apikey=0c615af4-d453-4a88-986b-7bf18ed04914&lang=ru_RU"
            type="text/javascript">
    </script>
    <style>
        .page-object-gallery .gallery-thumbs .swiper-slide{
            max-width: 160px!important;
        }
        .page-object-gallery .gallery-top .swiper-slide{
            background-size: contain;
        }
    </style>
@endsection

@section('og')
    <meta property="og:title" content="{{$object->title}}">
    <meta property="og:image" content="{{asset('/storage/'.$object->main_image)}}">
    <meta property="og:type" content="article">
    <meta property="og:url" content="{{route('employer',['slug'=>$object->slug])}}">
    <meta property="og:description" content="{{$object->description}}">
@endsection

@section('content')
    <div class="root-container container">

        <!-- Хлебные крошки -->
        <div class="breadcrumbs_row row">
            <div class="col-12">
                <nav class="breadcrumbs">
                    <ul class="breadcrumbs_list">
                        <li class="breadcrumbs_item"><a href="/" class="breadcrumbs_link">Главная</a></li>
                        <li class="breadcrumbs_item"><a href="{{route('objects')}}" class="breadcrumbs_link">Объекты</a>
                        </li>
                        <li class="breadcrumbs_item">
                            <a href="#" class="breadcrumbs_link breadcrumbs_link--active">{{Str::limit($object->name,15)}}</a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>

        <div class="row" id="object-description">
            <div class="offset-md-3 col-12 col-md-7">
                <h1 class="page-object_title-name">{{$object->name}}</h1>
            </div>
        </div>

        <div class="row">
            <div class="col-md-3 order-2 order-md-0 d-flex flex-column d-md-block order-md-1">
                <div class="page-object_recommended order-1 order-md-0 d-block d-md-none d-xl-block">
                    <h4 class="page-object_h4">Рекомендуем</h4>
                    <ul class="page-object_recommended-list">
                        @foreach($blocks->where('block_id','5')->all() as $part)
                            <li class="page-object_recommended-item">
                                <h5>{{$part->title}}</h5>
                                <p>{{strip_tags($part->text)}}</p>
                            </li>
                        @endforeach
                    </ul>
                </div>

                <div class="page-object_desc order-0 order-md-0">
                    <h4 class="page-object_h4">Содержимое</h4>
                    <ul class="page-object_desc-list">
                        <li class="page-object_desc-item">
                            @if($object->type)
                                @switch(json_decode($object->type,1)[0])
                                    @case('buy')
                                    <h5>
                                        <svg width="25" height="25">
                                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/img/sprite-svg.svg#auction-hammer"></use>
                                        </svg>
                                        Покупка
                                    </h5>
                                    @break
                                    @case('sell')
                                    <h5>
                                        <svg width="25" height="25">
                                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/img/sprite-svg.svg#auction-hammer"></use>
                                        </svg>
                                        Продажа
                                    </h5>
                                    @break
                                    @case('rent')
                                    <h5>
                                        <svg width="25" height="25">
                                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/img/sprite-svg.svg#auction-hammer"></use>
                                        </svg>
                                        Аренда
                                    </h5>
                                    @break
                                @endswitch
                            @endif
                            <p>
                                {{Str::limit($object->description,50)}}
                            </p>
                        </li>
                        <li class="page-object_desc-item">
                            <h5 class="page-object_desc-title-normal">
                                <svg width="20" height="24">
                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/img/sprite-svg.svg#pin-outline"></use>
                                </svg>
                                {{$object->address}}
                            </h5>
                        </li>
                    </ul>
                </div>

                <div class="d-block d-xl-none order-2 order-md-0">
                    <h4 class="page-object_h4 mt-5 mt-md-0 text-center">Ваш агент</h4>
                    <div class="page-object-agent">
                        <div class="page-object-agent_preview">
                            <img src="/storage/{{$employer->photo}}" alt="{{$employer->first_name}}">
                        </div>
                        <div class="page-object-agent_contact">
                            <span class="page-object-agent_name">{{$employer->first_name}} {{$employer->last_name}}</span>
                            <a href="tel:{{$employer->phone}}" class="page-object-agent_tel">{{$employer->phone}}</a>
                            <button class="page-object-agent_call button button--outline" data-toggle="modal"
                                    data-target="#requestCall">Заказать
                            </button>
                        </div>
                    </div>

                </div>
            </div>

            <div class="col-md-9 col-xl-7 order-md-1">
                @if($object->images)
                    <div class="page-object-gallery">
                        <div class="swiper-container gallery-top">
                            <div class="swiper-wrapper">
                                @foreach(json_decode($object->images,1) as $image)
                                    <div class="swiper-slide" style="background-image:url('/storage/{{$image}}')"></div>
                                @endforeach
                            </div>

                            <div class="swiper-button-next swiper-button-accent"></div>
                            <div class="swiper-button-prev swiper-button-accent"></div>
                        </div>
                        <div class="swiper-container gallery-thumbs">
                            <div class="swiper-wrapper">
                                @foreach(json_decode($object->images,1) as $image)
                                    <div class="swiper-slide" style="background-image:url('/storage/{{$image}}')"></div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                @else
                    <div class="page-object-gallery">
                        <div class="swiper-container gallery-top">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide" style="background-image:url('/storage/{{$object->main_image}}')"></div>
                            </div>

                            <div class="swiper-button-next swiper-button-accent"></div>
                            <div class="swiper-button-prev swiper-button-accent"></div>
                        </div>
                        <div class="swiper-container gallery-thumbs">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide" style="background-image:url('/storage/{{$object->main_image}}')"></div>
                            </div>
                        </div>
                    </div>
                @endif
                <div class="page-object_recommended d-none d-md-block d-xl-none">
                    <ul class="page-object_recommended-list">
                        @foreach($blocks->where('block_id','5')->all() as $part)
                            <li class="page-object_recommended-item">
                                <h5>{{$part->title}}</h5>
                                <p>{{strip_tags($part->text)}}</p>
                            </li>
                        @endforeach

                    </ul>
                </div>
            </div>

            <div class="d-none order-3 order-md-2 d-xl-block col-xl-2">
                <h4 class="page-object_h4 text-center">Ваш агент</h4>
                <div class="page-object-agent">
                    <div class="page-object-agent_preview">
                        <img src="/storage/{{$employer->photo}}" alt="{{$employer->first_name}}">
                    </div>
                    <div class="page-object-agent_contact">
                        <span class="page-object-agent_name">{{$employer->first_name}} {{$employer->last_name}}</span>
                        {{--<a href="tel:{{$employer->phone}}" class="page-object-agent_tel">{{$employer->phone}}</a>--}}
                        <button class="page-object-agent_call button button--outline" data-toggle="modal"
                               style="font-size: 16px;" data-target="#requestConsaltCall">Заказать звонок
                        </button>
                    </div>
                </div>
                <div width="400" style="position: absolute; z-index: 10; left: -100px;">
                    <iframe src="{{route('frame')}}" width="400" height="700" frameborder="0" align="left">
                        Ваш браузер не поддерживает плавающие фреймы!
                    </iframe>
                </div>
                {{--<div class="page-object-agent">--}}
                {{--<div class="page-object-agent_preview">--}}
                {{--<img src="/img/employee2.png" alt="мария">--}}
                {{--</div>--}}
                {{--<div class="page-object-agent_contact">--}}
                {{--<span class="page-object-agent_name">Мария Петросянова</span>--}}
                {{--<a href="tel:++375 29 777 77 77" class="page-object-agent_tel">+375 29 777 77 77</a>--}}
                {{--<button class="page-object-agent_call button button--outline" data-toggle="modal"--}}
                {{--data-target="#requestCall">Заказать</button>--}}
                {{--</div>--}}
                {{--</div>--}}
            </div>
        </div>

        <!-- Харакстеристики объекта -->
        <div class="obj-spec_row row">
            <div class="offset-md-3 col-md-7">
                <h2 class="obj-spec_title text-center">Характеристики объекта</h2>
            </div>
        </div>

        <div class="obj-spec_row-second row">
            <div class="col-md-4 col-xl-4 col-xxl-3 mb-5 mb-md-0">
                <div class="obj-spec_coast-box-container">
                    <div class="obj-spec_coast-box">
            <span class="obj-spec_coast-box-title">
              Цена
            </span>
                        <div class="obj-spec_coast-box-num"><b>{{number_format($object->price_usd,0,',',' ')}}</b> $</div>
                        <div class="obj-spec_coast-box-num"><b>{{number_format($object->price,0,',',' ')}}</b> Byn</div>
                    </div>

                    <div class="obj-spec_coast-box">
            <span class="obj-spec_coast-box-title">
              Цена за кв.м
            </span>
                        <div class="obj-spec_coast-box-num"><b>{{number_format($object->price_m2_usd,0,',',' ')}}</b> $
                        </div>
                        <div class="obj-spec_coast-box-num"><b>{{number_format($object->price_m2,0,',',' ')}}</b> Byn
                        </div>
                    </div>
                </div>

                <button class="obj-spec_button button button--main button--dark" data-toggle="modal"
                        data-target="#priceOffer">Предложить свою цену
                </button>
            </div>

            <div class="offset-xl-1 col-md-8 col-xl-7">
                <div class="obj-spec_slider-wrapper">

                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <ul class="obj-spec_list">
                                        <li>
                                            <img src="/storage/icons/general_area.svg" width="35" height="35" style="height: 35px;width: 35px;">&nbsp;
                                            Общая площадь: {{$object->square}}
                                        </li>
                                        <li>
                                            <img src="/storage/icons/live_area-01.svg" width="35" height="35" style="height: 35px;width: 35px;">&nbsp;
                                            Жилая площадь: {{$object->living_square}}
                                        </li>
                                        @if($object->kitchen_square)
                                        <li>
                                            <img src="/storage/icons/kitchen-01.svg" width="35" height="35" style="height: 35px;width: 35px;">&nbsp;
                                            {{--<svg xmlns="http://www.w3.org/2000/svg" width="43.914" height="53.884" viewBox="0 0 43.914 53.884">--}}
                                                {{--<g id="kitchen" transform="translate(-0.001 -6)">--}}
                                                    {{--<path id="Path_2645" data-name="Path 2645" d="M44.664,239.932a1,1,0,1,1-1-1A1,1,0,0,1,44.664,239.932Zm0,0" transform="translate(-37.677 -210.988)" fill="#ad221f"/>--}}
                                                    {{--<path id="Path_2646" data-name="Path 2646" d="M78.8,239.932a1,1,0,1,1-1-1A1,1,0,0,1,78.8,239.932Zm0,0" transform="translate(-67.817 -210.988)" fill="#ad221f"/>--}}
                                                    {{--<path id="Path_2647" data-name="Path 2647" d="M147.063,239.932a1,1,0,1,1-1-1A1,1,0,0,1,147.063,239.932Zm0,0" transform="translate(-128.099 -210.988)" fill="#ad221f"/>--}}
                                                    {{--<path id="Path_2648" data-name="Path 2648" d="M181.2,239.932a1,1,0,1,1-1-1A1,1,0,0,1,181.2,239.932Zm0,0" transform="translate(-158.24 -210.988)" fill="#ad221f"/>--}}
                                                    {{--<path id="Path_2649" data-name="Path 2649" d="M51.1,298.668H37.127a3,3,0,0,0-2.994,2.994v9.981a3,3,0,0,0,2.994,2.994H51.1a3,3,0,0,0,2.994-2.994v-9.981A3,3,0,0,0,51.1,298.668Zm1,12.975a1,1,0,0,1-1,1H37.127a1,1,0,0,1-1-1v-9.981a1,1,0,0,1,1-1H51.1a1,1,0,0,1,1,1Zm0,0" transform="translate(-30.14 -263.736)"/>--}}
                                                    {{--<path id="Path_2650" data-name="Path 2650" d="M9.531,11.976H33.484a1,1,0,0,0,.65-1.756L27.5,4.531V1a1,1,0,0,0-1-1h-9.98a1,1,0,0,0-1,1V4.531L8.881,10.22a1,1,0,0,0,.65,1.756ZM17.515,2H25.5v2H17.515Zm-.629,3.992h9.242L30.786,9.98H12.228Zm0,0" transform="translate(-7.534 6)"/>--}}
                                                    {{--<path id="Path_2651" data-name="Path 2651" d="M27.872,27c-.041-.892-1.1-2-2.92-2H3A3,3,0,0,0,0,27.994V56.938A3,3,0,0,0,3,59.932H24.952a2.961,2.961,0,0,0,1.285-.3c.187.192,1.55-.8,1.635-2.693S27.913,27.888,27.872,27ZM2,27.994a1,1,0,0,1,1-1H24.952a1,1,0,0,1,1,1v2.994H2ZM24.952,57.936H3s-1-.447-1-1V32.985H25.95V56.938c0,.551-1,1-1,1Zm18.963,0Z" transform="translate(0 -0.049)"/>--}}
                                                    {{--<path id="Path_2658" data-name="Path 2658" d="M69.264,332.8a1,1,0,0,0-1,1v1a1,1,0,0,0,2,0v-1A1,1,0,0,0,69.264,332.8Zm0,0" transform="translate(-60.281 -293.877)" fill="#ad221f"/>--}}
                                                    {{--<path id="Path_2659" data-name="Path 2659" d="M69.264,375.465a1,1,0,0,0-1,1v1a1,1,0,0,0,2,0v-1A1,1,0,0,0,69.264,375.465Zm0,0" transform="translate(-60.281 -331.551)" fill="#ad221f"/>--}}
                                                {{--</g>--}}
                                            {{--</svg>--}}
                                            Площадь кухни: {{$object->kitchen_square}}
                                        </li>
                                        @endif
                                        <li>
                                            <img src="/storage/icons/house_type.svg" width="35" height="35" style="height: 35px;width: 35px;">&nbsp;
                                            Тип постройки: {{$object->house_type}}
                                        </li>
                                    </ul>
                                </div>
                                <div class="swiper-slide">
                                    <ul class="obj-spec_list">
                                        <li>
                                            <img src="/storage/icons/quantity-01.svg" width="35" height="35" style="height: 35px;width: 35px;">&nbsp;
                                            Кол-во комнат: {{$object->rooms}}
                                        </li>
                                        <li>
                                            <img src="/storage/icons/floor.svg" width="35" height="35" style="height: 35px;width: 35px;">&nbsp;
                                            Этаж: {{$object->floor}}
                                        </li>
                                        @if($object->year)
                                        <li>
                                            <img src="/storage/icons/calendar (1)-01.svg" width="35" height="35" style="height: 35px;width: 35px;">&nbsp;
                                            {{--<svg id="calendar_1_" data-name="calendar (1)" xmlns="http://www.w3.org/2000/svg" width="30.64" height="30.64" viewBox="0 0 30.64 30.64">--}}
                                                {{--<path id="Path_2670" data-name="Path 2670" d="M27.947,3.591h-2.8a6.172,6.172,0,0,0-.57-2.043A2.4,2.4,0,0,0,22.5,0a2.4,2.4,0,0,0-2.072,1.548,6.172,6.172,0,0,0-.57,2.043h-1.9a6.172,6.172,0,0,0-.57-2.043A2.4,2.4,0,0,0,15.32,0a2.4,2.4,0,0,0-2.072,1.548,6.172,6.172,0,0,0-.57,2.043h-1.9a6.172,6.172,0,0,0-.57-2.043A2.4,2.4,0,0,0,8.139,0,2.4,2.4,0,0,0,6.066,1.548,6.172,6.172,0,0,0,5.5,3.591h-2.8A2.7,2.7,0,0,0,0,6.284V27.947A2.7,2.7,0,0,0,2.693,30.64H27.947a2.7,2.7,0,0,0,2.693-2.693V6.284A2.7,2.7,0,0,0,27.947,3.591Zm-5.912-1.24c.23-.459.436-.555.467-.555s.237.1.467.555a4.211,4.211,0,0,1,.365,1.24H21.669A4.218,4.218,0,0,1,22.035,2.351Zm-7.181,0c.23-.459.436-.555.467-.555s.237.1.467.555a4.218,4.218,0,0,1,.365,1.24H14.488a4.218,4.218,0,0,1,.365-1.24Zm-7.181,0c.23-.459.436-.555.467-.555s.237.1.467.555a4.218,4.218,0,0,1,.365,1.24H7.307a4.218,4.218,0,0,1,.365-1.24ZM2.693,5.386H8.971a4.218,4.218,0,0,1-.365,1.24c-.23.459-.436.555-.467.555a.9.9,0,1,0,0,1.8,2.4,2.4,0,0,0,2.072-1.548,6.172,6.172,0,0,0,.57-2.043h5.371a4.218,4.218,0,0,1-.365,1.24c-.23.459-.436.555-.467.555a.9.9,0,0,0,0,1.8,2.4,2.4,0,0,0,2.072-1.548,6.172,6.172,0,0,0,.57-2.043h5.371a4.218,4.218,0,0,1-.365,1.24c-.23.459-.436.555-.467.555a.9.9,0,0,0,0,1.8,2.4,2.4,0,0,0,2.072-1.548,6.172,6.172,0,0,0,.57-2.043h2.8a.9.9,0,0,1,.9.9v4.548H1.8V6.284a.9.9,0,0,1,.9-.9ZM27.947,28.845H2.693a.9.9,0,0,1-.9-.9V12.627H28.845v15.32A.9.9,0,0,1,27.947,28.845Zm0,0" transform="translate(0 0)"/>--}}
                                                {{--<path id="Path_2671" data-name="Path 2671" d="M83.441,241H61.9a.9.9,0,0,0-.9.9v10.832a.9.9,0,0,0,.9.9H83.441a.9.9,0,0,0,.9-.9V241.9A.9.9,0,0,0,83.441,241Zm-.9,5.386H78.953V242.8h3.591Zm-10.772,1.8v3.65H68.181v-3.65Zm-3.591-1.8V242.8h3.591v3.591Zm5.386,1.8h3.591v3.65H73.567Zm0-1.8V242.8h3.591v3.591ZM66.386,242.8v3.591H62.8V242.8ZM62.8,248.181h3.591v3.65H62.8Zm16.158,3.65v-3.65h3.591v3.65Zm0,0" transform="translate(-57.35 -226.578)" fill="#ad221f"/>--}}
                                            {{--</svg>--}}
                                            Год постройки: {{$object->year}}
                                        </li>
                                        @endif
                                        <li>
                                            <img src="/storage/icons/house_type.svg" width="35" height="35" style="height: 35px;width: 35px;">&nbsp;
                                            Тип дома: {{$object->house_type}}
                                        </li>
                                    </ul>
                                </div>
                                @if($all_blocks->where('block_id','8')->count())
                                <div class="swiper-slide">
                                    <ul class="obj-spec_list">

                                        @foreach($all_blocks->where('block_id','8')->all() as $charact)
                                            <li>
                                                <img src="/storage/{{$charact->file}}" alt="{{$charact->title}}" width="35" height="35" style="height: 35px;width: 35px;">&nbsp;
                                                {{$charact->title}}@if($charact->text): {{$charact->text}} @endif
                                            </li>
                                            @if(($loop->iteration%4===0) && !($loop->last))
                                                    </ul>
                                                </div>
                                                <div class="swiper-slide">
                                                    <ul class="obj-spec_list">
                                            @endif
                                        @endforeach
                                    </ul>
                                </div>
                                @endif
                            </div>

                        </div>
                        <div class="swiper-pagination"></div>
                        <div class="obj-spec_arrow-prev"></div>
                        <div class="obj-spec_arrow-next"></div>

                </div>
            </div>
        </div>
    </div>

    <!-- Форма обратной свзязи с типо табами -->
    <section class="obj-inner_tab-form-container">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="tab-form">
                        <h2 class="tab-form_title">
                            <svg class="tab-form_title-icon d-none d-xl-block" width="40" height="40">
                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/img/sprite-svg.svg#ornament2"></use>
                            </svg>
                            <svg class="tab-form_title-icon d-none d-xl-block" width="40" height="40">
                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/img/sprite-svg.svg#ornament2"></use>
                            </svg>
                            Продавайте с нами квартиры по выгодной цене
                        </h2>

                        <form class="tab-form_form ajax-form" method="post" action="{{route('send-form')}}">
                            {{ csrf_field() }}
                            @isset($object)
                                <input type="hidden" name="object" value="{{$object->name}}">
                                <input type="hidden" name="employer"  value="{{$employer->first_name}} {{$employer->last_name}}">
                                <input type="hidden" name="person_id" value="{{$employer->bitrix_id}}">
                                <input type="hidden" name="obj_address" value="{{$object->address}}">
                                <input type="hidden" name="obj_id" value="{{$object->bitrix_id ?? preg_replace("/[^0-9]/", '', $object->slug)}}">
                            @endisset
                            <div class="tab-form_radio-container">
                                <div class="custom-control custom-radio">
                                    <input type="radio" class="custom-control-input" id="sale-Checlbox" name="lead_id" value="6" checked>
                                    <label class="custom-control-label" for="sale-Checlbox">Продать</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input type="radio" class="custom-control-input" id="purchase-Checlbox" value="7" name="lead_id">
                                    <label class="custom-control-label" for="purchase-Checlbox">Купить</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input type="radio" class="custom-control-input" id="exchange-Checlbox" value="8" name="lead_id">
                                    <label class="custom-control-label" for="exchange-Checlbox">Обменять</label>
                                </div>
                            </div>

                            <div class="row">
                                <div class="offset-xl-2 col-md-12 col-xl-8 d-flex justify-content-md-between flex-column flex-md-row">
                                    <input type="text" name="name" required placeholder="Ваше Имя">
                                    <input type="tel" name="phone" required placeholder="Телефон">
                                    <button type="submit" class="tab-form_button button button--main">Оставить заявку
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="obj-inner_map">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="obj-inner_map-button-wrapper nav nav-tabs" id="objTab">
                        <a href="#description" id="description-tab" data-toggle="tab"
                           class="obj-inner_map-button button button--outline active" role="tab">Описание объекта</a>
                        <a href="#object-inner-map" id="object-inner-map-tab" data-toggle="tab"
                           class="obj-inner_map-button button button--lighte" role="tab">Объект на
                            карте</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="obj-inner_map-wrapper tab-content" id="objTabContent">
            <div class="tab-pane fade show active" id="description" role="tabpanel">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            {!! $object->content !!}
                        </div>
                    </div>
                </div>

            </div>
            <div class="tab-pane fade" id="object-inner-map" role="tabpanel"></div>
        </div>
    </section>

    <section class="section-hot-offer">
        <h2 class="base-title base-title--large base-title--icon-accent" data-aos="fade" data-aos-delay="150">
            <svg class="base-title-icon" width="40" height="40">
                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/img/sprite-svg.svg#ornament2"></use>
            </svg>
            <svg class="base-title-icon" width="40" height="40">
                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/img/sprite-svg.svg#ornament2"></use>
            </svg>
            Похожие варианты
        </h2>

        <div class="section-hot-offer_container container">

            <div class="row d-none d-sm-flex">
                <div class="col-12">
                    <div class="section-hot-offer_swiper-main swiper-container">
                        <div class="swiper-wrapper">
                        @foreach($objects as $add_object)
                            <!-- Карточка объекта -->
                                <a href="{{route('object',['slug'=>$add_object->slug])}}" class="swiper-slide obg-card obg-card--large" data-aos="fade-up" data-aos-delay="150">
                                    <div class="obg-card_label">@if($add_object->small_title){{$add_object->small_title}}@else{{$add_object->rooms}}-х комнатная квартира@endif</div>
                                    <!-- Слайдер -->
                                    @if($add_object->images)
                                        <div class="obg-card_preview swiper-container">
                                            <div class="obg-card_preview-wrapper swiper-wrapper">
                                            @foreach(json_decode($add_object->images,1) as $image)
                                                <!-- Слайды -->
                                                    <div class="obg-card_preview-slide swiper-slide">
                                                        <img src="/storage/{{$image}}" alt="{{$add_object->name}} {{$loop->iteration}}">
                                                    </div>
                                                @endforeach
                                            </div>

                                            <button class="obg-card_arrow obg-card_arrow-prev">
                                                <svg width="14" height="14">
                                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/img/sprite-svg.svg#arrow-left"></use>
                                                </svg>
                                            </button>
                                            <button class="obg-card_arrow obg-card_arrow-next">
                                                <svg width="14" height="14">
                                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/img/sprite-svg.svg#arrow-right"></use>
                                                </svg>
                                            </button>
                                        </div>
                                    @else
                                        <div class="obg-card_preview swiper-container">
                                            <div class="obg-card_preview-wrapper swiper-wrapper">
                                                <!-- Слайды -->
                                                <div class="obg-card_preview-slide swiper-slide">
                                                    <img src="{{$add_object->main_image}}" alt="{{$add_object->name}}">
                                                </div>
                                            </div>


                                        </div>
                                @endif
                                <!-- Описание -->
                                    <div class="obg-card_desc">
                                        <h3 class="obg-card_title">{{$add_object->name}}</h3>
                                        <span class="obg-card_address">
                                        <svg width="15" height="15">
                                          <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/img/sprite-svg.svg#pin"></use>
                                        </svg>
                                        {{$add_object->address}}
                                  </span>
                                    </div>
                                </a>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            <div class="row d-flex d-sm-none">
                @foreach($objects as $add_object)
                    <div class="section-hot-offer_obg-card-col col-md-4 col-xl-3">
                        <!-- Карточка объекта -->
                        <a href="{{route('object',['slug'=>$add_object->slug])}}" class="obg-card obg-card--large" data-aos="fade-up" data-aos-delay="150">
                            <div class="obg-card_label">{{$add_object->name}}</div>
                            <!-- Слайдер -->
                            @if($add_object->images)
                                <div class="obg-card_preview swiper-container">
                                    <div class="obg-card_preview-wrapper swiper-wrapper">
                                    @foreach(json_decode($add_object->images,1) as $image)
                                        <!-- Слайды -->
                                            <div class="obg-card_preview-slide swiper-slide">
                                                <img src="/storage/{{$image}}" alt="{{$add_object->name}} {{$loop->iteration}}" style="max-width: 414px; max-height: 232px;">
                                            </div>
                                        @endforeach
                                    </div>

                                    <button class="obg-card_arrow obg-card_arrow-prev">
                                        <svg width="14" height="14">
                                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/img/sprite-svg.svg#arrow-left"></use>
                                        </svg>
                                    </button>
                                    <button class="obg-card_arrow obg-card_arrow-next">
                                        <svg width="14" height="14">
                                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/img/sprite-svg.svg#arrow-right"></use>
                                        </svg>
                                    </button>
                                </div>
                            @else
                                <div class="obg-card_preview swiper-container">
                                    <div class="obg-card_preview-wrapper swiper-wrapper">
                                        <!-- Слайды -->
                                        <div class="obg-card_preview-slide swiper-slide">
                                            <img src="{{$add_object->main_image}}" alt="{{$add_object->name}}" style="max-width: 414px; max-height: 232px;">
                                        </div>
                                    </div>


                                </div>
                        @endif
                        <!-- Описание -->
                            <div class="obg-card_desc">
                                <h3 class="obg-card_title">{{$add_object->name}}</h3>
                                <span class="obg-card_address">
                                <svg width="15" height="15">
                                  <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/img/sprite-svg.svg#pin"></use>
                                </svg>
                                {{$add_object->address}}
                          </span>
                            </div>
                        </a>
                    </div>
                @endforeach
            </div>

            <div class="social-sharing_row row">
                <div class="col-12">
                    <div class="social-sharing">
                        <div class="social-sharing_item">
                            <div class="social-sharing_label">
                                <svg width="30" height="30">
                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/img/sprite-svg.svg#share"></use>
                                </svg>
                            </div>

                            <div class="social-sharing_content">
                                <a href="https://vk.com/share.php?url={{route('object',['slug'=>$object->slug])}}" class="social-sharing_link">
                                    <svg width="33" height="33">
                                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/img/sprite-svg.svg#vk"></use>
                                    </svg>
                                </a>
                                <a href="https://twitter.com/intent/tweet?url={{route('object',['slug'=>$object->slug])}}" class="social-sharing_link">
                                    <svg width="28" height="28">
                                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/img/sprite-svg.svg#tw"></use>
                                    </svg>
                                </a>
                                <a href="https://www.facebook.com/sharer/sharer.php?u={{route('object',['slug'=>$object->slug])}}" class="social-sharing_link">
                                    <svg width="23" height="23">
                                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/img/sprite-svg.svg#fb"></use>
                                    </svg>
                                </a>
                            </div>
                        </div>

                        <button class="social-sharing_item" onclick="window.print()">
                            <div class="social-sharing_label">
                                <svg width="30" height="30">
                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/img/sprite-svg.svg#print"></use>
                                </svg>
                            </div>

                            <div class="social-sharing_content">
                                <p>Печать</p>
                            </div>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="request-call-modal modal fade" id="priceOffer" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">

                <button class="close" aria-label="Закрыть" data-dismiss="modal"></button>
                <svg class="request-call-modal_ornament" data-aos="fade" data-aos-delay="200">
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/img/sprite-svg.svg#ornament3"></use>
                </svg>
                <svg class="request-call-modal_ornament" data-aos="fade" data-aos-delay="200">
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/img/sprite-svg.svg#ornament3"></use>
                </svg>

                <form class="request-call-modal_form ajax-form" method="post" action="{{route('send-form')}}">
                    {{ csrf_field() }}
                    @isset($object)
                        <input type="hidden" name="object" value="{{$object->name}}">
                        <input type="hidden" name="employer"  value="{{$employer->first_name}} {{$employer->last_name}}">
                        <input type="hidden" name="person_id" value="{{$employer->bitrix_id}}">
                        <input type="hidden" name="obj_address" value="{{$object->address}}">
                        <input type="hidden" name="obj_id" value="{{$object->bitrix_id ?? preg_replace("/[^0-9]/", '', $object->slug)}}">
                    @endisset
                    <input type="hidden" name="lead_id" value="9">
                    <label class="visually-hidden" for="request-call-modal-field-name"></label>
                    <input class="field failed--horizontal" type="text" name="name" required placeholder="Ваше Имя"
                           id="request-call-modal-field-name">

                    <label class="visually-hidden" for="request-call-modal-phone"></label>
                    <input class="field failed--horizontal" type="tel" name="phone" required placeholder="Телефон"
                           id="request-call-modal-name">
                    <label class="visually-hidden" for="request-call-modal-phone"></label>
                    <input class="field failed--horizontal" type="text" name="price"  placeholder="Укажите цену"
                           id="request-call-modal-name">

                    <button type="submit" class="button button--form">Оставить
                        заявку</button>
                </form>
            </div>
        </div>
    </div>
    <div class="request-call-modal modal fade" id="requestConsaltCall" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">

                <button class="close" aria-label="Закрыть" data-dismiss="modal"></button>
                <svg class="request-call-modal_ornament" data-aos="fade" data-aos-delay="200">
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/img/sprite-svg.svg#ornament3"></use>
                </svg>
                <svg class="request-call-modal_ornament" data-aos="fade" data-aos-delay="200">
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/img/sprite-svg.svg#ornament3"></use>
                </svg>

                <form class="request-call-modal_form ajax-form" method="post" action="{{route('send-form')}}">
                    {{ csrf_field() }}
                    @isset($object)
                        <input type="hidden" name="object" value="{{$object->name}}">
                        <input type="hidden" name="employer"  value="{{$employer->first_name}} {{$employer->last_name}}">
                        <input type="hidden" name="person_id" value="{{$employer->bitrix_id}}">
                        <input type="hidden" name="obj_address" value="{{$object->address}}">
                        <input type="hidden" name="obj_id" value="{{$object->bitrix_id ?? preg_replace("/[^0-9]/", '', $object->slug)}}">
                    @endisset
                    <input type="hidden" name="place" value="header">
                    <input type="hidden" name="lead_id" value="15">
                    <label class="visually-hidden" for="request-call-modal-field-name"></label>
                    <input class="field failed--horizontal" type="text" name="name" required placeholder="Ваше Имя"
                           id="request-call-modal-field-name">

                    <label class="visually-hidden" for="request-call-modal-phone"></label>
                    <input class="field failed--horizontal" type="tel" name="phone" required placeholder="Телефон"
                           id="request-call-modal-name">

                    {{--<label class="visually-hidden" for="request-call-modal-phone"></label>--}}
                    {{--<input class="field failed--horizontal" type="text" name="price"  placeholder="Укажите цену"--}}
                    {{--id="request-call-modal-name">--}}

                    <button type="submit" class="button button--form">Оставить
                        заявку</button>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        @if($object->images)
        var galleryThumbs = new Swiper('.gallery-thumbs', {
            spaceBetween: 20,
            slidesPerView: 'auto', {{--@if(count(json_decode($object->images,1))>5) 5 @else {{count(json_decode($object->images,1))}} @endif ,--}}
            freeMode: true,
            watchSlidesVisibility: true,
            watchSlidesProgress: true,
            //loop: true,
            loopedSlides: 1,
            breakpoints: {
                1199: {
                    direction: "vertical",
                    spaceBetween: 10,
                },

                768: {
                    direction: "horizontal",
                    spaceBetween: 10,
                }
            }
        });
        @endif
        var galleryTop = new Swiper('.gallery-top', {
            spaceBetween: 20,
            grabCursor: true,
                slidesPerView: 1,
            loop: true,
            loopedSlides: 1,
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
            thumbs: {
                swiper: galleryThumbs
            },
            breakpoints: {
                992: {
                    spaceBetween: 10,
                }
            }
        });

        var objSpec = new Swiper('.obj-spec_slider-wrapper .swiper-container', {
            slidesPerView: 2,
            loop: true,
            navigation: {
                nextEl: '.obj-spec_arrow-next',
                prevEl: '.obj-spec_arrow-prev',
            },
            pagination: {
                el: '.swiper-pagination',
                type: 'bullets',
                clickable: true
            },
            breakpoints: {
                1599: {
                    slidesPerView: 2,
                },
            }
        });

        if ($(window).width() > 576) {
            var objCardSlider = new Swiper(".section-hot-offer_swiper-main.swiper-container", {
                spaceBetween: 15,
                slidesPerView: 4,
                loop: true,
                breakpoints: {
                    1199: {
                        slidesPerView: 3,
                    },

                    767: {
                        slidesPerView: 2,
                    },
                }
            });
        }


        ymaps.ready(init);

        function init() {
            var myMap = new ymaps.Map("object-inner-map", {
                center: [{{$object->getCoordinates()[0]['lat']}}, {{$object->getCoordinates()[0]['lng']}}],
                zoom: 14
            });
            myMap.geoObjects.add(new ymaps.Placemark([{{$object->getCoordinates()[0]['lat']}}, {{$object->getCoordinates()[0]['lng']}}], {
                balloonContent: '{{$object->name}}'
            }, {
                iconLayout: 'default#imageWithContent',
                iconImageHref: '{{asset('img/pin.svg')}}',
                iconImageSize: [33, 40],
                iconImageOffset: [-16.5, -20],
            }))
        }

    </script>
@endsection
