@extends('layouts.main')

@section('title')
    <title>Наши объекты</title>
    <meta name="description" content="Наши объекты">
@endsection

@section('pre_css')
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <link rel="stylesheet" href="{{asset('/css/chunk-vendors.css')}}">
    <script src="https://api-maps.yandex.ru/2.1/?apikey=0c615af4-d453-4a88-986b-7bf18ed04914&lang=ru_RU"
            type="text/javascript">
    </script>
    <style>
        .object-main-page .obgect-card_arrow {display:none;}
    </style>
@endsection

@section('og')
    <meta property="og:title" content="Наши объекты">
    <meta property="og:image" content="{{asset('/img/slider-bg1.jpg')}}">
    <meta property="og:type" content="article">
    <meta property="og:url" content="{{url($page_path)}}">
    <meta property="og:description" content="Наши объекты">
@endsection

@section('content')
    <div class="root-container container">
        <div class="breadcrumbs_row row align-items-center">
            <div class="col-6">
                <nav class="breadcrumbs">
                    <ul class="breadcrumbs_list">
                        <li class="breadcrumbs_item"><a href="/" class="breadcrumbs_link">Главная</a></li>
                        <li class="breadcrumbs_item"><a class="breadcrumbs_link breadcrumbs_link--active">Объекты</a></li>
                    </ul>
                </nav>
            </div>

            <div class="col-6 d-none d-md-flex d-xl-none justify-content-end">
                <a href="#obj-form" data-scroll-link="obj-form" class="button button--dark">Оставить заявку</a>
            </div>
        </div>
    </div>
    <div class="object-main-page">
        <div id="app"></div>
    </div>

    <section class="obj-form position-relative d-block d-xl-none" id="obj-form">
        <img class="obj-form-bg" src="{{asset('/img/slider-bg1.jpg')}}" alt="bg">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="tab-form">
                        <h2 class="tab-form_title">
                            Продавайте с нами квартиры по выгодной цене
                        </h2>

                        <form class="tab-form_form ajax-form" method="post" action="{{route('send-form')}}">
                            {{ csrf_field() }}
                            <div class="tab-form_radio-container">
                                <div class="custom-control custom-radio">
                                    <input type="radio" class="custom-control-input" id="sale-Checlbox-main-layout" name="form-type"
                                           checked value="Продать">
                                    <label class="custom-control-label" for="sale-Checlbox-main-layout">Продать</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input type="radio" class="custom-control-input" id="purchase-Checlbox-main-layout" name="form-type" value="Купить">
                                    <label class="custom-control-label" for="purchase-Checlbox-main-layout">Купить</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input type="radio" class="custom-control-input" id="exchange-Checlbox-main-layout" name="form-type" value="Обменять">
                                    <label class="custom-control-label" for="exchange-Checlbox-main-layout">Обменять</label>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-12 d-flex justify-content-md-between flex-column flex-md-row">
                                    <input type="text" name="name" required placeholder="Ваше Имя">
                                    <input type="tel" name="phone" required placeholder="Телефон">
                                    <button type="submit" class="tab-form_button button button--main">Оставить заявку</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('script')
    <script>
        window.way = "{{url($path)}}"
    </script>

    <script src="{{asset('/js/app.js')}}"></script>
    <script src="{{asset('/js/chunk-vendors.js')}}"></script>
    {{--<script async src="https://api-maps.yandex.ru/2.1/?apikey=0c615af4-d453-4a88-986b-7bf18ed04914&lang=ru_RU"--}}
            {{--type="text/javascript">--}}
    {{--</script>--}}
@endsection
