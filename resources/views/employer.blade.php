@extends('layouts.main')

@section('title')
    <title>{{$employer->first_name}}</title>
    <meta name="description" content="{{$employer->description}}">
@endsection

@section('og')
    <meta property="og:title" content="{{$employer->first_name}}">
    <meta property="og:image" content="{{asset('/storage/'.$employer->photo)}}">
    <meta property="og:type" content="article">
    <meta property="og:url" content="{{route('employer',['id'=>$employer->id])}}">
    <meta property="og:description" content="{{$employer->description}}">
@endsection

@section('content')
    <div class="page-employee-inner">
        <main class="page-employee-inner_main">
            <img class="main-slider_bg" src="/img/slider-bg1.jpg" alt="Фон">
            <div class="container">
                <div class="row align-items-end align-items-md-start">
                    <div class="col-md-8">
                        <h1 class="page-employee-inner_main-title">Продавайте со мной квартиру по выгодной цене</h1>
                        <span class="page-employee-inner_main-subtitle">Получите бесплатную консультацию у агента</span>
                        <span class="page-employee-inner_main-name">
              <img src="/storage/{{$employer->photo}}" class="page-employee-inner_main-mobile-employee d-block d-md-none"
                   alt="alt">
              {{$employer->first_name}} {{$employer->last_name}}
            </span>

                        <form class="main-slider_form flex-xl-column flex-column-reverse ajax-form" method="post" action="{{route('send-form')}}">
                            {{ csrf_field() }}
                            <input type="hidden" name="person_id" value="{{$employer->bitrix_id}}">
                            <div class="d-flex flex-column flex-md-row">
                                <label class="visually-hidden" for="main-slider-field-name"></label>
                                <input class="field failed--horizontal" type="text" name="name" placeholder="Ваше Имя"
                                       id="main-slider-field-name">

                                <label class="visually-hidden" for="main-slider-field-phone"></label>
                                <input class="field failed--horizontal" type="tel" name="phone" placeholder="Телефон"
                                       id="main-slider-field-name">

                                <button type="submit" class="button button--form">Оставить заявку</button>
                            </div>

                            <div class="tab-form_radio-container">
                                <div class="custom-control custom-radio">
                                    <input type="radio" class="custom-control-input" id="sale-Checlbox" name="lead_id" value="10" checked>
                                    <label class="custom-control-label" for="sale-Checlbox">Продать</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input type="radio" class="custom-control-input" id="purchase-Checlbox" value="11" name="lead_id">
                                    <label class="custom-control-label" for="purchase-Checlbox">Купить</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input type="radio" class="custom-control-input" id="exchange-Checlbox" value="12" name="lead_id">
                                    <label class="custom-control-label" for="exchange-Checlbox">Обменять</label>
                                </div>
                            </div>
                        </form>
                    </div>

                    <div class="col-md-4 d-none d-md-block">
                        <img src="/storage/{{$employer->photo}}" alt="alt">
                    </div>
                </div>
            </div>
        </main>

        <section class="page-employee-inner_about container--light">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <h2 class="base-title base-title--large base-title--icon-accent" data-aos="fade" data-aos-delay="150">
                            <svg class="base-title-icon" width="40" height="40">
                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/img/sprite-svg.svg#ornament2"></use>
                            </svg>
                            <svg class="base-title-icon" width="40" height="40">
                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/img/sprite-svg.svg#ornament2"></use>
                            </svg>
                            Обо мне
                        </h2>
                    </div>
                </div>

                <div class="page-employee-inner_about-item-row row">
                    <div class="offset-xl-1 col-md-4 col-xl-3 mb-4 mb-md-0">
                        <div class="page-employee-inner_about-item top">
                            <h3>Коротко обо мне</h3>
                            <p>{{$employer->description}}</p>
                        </div>
                    </div>

                    <div class="col-md-4 d-flex justify-content-md-center justify-content-start mb-4 mb-md-0">
                        <div class="page-employee-inner_about-item top">
                            <h3>Увлечения</h3>
                            <p>{{$employer->hobbie}}</p>
                        </div>
                    </div>

                    <div class="col-md-4 col-xl-3 mb-4 mb-md-0">
                        <div class="page-employee-inner_about-item top">
                            <h3>Цитата</h3>
                            <p>{{$employer->quote}}</p>
                        </div>
                    </div>
                </div>

                <div class="page-employee-inner_about-item-row row">
                    <div class="offset-xl-1 col-md-5 mb-4 mb-md-0">
                        <div class="page-employee-inner_about-item">
                            <h3>Главное в моей работе</h3>
                            <p>{{$employer->main_work}}</p>
                        </div>
                    </div>

                    <div
                        class="offset-md-1 offset-xl-1 col-md-6 col-lg-4 d-flex justify-content-end justify-content-xl-start mb-4 mb-md-0">
                        <div class="page-employee-inner_about-item">
                            <h3>Совет клиентам</h3>
                            <p>{{$employer->advice}}</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="section-hot-offer">
            <h2 class="section-title" data-aos="fade" data-aos-delay="150">
        <span>
          <!-- Орнамент для десктопа -->
          <svg class="d-none d-md-block">
            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/img/sprite-svg.svg#ornament1"></use>
          </svg>
          <svg class="d-none d-md-block">
            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/img/sprite-svg.svg#ornament1"></use>
          </svg>

            <!-- Орнамент для мобилы -->
          <span>
            Объекты проданные мной
          </span>
            </h2>

            <div class="section-hot-offer_container container">
                <div class="row">
                    @foreach($sell_objects as $object)
                        <div class="section-hot-offer_obg-card-col col-md-4 col-xl-3">
                            <!-- Карточка объекта -->
                            <a href="{{route('object',['slug'=>$object->slug])}}" class="obg-card obg-card--large" data-aos="fade-up" data-aos-delay="150">
                                <div class="obg-card_label">@if($object->small_title){{$object->small_title}}@else{{$object->rooms}}-х комнатная квартира@endif</div>
                                <!-- Слайдер -->
                                @if($object->images)
                                    <div class="obg-card_preview swiper-container">
                                        <div class="obg-card_preview-wrapper swiper-wrapper">
                                        @foreach(json_decode($object->images,1) as $image)
                                            <!-- Слайды -->
                                                <div class="obg-card_preview-slide swiper-slide">
                                                    <img src="/storage/{{$image}}" alt="{{$object->name}} {{$loop->iteration}}" style="width: 414px; height: 232px;">
                                                </div>
                                            @endforeach
                                        </div>

                                        <button class="obg-card_arrow obg-card_arrow-prev">
                                            <svg width="14" height="14">
                                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/img/sprite-svg.svg#arrow-left"></use>
                                            </svg>
                                        </button>
                                        <button class="obg-card_arrow obg-card_arrow-next">
                                            <svg width="14" height="14">
                                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/img/sprite-svg.svg#arrow-right"></use>
                                            </svg>
                                        </button>
                                    </div>
                                @else
                                    <div class="obg-card_preview swiper-container">
                                        <div class="obg-card_preview-wrapper swiper-wrapper">
                                            <!-- Слайды -->
                                            <div class="obg-card_preview-slide swiper-slide">
                                                <img src="{{$object->main_image}}" alt="{{$object->name}}" style="width: 414px; height: 232px;">
                                            </div>
                                        </div>


                                    </div>
                            @endif
                            <!-- Описание -->
                                <div class="obg-card_desc">
                                    <h3 class="obg-card_title">{{$object->name}}</h3>
                                    <span class="obg-card_address">
                                <svg width="15" height="15">
                                  <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/img/sprite-svg.svg#pin"></use>
                                </svg>
                                    {{$object->address}}
                          </span>
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
            </div>
        </section>

        <section class="section-application">
            <svg class="section-application_ornament">
                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/img/sprite-svg.svg#ornament3"></use>
            </svg>
            <svg class="section-application_ornament">
                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/img/sprite-svg.svg#ornament3"></use>
            </svg>
            <h2 class="section-title" data-aos="fade" data-aos-delay="150">
        <span>
          Получите мою бесплатную консультацию
        </span>
            </h2>

            <div class="container">
                <div class="row">
                    <div class="col-xl-8 offset-xl-2">
                        <form class="main-slider_form ajax-form" method="post" action="{{route('send-form')}}">
                            {{ csrf_field() }}
                            <input type="hidden" name="lead_id" value="13">
                            <input type="hidden" name="person_id" value="{{$employer->bitrix_id}}">
                            <label class="visually-hidden" for="main-slider-field-name"></label>
                            <input class="field failed--horizontal" type="text" name="name" placeholder="Ваше Имя"
                                   id="main-slider-field-name">

                            <label class="visually-hidden" for="main-slider-field-phone"></label>
                            <input class="field failed--horizontal" type="tel" name="phone" placeholder="Телефон"
                                   id="main-slider-field-name">

                            <button type="submit" class="button button--form">Оставить заявку</button>
                        </form>
                    </div>
                </div>
            </div>
        </section>

        <section class="section-reviews">
            <!-- <svg class="section-reviews_ornament" data-aos="fade" data-aos-delay="150">
              <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite-svg.svg#ornament3"></use>
            </svg>
            <svg class="section-reviews_ornament" data-aos="fade" data-aos-delay="150">
              <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite-svg.svg#ornament3"></use>
            </svg> -->

            <h2 class="section-title" data-aos="fade" data-aos-delay="150">
        <span>
          <!-- Орнамент для десктопа -->
          <svg class="d-none d-md-block">
            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/img/sprite-svg.svg#ornament1"></use>
          </svg>
          <svg class="d-none d-md-block">
            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/img/sprite-svg.svg#ornament1"></use>
          </svg>

            <!-- Орнамент для мобилы -->
          <span>
            <svg width="24" height="24" class="d-block d-md-none">
              <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/img/sprite-svg.svg#ornament4"></use>
            </svg>
            <svg width="24" height="24" class="d-block d-md-none">
              <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/img/sprite-svg.svg#ornament4"></use>
            </svg>
            Отзывы обо мне
          </span>
            </h2>

            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="section-reviews_slider swiper-container">
                            <div class="section-reviews_slider-wrapper swiper-wrapper">

                            @foreach($reviews as $review)
                                @if(($loop->iteration)%2!=0 || $loop->first)
                                    <!-- Слайд с отзывами -->
                                        <div class="section-reviews_slide swiper-slide">
                                            <div class="section-reviews_slide-inner">
                                            @endif
                                            @if($review->video)
                                                <!-- Видео отзыв -->
                                                    <div class="section-reviews_slide-video">
                                                        <div class="review-card review-card-video">
                                                            <a class="review-card-video_link" href="https://youtu.be/{{$review->video}}">
                                                                <picture>
                                                                    <source srcset="https://i.ytimg.com/vi_webp/{{$review->video}}/maxresdefault.webp" type="image/webp">
                                                                    <img class="review-card-video_media"
                                                                         src="https://i.ytimg.com/vi/{{$review->video}}/maxresdefault.jpg" alt="PERU">
                                                                </picture>
                                                            </a>
                                                            <button class="review-card-video_button" type="button" aria-label="Запустить видео">
                                                                <svg width="80" height="80">
                                                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/img/sprite-svg.svg#play-button">
                                                                    </use>
                                                                </svg>
                                                            </button>
                                                        </div>
                                                        <header class="review-card_header">
                                                            <img src="/storage/{{$review->user->avatar}}" alt="{{$review->user->name}}" class="review-card_avatar avatar">
                                                            <div class="review-card_reviewer-info">
                                                                <h3 class="review-card_reviewer-name">{{$review->user->name}}</h3>
                                                                <time class="review-card_date" datetime="{{$review->created_at}}">{{$review->created_at->diffForHumans()}}</time>
                                                            </div>
                                                        </header>
                                                    </div>
                                            @else
                                                <!-- Текстовый отзыв -->
                                                    <div class="section-reviews_slide-text">
                                                        <article class="review-card review-card-text">
                                                            <header class="review-card_header">
                                                                <img src="/storage/{{$review->user->avatar}}" alt="{{$review->user->name}}" class="review-card_avatar avatar">
                                                                <div class="review-card_reviewer-info">
                                                                    <h3 class="review-card_reviewer-name">{{$review->user->name}}</h3>
                                                                    <time class="review-card_date" datetime="{{$review->created_at}}">{{$review->created_at->diffForHumans()}}</time>
                                                                </div>
                                                            </header>

                                                            <section class="review-card_text">
                                                                {!! $review->text !!}
                                                            </section>
                                                        </article>
                                                    </div>
                                                @endif
                                                @if(($loop->iteration)%2==0 || $loop->last)

                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                            </div>

                            <button class="section-reviews_arrow section-reviews_arrow-prev">
                                <svg width="37" height="37">
                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/img/sprite-svg.svg#arrow-left-big-dark">
                                    </use>
                                </svg>
                            </button>
                            <button class="section-reviews_arrow section-reviews_arrow-next">
                                <svg width="37" height="37">
                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/img/sprite-svg.svg#arrow-right-big-dark">
                                    </use>
                                </svg>
                            </button>
                        </div>
                    </div>
                </div>

                <div class="row section-see-more">
                    <div class="col-12 d-flex justify-content-center">
                        <a href="{{route('reviews')}}" class="button button button--outline">Посмотреть все отзывы</a>
                    </div>
                </div>
            </div>
        </section>

        <section class="section-application">
            <svg class="section-application_ornament">
                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/img/sprite-svg.svg#ornament3"></use>
            </svg>
            <svg class="section-application_ornament">
                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/img/sprite-svg.svg#ornament3"></use>
            </svg>
            <h2 class="section-title" data-aos="fade" data-aos-delay="150">
        <span>
          Заполните форму и я приступлю<br>
          к продаже вашей недвижимости
        </span>
            </h2>

            <div class="container">
                <div class="row">
                    <div class="col-xl-8 offset-xl-2">
                        <form class="main-slider_form ajax-form" ethod="post" action="{{route('send-form')}}">
                            {{ csrf_field() }}
                            <input type="hidden" name="lead_id" value="14">
                            <input type="hidden" name="person_id" value="{{$employer->bitrix_id}}">
                            <label class="visually-hidden" for="main-slider-field-name"></label>
                            <input class="field failed--horizontal" type="text" name="name" placeholder="Ваше Имя"
                                   id="main-slider-field-name">

                            <label class="visually-hidden" for="main-slider-field-phone"></label>
                            <input class="field failed--horizontal" type="tel" name="phone" placeholder="Телефон"
                                   id="main-slider-field-name">

                            <button type="submit" class="button button--form">Оставить заявку</button>
                        </form>
                    </div>
                </div>
            </div>
        </section>

        <section class="section-hot-offer">
            <h2 class="section-title" data-aos="fade" data-aos-delay="150">
        <span>
          <!-- Орнамент для десктопа -->
          <svg class="d-none d-md-block">
            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/img/sprite-svg.svg#ornament1"></use>
          </svg>
          <svg class="d-none d-md-block">
            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/img/sprite-svg.svg#ornament1"></use>
          </svg>

            <!-- Орнамент для мобилы -->
          <span>
            Мои объекты
          </span>
            </h2>

            <div class="section-hot-offer_container container">
                <div class="row">
                    @foreach($objects as $object)
                        <div class="section-hot-offer_obg-card-col col-md-4 col-xl-3">
                            <!-- Карточка объекта -->
                            <a href="{{route('object',['slug'=>$object->slug])}}" class="obg-card obg-card--large" data-aos="fade-up" data-aos-delay="150">
                                <div class="obg-card_label">@if($object->small_title){{$object->small_title}}@else{{$object->rooms}}-х комнатная квартира@endif</div>
                                <!-- Слайдер -->
                                @if($object->images)
                                    <div class="obg-card_preview swiper-container">
                                        <div class="obg-card_preview-wrapper swiper-wrapper">
                                        @foreach(json_decode($object->images,1) as $image)
                                            <!-- Слайды -->
                                                <div class="obg-card_preview-slide swiper-slide">
                                                    <img src="/storage/{{$image}}" alt="{{$object->name}} {{$loop->iteration}}" style="width: 414px; height: 232px;">
                                                </div>
                                            @endforeach
                                        </div>

                                        <button class="obg-card_arrow obg-card_arrow-prev">
                                            <svg width="14" height="14">
                                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/img/sprite-svg.svg#arrow-left"></use>
                                            </svg>
                                        </button>
                                        <button class="obg-card_arrow obg-card_arrow-next">
                                            <svg width="14" height="14">
                                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/img/sprite-svg.svg#arrow-right"></use>
                                            </svg>
                                        </button>
                                    </div>
                                @else
                                    <div class="obg-card_preview swiper-container">
                                        <div class="obg-card_preview-wrapper swiper-wrapper">
                                            <!-- Слайды -->
                                            <div class="obg-card_preview-slide swiper-slide">
                                                <img src="{{$object->main_image}}" alt="{{$object->name}}" style="width: 414px; height: 232px;">
                                            </div>
                                        </div>


                                    </div>
                            @endif
                            <!-- Описание -->
                                <div class="obg-card_desc">
                                    <h3 class="obg-card_title">{{$object->name}}</h3>
                                    <span class="obg-card_address">
                                <svg width="15" height="15">
                                  <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/img/sprite-svg.svg#pin"></use>
                                </svg>
                                    {{$object->address}}
                          </span>
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
            </div>
        </section>
    </div>

@endsection

@section('js')
    <script>
        var reviewSlider = new Swiper(".section-reviews_slider.swiper-container", {
            grabCursor: true,
            loop: true,
            slidesPerView: 1,
            spaceBetween: 30,
            navigation: {
                nextEl: '.section-reviews_arrow-next',
                prevEl: '.section-reviews_arrow-prev',
            },
        })
    </script>
@endsection
