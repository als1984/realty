<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PagesController@index')->name('index');

Route::get('/objects', 'ObjectController@objects')->name('objects');

Route::get('/objects/buy', 'ObjectController@objects')->name('objects-bue');

Route::get('/objects/sell', 'ObjectController@objects')->name('objects-sel');

Route::get('/objects/rent', 'ObjectController@objects')->name('objects-rent');

Route::get('/objects/change', 'ObjectController@objects')->name('objects-change');

Route::post('/form/contacts', 'FormContactsController@addContact')->name('send-form');

Route::get('/objects/{slug}', 'ObjectController@index')->name('object');

Route::get('/script/frame', 'FrameController@index')->name('frame');

Route::get('/about/employers', 'EmployersController@index')->name('employers');
Route::get('/about/employers/{id}', 'EmployersController@employer')->name('employer');

Route::get('/about/reviews', 'ReviewController@index')->name('reviews');

Route::post('/about/reviews', 'ReviewController@add')->name('add-review');

Route::post('/form/invoice', 'FormContactsController@addInvoice')->name('add-invoice');

Auth::routes();

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

Route::get('/{slug}', 'PagesController@pages')->name('pages');
