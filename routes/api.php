<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('/objects/filter','Api\FilterController@filter');

Route::get('/objects/{type?}','Api\FilterController@index');

Route::get('/bitix/auth','Api\FormsController@token')->name('token');

Route::post('/forms','Api\FormsController@index');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
